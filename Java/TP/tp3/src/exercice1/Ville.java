<<<<<<< HEAD
package exercice1;

import java.util.Arrays;
import java.io.Serializable;

public class Ville implements Serializable {
    private static final long serialVersionUID = 1L;
    String nom;
    int population;
    Pays pays;
    Ville[] jumelle;

    public Ville(String nom, int population, Pays pays) throws Exception {

        if (nom.length() < 5)
            throw new ShortName(nom);
        if (nom.length() > 14)
            throw new LongName(nom);
        this.nom = nom;
        if (population < 1)
            throw new InhabiterException(population);
        this.population = population;
        this.pays = pays;
        this.jumelle = null;
    }

    public Ville(String nom, int population, Pays pays, Ville[] jumelle) throws Exception {
        this(nom, population, pays);
        this.jumelle = jumelle;
    }

    @Override
    public String toString() {
        return "Ville [jumelle=" + jumelle + ", nom=" + nom + ", pays=" + pays + ", population=" + population + "]";
    }

    @Override
    public boolean equals(Object obj) {
        if (this == obj)
            return true;
        if (obj == null)
            return false;
        if (getClass() != obj.getClass())
            return false;
        Ville other = (Ville) obj;
        if (!Arrays.equals(jumelle, other.jumelle))
            return false;
        if (nom == null) {
            if (other.nom != null)
                return false;
        } else if (!nom.equals(other.nom))
            return false;
        if (pays == null) {
            if (other.pays != null)
                return false;
        } else if (!pays.equals(other.pays))
            return false;
        if (population != other.population)
            return false;
        return true;
    }

    public String getNom() {
        return nom;
    }

    public void setNom(String nom) {
        this.nom = nom;
    }

    public int getPopulation() {
        return population;
    }

    public void setPopulation(int population) {
        this.population = population;
    }

    public Pays getPays() {
        return pays;
    }

    public void setPays(Pays pays) {
        this.pays = pays;
    }

    public Ville[] getJumelle() {
        return jumelle;
    }

    public void setJumelle(Ville[] jumelle) {
        this.jumelle = jumelle;
    }

}
=======
package exercice1;

import java.util.Arrays;
import java.io.Serializable;

public class Ville implements Serializable {
    private static final long serialVersionUID = 1L;
    String nom;
    int population;
    Pays pays;
    Ville[] jumelle;

    public Ville(String nom, int population, Pays pays) throws Exception {

        if (nom.length() < 5)
            throw new ShortName(nom);
        if (nom.length() > 14)
            throw new LongName(nom);
        this.nom = nom;
        if (population < 1)
            throw new InhabiterException(population);
        this.population = population;
        this.pays = pays;
        this.jumelle = null;
    }

    public Ville(String nom, int population, Pays pays, Ville[] jumelle) throws Exception {
        this(nom, population, pays);
        this.jumelle = jumelle;
    }

    @Override
    public String toString() {
        return "Ville [jumelle=" + jumelle + ", nom=" + nom + ", pays=" + pays + ", population=" + population + "]";
    }

    @Override
    public boolean equals(Object obj) {
        if (this == obj)
            return true;
        if (obj == null)
            return false;
        if (getClass() != obj.getClass())
            return false;
        Ville other = (Ville) obj;
        if (!Arrays.equals(jumelle, other.jumelle))
            return false;
        if (nom == null) {
            if (other.nom != null)
                return false;
        } else if (!nom.equals(other.nom))
            return false;
        if (pays == null) {
            if (other.pays != null)
                return false;
        } else if (!pays.equals(other.pays))
            return false;
        if (population != other.population)
            return false;
        return true;
    }

    public String getNom() {
        return nom;
    }

    public void setNom(String nom) {
        this.nom = nom;
    }

    public int getPopulation() {
        return population;
    }

    public void setPopulation(int population) {
        this.population = population;
    }

    public Pays getPays() {
        return pays;
    }

    public void setPays(Pays pays) {
        this.pays = pays;
    }

    public Ville[] getJumelle() {
        return jumelle;
    }

    public void setJumelle(Ville[] jumelle) {
        this.jumelle = jumelle;
    }

}
>>>>>>> main
