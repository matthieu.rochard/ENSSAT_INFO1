<<<<<<< HEAD
package exercice1;

import java.io.Serializable;

public class Pays implements Serializable {
    private static final long serialVersionUID = 1L;

    String nom, nomContinent;

    public Pays(String nom, String nomContinent) {
        this.nom = nom;
        this.nomContinent = nomContinent;
    }

    @Override
    public String toString() {
        return "Pays [nom=" + nom + ", nomContinent=" + nomContinent + "]";
    }

    @Override
    public boolean equals(Object obj) {
        if (this == obj)
            return true;
        if (obj == null)
            return false;
        if (getClass() != obj.getClass())
            return false;
        Pays other = (Pays) obj;
        if (nom == null) {
            if (other.nom != null)
                return false;
        } else if (!nom.equals(other.nom))
            return false;
        if (nomContinent == null) {
            if (other.nomContinent != null)
                return false;
        } else if (!nomContinent.equals(other.nomContinent))
            return false;
        return true;
    }

    public String getNom() {
        return nom;
    }

    public void setNom(String nom) {
        this.nom = nom;
    }

    public String getNomContinent() {
        return nomContinent;
    }

    public void setNomContinent(String nomContinent) {
        this.nomContinent = nomContinent;
    }
}
=======
package exercice1;

import java.io.Serializable;

public class Pays implements Serializable {
    private static final long serialVersionUID = 1L;

    String nom, nomContinent;

    public Pays(String nom, String nomContinent) {
        this.nom = nom;
        this.nomContinent = nomContinent;
    }

    @Override
    public String toString() {
        return "Pays [nom=" + nom + ", nomContinent=" + nomContinent + "]";
    }

    @Override
    public boolean equals(Object obj) {
        if (this == obj)
            return true;
        if (obj == null)
            return false;
        if (getClass() != obj.getClass())
            return false;
        Pays other = (Pays) obj;
        if (nom == null) {
            if (other.nom != null)
                return false;
        } else if (!nom.equals(other.nom))
            return false;
        if (nomContinent == null) {
            if (other.nomContinent != null)
                return false;
        } else if (!nomContinent.equals(other.nomContinent))
            return false;
        return true;
    }

    public String getNom() {
        return nom;
    }

    public void setNom(String nom) {
        this.nom = nom;
    }

    public String getNomContinent() {
        return nomContinent;
    }

    public void setNomContinent(String nomContinent) {
        this.nomContinent = nomContinent;
    }
}
>>>>>>> main
