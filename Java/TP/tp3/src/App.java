import java.io.File;
import java.io.FileInputStream;
import java.io.FileOutputStream;
import java.io.ObjectInputStream;
import java.io.ObjectOutputStream;

import exercice1.Pays;
import exercice1.Ville;
import exercice2.InputReader;

public class App {
    public static void main(String[] args) throws Exception {
        int jobDone = 0;
        String fichier, ville, pays, continent;
        int nb;
        File f = null;
        InputReader.Initialise();
        Ville tmp;
        Pays p;
        FileOutputStream fos;
        ObjectOutputStream oos;
        FileInputStream fis;
        ObjectInputStream ois;
        do {

            System.out.println("Nom du fichier à créer:");
            fichier = InputReader.chaine();
            try {
                f = new File(fichier);
                jobDone++;
            } catch (Exception exception) {
                System.out.println(exception.getMessage());
            }
        } while (jobDone < 0);
        fos = new FileOutputStream(f);
        oos = new ObjectOutputStream(fos);
        while (jobDone < 4) {
            System.out.println("Saisir nom de la ville:");
            ville = InputReader.chaine();
            System.out.println("Saisir nb d’habitant:");
            nb = InputReader.entier();
            System.out.println("Saisir nom de la Pays:");
            pays = InputReader.chaine();
            System.out.println("Saisir nom de la Continent:");
            continent = InputReader.chaine();
            try {
                p = new Pays(pays, continent);
                tmp = new Ville(ville, nb, p);
                jobDone++;
                oos.writeObject(tmp);
            } catch (Exception e) {
                System.out.println("La ville n'a pas été cree car: " + e.getMessage());
            }
        }
        oos.close();
        fos.close();
        fis = new FileInputStream(fichier);
        ois = new ObjectInputStream(fis);

        while (jobDone < 7) {
            try {
                tmp = (Ville) ois.readObject();
                System.out.println(tmp);
                jobDone++;
            } catch (Exception e) {
                System.out.println(e.getMessage());
            }
        }
        ois.close();
        fis.close();
    }
}
