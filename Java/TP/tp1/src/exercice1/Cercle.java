package exercice1;

public class Cercle {
    float rayon;

    public Cercle(float rayon) {
        this.rayon = rayon;
    }

    public double calculerAire(){
        return 3.14*rayon*rayon;
    }

    public static void main(String[] args){
        //System.out.println(args);
        Cercle q1 = new Cercle(Float.valueOf(args[0]));
        System.out.println("Aire: "+q1.calculerAire());
    }
}
