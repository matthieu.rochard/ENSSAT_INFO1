package exercice6;

import java.util.Collection;
import java.util.HashMap;
import java.util.Iterator;
import java.util.Random;

import exercice5.Personne;

public class CollectionPersonne {
    private HashMap<Integer, Personne> collection;
    private Random random;

    public CollectionPersonne() {
        this.collection = new HashMap<Integer, Personne>();
        this.random = new Random(555);
    }

    public int ajouter(Personne personne) {
        int index;
        do {
            index = this.random.nextInt(500);
        } while (this.collection.containsKey(index));
        this.collection.put(index, personne);
        return index;
    }

    public Personne getPersonne(int index) {
        if (this.collection.containsKey(index)) {
            return this.collection.get(index);
        } else {
            return null;
        }
    }

    public void afficher(){
        int i;
        Iterator<Integer> ite = this.collection.keySet().iterator();
        System.out.println("*************************");
        System.out.println("********Personne*********");
        System.out.println("*************************");
        while(ite.hasNext()){
            i = ite.next();
            System.out.println("Id="+i + this.collection.get(i));
        }
    }

    public static void main(String args[]) {
        int tmp;
        
        Personne pers1 = new Personne("Matthieu", "Rochard", "France", "Colombe");
        Personne pers2 = new Personne("Jean", "Phillipe", "France", "Rennes");
        Personne pers3 = new Personne("Jean", "Phillipe", "France", "Rennes");
        CollectionPersonne collectionPersonne = new CollectionPersonne();
        collectionPersonne.ajouter(pers1);
        collectionPersonne.ajouter(pers2);
        collectionPersonne.ajouter(pers3);

    }
}
