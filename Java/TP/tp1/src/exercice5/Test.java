package exercice5;

public class Test {

    public static void main(String args[]){
        Personne pers1 = new Personne("Matthieu","Rochard","France","Colombe");
        Personne pers2 = new Personne("Jean","Phillipe","France","Rennes");
        Personne pers3 = new Personne("Jean","Phillipe","France","Rennes");
        
        System.out.println(pers1);
        System.out.println(pers1.equals(pers2));
        System.out.println(pers2.equals(pers3));
    }
}
