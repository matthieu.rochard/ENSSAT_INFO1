package exercice5;

public class Personne {
    private String nom,prenom,paysNaissance,villeNaissance;

    public Personne(String nom, String prenom, String paysNaissance, String villeNaissance) {
        this.nom = nom;
        this.prenom = prenom;
        this.paysNaissance = paysNaissance;
        this.villeNaissance = villeNaissance;
    }

    public void addiche(){
        System.out.println(this.toString());
    }

    public String getNom() {
        return nom;
    }

    public void setNom(String nom) {
        this.nom = nom;
    }

    public String getPrenom() {
        return prenom;
    }

    public void setPrenom(String prenom) {
        this.prenom = prenom;
    }

    public String getPaysNaissance() {
        return paysNaissance;
    }

    public String getVilleNaissance() {
        return villeNaissance;
    }

    @Override
    public String toString() {
        return "Personne [nom=" + nom + ", paysNaissance=" + paysNaissance + ", prenom=" + prenom + ", villeNaissance="
                + villeNaissance + "]";
    }

    @Override
    public boolean equals(Object obj) {
        if (this == obj)
            return true;
        if (obj == null)
            return false;
        if (getClass() != obj.getClass())
            return false;
        Personne other = (Personne) obj;
        if (nom == null) {
            if (other.nom != null)
                return false;
        } else if (!nom.equals(other.nom))
            return false;
        if (paysNaissance == null) {
            if (other.paysNaissance != null)
                return false;
        } else if (!paysNaissance.equals(other.paysNaissance))
            return false;
        if (prenom == null) {
            if (other.prenom != null)
                return false;
        } else if (!prenom.equals(other.prenom))
            return false;
        if (villeNaissance == null) {
            if (other.villeNaissance != null)
                return false;
        } else if (!villeNaissance.equals(other.villeNaissance))
            return false;
        return true;
    }

}
