package exercice3;

import java.text.Normalizer;

public class Palindrome {

    static boolean isPalindrome1(String str){
        int mid = str.length()/2;
        String str1= str.substring(0, mid);
        StringBuilder str2 = new StringBuilder();
        str2.append(str.substring(mid, str.length()));
        str2.reverse();
        return str1.equalsIgnoreCase(str2.toString());
    }

    public static String unaccent(String src) {
		return Normalizer.normalize(src, Normalizer.Form.NFD).replaceAll("[^\\p{ASCII}]", "");
	}

    static boolean isPalindrome2(String str){
        int mid1,mid2;
        str = unaccent(str).replaceAll("\\s+", "");
        if(str.length()%2==0){
            mid1 = str.length()/2;
            mid2 = str.length()/2;
        }else{
            mid1 = (str.length()-1)/2;
            mid2 = (str.length()+1)/2;
        }
        
        String str1= str.substring(0, mid1);
        StringBuilder str2 = new StringBuilder();
        str2.append(str.substring(mid2, str.length()));
        str2.reverse();
        //System.out.println(str1+" || "+str2);
        return str1.equalsIgnoreCase(str2.toString());
    }

    public static void main(String[] args) throws Exception {
        
        System.out.println("Toto est un palindrome "+isPalindrome1("Toto"));
        System.out.println("Anna est un palindrome "+isPalindrome1("Anna"));
        System.out.println("anna est un palindrome "+isPalindrome1("anna"));
        System.out.println("toot est un palindrome "+isPalindrome1("toot"));
        System.out.println("élu par cette crapule "+isPalindrome2("élu par cette crapule"));
        System.out.println("ésope reste ici et se repose "+isPalindrome2("ésope reste ici et se repose"));
    }
}
