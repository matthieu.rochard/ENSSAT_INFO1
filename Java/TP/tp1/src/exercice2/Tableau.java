package exercice2;

import java.util.ArrayList;
import java.util.Random;

public class Tableau {
    public static void main(String[] args) {
        Random random = new Random(54);
        ArrayList<Integer> arrayList = new ArrayList<Integer>();
        for (int i = 0; i < 10; i++) {
            arrayList.add(random.nextInt()/54);
        }

        for (int i : arrayList) {
            System.out.print(i+";");
        }
        System.out.println();
        System.out.print(" Sorted: ");
        arrayList.sort(null);
        for (int i : arrayList) {
            System.out.print(i+";");
        }
    }
}
