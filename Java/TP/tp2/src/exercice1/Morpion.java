package exercice1;

import java.io.IOException;
import java.util.Arrays;

public class Morpion {
    // public static char[] joueurs = { '1', '2' };
    public int[][] grille = { { 0, 0, 0 }, { 0, 0, 0 }, { 0, 0, 0 } };

    public Morpion() {
    }

    boolean case_Libre() {
        return (this.contains(0) > 0);
    }

    int contains(int i) {
        int nb = 0;
        for (int j = 0; j < 3; j++) {
            for (int j2 = 0; j2 < 3; j2++) {
                nb += (this.grille[j][j2] == i) ? 1 : 0;
            }
        }
        return nb;
    }

    void saisir(int joueurs) throws IOException {

        int x, y;
        affiche();
        do {
            System.out.println("Joueur " + joueurs + " (ligne colonne) ");
            System.out.println("ligne:");
            x = InputReader.entier();
            System.out.println("colonne:");
            y = InputReader.entier();

        } while (this.grille[x - 1][y - 1] != 0);
        this.grille[x - 1][y - 1] = joueurs;
    }

    boolean gagne() {

        for (int j = 1; j < 3; j++) {
            if (this.contains(j) < 3) { // Si le joueur n'à pas jouer 3 fois
                continue;
            }
            for (int i = 0; i < grille.length; i++) {
                if (this.grille[i][0] == j && this.grille[i][1] == j && this.grille[i][2] == j) {
                    return true;
                } else if (this.grille[0][i] == j && this.grille[1][i] == j && this.grille[2][i] == j) {
                    return true;
                }
            }
            if (this.grille[0][0] == j && this.grille[1][1] == j && this.grille[2][2] == j) {
                return true;
            }
            if (this.grille[2][0] == j && this.grille[1][1] == j && this.grille[0][2] == j) {
                return true;
            }
        }
        return false;
    }

    public void affiche() {
        System.out.println(" 1 2 3 ");
        System.out.println("|" + this.grille[0][0] + "|" + this.grille[0][1] + "|" + this.grille[0][2] + "| 1");
        System.out.println("|" + this.grille[1][0] + "|" + this.grille[1][1] + "|" + this.grille[1][2] + "| 2");
        System.out.println("|" + this.grille[2][0] + "|" + this.grille[2][1] + "|" + this.grille[2][2] + "| 3");
    }

    public static void main(String[] args) throws Exception {
        Morpion mp = new Morpion();
        InputReader.Initialise();
        System.out.println(Arrays.asList(1, 2, 3, 1));
        int joueurActif = 2;
        do {
            joueurActif = (joueurActif == 1) ? 2 : 1;
            mp.saisir(joueurActif);
        } while (!mp.gagne() && mp.case_Libre());
        System.out.println("Bravo Joueur " + joueurActif);
        InputReader.Deinitialise();

    }
}
