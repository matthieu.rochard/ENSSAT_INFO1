package exercice1;

import java.io.IOException;
import java.util.Scanner;

public class InputReader {
	private static Scanner sc;

	static void Initialise() {
		sc = new Scanner(System.in);
	}

	static int entier() throws IOException {
		return sc.nextInt();
	}

	static double reeld() throws IOException {
		return sc.nextDouble();
	}

	static float reelf() throws IOException {
		return sc.nextFloat();
	}

	static String chaine() throws IOException {
		return sc.nextLine();
	}

	static char car() throws IOException {
		return chaine().charAt(0);
	}

	static void Deinitialise() {
		sc.close();
	}
}
