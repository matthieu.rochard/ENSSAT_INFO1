package exercice2;

public class Segment2D {
    private Point2D a, b;

    public Segment2D(Point2D a, Point2D b) {
        if (a.equals(b))
            System.err.println("<!>Les deux points du segment sont les mêmes");
        else {
            this.a = a;
            this.b = b;
        }
    }

    double norme() {
        return Math.sqrt(Math.pow(Math.abs(this.a.getX() - this.b.getX()), 2)
                + Math.pow(Math.abs(this.a.getY() - this.b.getY()), 2));
    }

    @Override
    public String toString() {
        return "Segment2D [a=" + a + ", b=" + b + "]";
    }

    @Override
    public boolean equals(Object obj) {
        if (this == obj)
            return true;
        if (obj == null)
            return false;
        if (getClass() != obj.getClass())
            return false;
        Segment2D other = (Segment2D) obj;
        if (a == null) {
            if (other.a != null)
                return false;
        } else if (!a.equals(other.a))
            return false;
        if (b == null) {
            if (other.b != null)
                return false;
        } else if (!b.equals(other.b))
            return false;
        if (a.equals(other.b) && b.equals(other.a))
            return true;
        return true;
    }

    public Point2D getA() {
        return a;
    }

    public void setA(Point2D a) {
        this.a = a;
    }

    public Point2D getB() {
        return b;
    }

    public void setB(Point2D b) {
        this.b = b;
    }

    public static void main(String[] args) throws Exception {
        Point2D p1 = new Point2D(5, 0);
        Point2D p2 = new Point2D(-1, 0);
        Segment2D s1 = new Segment2D(p1, p2);
        System.out.println(s1.norme());
    }

}
