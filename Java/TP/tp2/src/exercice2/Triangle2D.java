package exercice2;

public class Triangle2D {
    Point2D a, b, c;

    public Triangle2D(Point2D a, Point2D b, Point2D c) {
        if (a.equals(b) || a.equals(c) || b.equals(c))
            System.err.println("<!>Deux points du triangle sont les mêmes");
        else {
            this.a = a;
            this.b = b;
            this.c = c;
        }
    }

    public static void main(String[] args) throws Exception {
        Point2D p1 = new Point2D(5, 0);
        Point2D p2 = new Point2D(0, 5);
        Point2D p3 = new Point2D(0, 0);
        Triangle2D t1 = new Triangle2D(p1, p2, p3);
        System.out.println(t1.estIsocele());
        System.out.println(t1.estEquilateral());
        System.out.println(t1.estRectangle());

    }

    public boolean estIsocele() {
        Segment2D s1 = new Segment2D(this.a, this.b);
        Segment2D s2 = new Segment2D(this.b, this.c);
        Segment2D s3 = new Segment2D(this.c, this.a);
        return s1.norme() == s2.norme() || s1.norme() == s3.norme() || s2.norme() == s3.norme();
    }

    public boolean estEquilateral() {
        Segment2D s1 = new Segment2D(this.a, this.b);
        Segment2D s2 = new Segment2D(this.b, this.c);
        Segment2D s3 = new Segment2D(this.c, this.a);
        return s1.norme() == s2.norme() && s1.norme() == s3.norme();
    }

    public boolean estRectangle() {
        Segment2D s1 = new Segment2D(this.a, this.b);
        Segment2D s2 = new Segment2D(this.b, this.c);
        Segment2D s3 = new Segment2D(this.c, this.a);
        System.out.println(s1.norme());
        System.out.println(s2.norme());
        System.out.println(s3.norme());
        System.out.println(Math.pow(s1.norme(), 2) + " " + Math.pow(s2.norme(), 2) + Math.pow(s3.norme(), 2));
        System.out.println(Math.pow(s2.norme(), 2) + " " + Math.pow(s1.norme(), 2) + Math.pow(s3.norme(), 2));
        System.out.println(Math.pow(s3.norme(), 2) + " " + Math.pow(s2.norme(), 2) + Math.pow(s1.norme(), 2));
        if (s1.norme() > s2.norme() && s1.norme() > s3.norme()) {
            return Math.pow(s1.norme(), 2) == Math.pow(s2.norme(), 2) + Math.pow(s3.norme(), 2);
        } else if (s2.norme() > s1.norme() && s2.norme() > s3.norme()) {
            return Math.pow(s2.norme(), 2) == Math.pow(s1.norme(), 2) + Math.pow(s3.norme(), 2);
        } else if (s3.norme() > s1.norme() && s3.norme() > s2.norme()) {
            return Math.pow(s3.norme(), 2) == Math.pow(s2.norme(), 2) + Math.pow(s1.norme(), 2);
        } else {
            return false;
        }
    }
    /*
     * public double aire() {
     * 
     * }
     */

    @Override
    public String toString() {
        return "Triangle2D [a=" + a + ", b=" + b + ", c=" + c + "]";
    }

    @Override
    public boolean equals(Object obj) {
        if (this == obj)
            return true;
        if (obj == null)
            return false;
        if (getClass() != obj.getClass())
            return false;
        Triangle2D other = (Triangle2D) obj;
        if (a == null) {
            if (other.a != null)
                return false;
        } else if (!a.equals(other.a))
            return false;
        if (b == null) {
            if (other.b != null)
                return false;
        } else if (!b.equals(other.b))
            return false;
        if (c == null) {
            if (other.c != null)
                return false;
        } else if (!c.equals(other.c))
            return false;
        return true;
    }

    public Point2D getA() {
        return a;
    }

    public void setA(Point2D a) {
        this.a = a;
    }

    public Point2D getB() {
        return b;
    }

    public void setB(Point2D b) {
        this.b = b;
    }

    public Point2D getC() {
        return c;
    }

    public void setC(Point2D c) {
        this.c = c;
    }
}
