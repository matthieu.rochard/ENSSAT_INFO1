<<<<<<< HEAD
import java.util.ArrayList;
import java.util.Arrays;
import java.util.List;

public class Client {
    private int numero, numeroDePorte, codePostalClient;
    private String nom, prenom, rueClient, villeClient;
    private CompteBancaire possede[];

    public Client(int numero, int numeroDePorte, int codePostalClient, String nom, String prenom, String rueClient,
            String villeClient) {
        this.numero = numero;
        this.numeroDePorte = numeroDePorte;
        this.codePostalClient = codePostalClient;
        this.nom = nom;
        this.prenom = prenom;
        this.rueClient = rueClient;
        this.villeClient = villeClient;
        this.possede = new CompteBancaire[10];
    }

    public void addPossede(CompteBancaire compteBancaire) {
        List<CompteBancaire> l = new ArrayList<CompteBancaire>(Arrays.asList(this.possede));
        l.add(compteBancaire);
        this.possede = l.toArray(this.possede);
        // System.out.println(Arrays.toString(this.possede));
        compteBancaire.setClient(this);
    }

    public String getNomClient() {
        return nom;
    }

    public void setNomClient(String nom) {
        this.nom = nom;
    }

    public String getPrenomClient() {
        return prenom;
    }

    public void setPrenomClient(String prenom) {
        this.prenom = prenom;
    }

    public String getAdresseClient() {
        return rueClient + "\n" + codePostalClient + " " + villeClient;
    }

    public void setAdresseClient(int codePostal, String rueClient, String villeClient) {
        this.codePostalClient = codePostal;
        this.rueClient = rueClient;
        this.villeClient = villeClient;
    }

    @Override
    public String toString() {
        return "Client [codePostalClient=" + codePostalClient + ", nom=" + nom + ", numero=" + numero
                + ", numeroDePorte=" + numeroDePorte + ", possede=" + possede + ", prenom=" + prenom + ", rueClient="
                + rueClient + ", villeClient=" + villeClient + "]";
    }

    public CompteBancaire[] getPossede() {
        return possede;
    }
}
=======
import java.util.ArrayList;
import java.util.Arrays;
import java.util.List;

public class Client {
    private int numero, numeroDePorte, codePostalClient;
    private String nom, prenom, rueClient, villeClient;
    private CompteBancaire possede[];

    public Client(int numero, int numeroDePorte, int codePostalClient, String nom, String prenom, String rueClient,
            String villeClient) {
        this.numero = numero;
        this.numeroDePorte = numeroDePorte;
        this.codePostalClient = codePostalClient;
        this.nom = nom;
        this.prenom = prenom;
        this.rueClient = rueClient;
        this.villeClient = villeClient;
        this.possede = new CompteBancaire[10];
    }

    public void addPossede(CompteBancaire compteBancaire) {
        List<CompteBancaire> l = new ArrayList<CompteBancaire>(Arrays.asList(this.possede));
        l.add(compteBancaire);
        this.possede = l.toArray(this.possede);
        // System.out.println(Arrays.toString(this.possede));
        compteBancaire.setClient(this);
    }

    public String getNomClient() {
        return nom;
    }

    public void setNomClient(String nom) {
        this.nom = nom;
    }

    public String getPrenomClient() {
        return prenom;
    }

    public void setPrenomClient(String prenom) {
        this.prenom = prenom;
    }

    public String getAdresseClient() {
        return rueClient + "\n" + codePostalClient + " " + villeClient;
    }

    public void setAdresseClient(int codePostal, String rueClient, String villeClient) {
        this.codePostalClient = codePostal;
        this.rueClient = rueClient;
        this.villeClient = villeClient;
    }

    @Override
    public String toString() {
        return "Client [codePostalClient=" + codePostalClient + ", nom=" + nom + ", numero=" + numero
                + ", numeroDePorte=" + numeroDePorte + ", possede=" + possede + ", prenom=" + prenom + ", rueClient="
                + rueClient + ", villeClient=" + villeClient + "]";
    }

    public CompteBancaire[] getPossede() {
        return possede;
    }
}
>>>>>>> main
