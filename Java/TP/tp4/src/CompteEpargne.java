<<<<<<< HEAD
import java.util.Date;

public class CompteEpargne extends CompteBancaire implements ParametresCompteEpargne {
    public double remuneration;
    private boolean fiscalite;

    public CompteEpargne(double solde, int numeroCompte, Date dateOuverture, Client possede, GestionCompte gere,
            double remuneration, boolean fiscalite) {
        super(solde, numeroCompte, dateOuverture, possede, gere);
        this.remuneration = remuneration;
        this.fiscalite = fiscalite;
    }

    @Override
    void credit(double somme) {
        if (this.solde + somme < PLAFOND)
            this.solde += somme;
        else
            System.out.println("MAXIMUM atteint");
    }

    @Override
    void dedit(double somme) {
        if (this.solde - somme > MINIMUM_SUR_LA_COMPTE)
            this.solde -= somme;
        else
            System.out.println("MINIMUM atteint");
    }

    public double calculeInteret() {
        return this.solde * 0.2;
    }

    public double getRemuneration() {
        return remuneration;
    }

    public void setRemuneration(double remuneration) {
        this.remuneration = remuneration;
    }

    public boolean isFiscalite() {
        return fiscalite;
    }

    public void setFiscalite(boolean fiscalite) {
        this.fiscalite = fiscalite;
    }

    @Override
    public String toString() {
        return "CompteEpargne [fiscalite=" + fiscalite + ", remuneration=" + remuneration + "]";
    }

}
=======
import java.util.Date;

public class CompteEpargne extends CompteBancaire implements ParametresCompteEpargne {
    public double remuneration;
    private boolean fiscalite;

    public CompteEpargne(double solde, int numeroCompte, Date dateOuverture, Client possede, GestionCompte gere,
            double remuneration, boolean fiscalite) {
        super(solde, numeroCompte, dateOuverture, possede, gere);
        this.remuneration = remuneration;
        this.fiscalite = fiscalite;
    }

    @Override
    void credit(double somme) {
        if (this.solde + somme < PLAFOND)
            this.solde += somme;
        else
            System.out.println("MAXIMUM atteint");
    }

    @Override
    void dedit(double somme) {
        if (this.solde - somme > MINIMUM_SUR_LA_COMPTE)
            this.solde -= somme;
        else
            System.out.println("MINIMUM atteint");
    }

    public double calculeInteret() {
        return this.solde * 0.2;
    }

    public double getRemuneration() {
        return remuneration;
    }

    public void setRemuneration(double remuneration) {
        this.remuneration = remuneration;
    }

    public boolean isFiscalite() {
        return fiscalite;
    }

    public void setFiscalite(boolean fiscalite) {
        this.fiscalite = fiscalite;
    }

    @Override
    public String toString() {
        return "CompteEpargne [fiscalite=" + fiscalite + ", remuneration=" + remuneration + "]";
    }

}
>>>>>>> main
