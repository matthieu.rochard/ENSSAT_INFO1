<<<<<<< HEAD
import java.util.Date;

public class CompteCourant extends CompteBancaire {
    private int decouvertAutorise;
    private boolean chequier;
    private int carte;

    public CompteCourant(double solde, int numeroCompte, Date dateOuverture, Client possede, GestionCompte gere,
            int decouvertAutorise, boolean chequier, int carte) {
        super(solde, numeroCompte, dateOuverture, possede, gere);
        this.decouvertAutorise = decouvertAutorise;
        this.chequier = chequier;
        this.carte = carte;
    }

    @Override
    void credit(double somme) {
        this.solde += somme;
    }

    @Override
    void dedit(double somme) {
        if (this.solde - somme < -this.decouvertAutorise) {
            System.out.println("Découvert non autorisé");
        } else {
            this.solde -= somme;
        }
    }

    @Override
    public String toString() {
        return "CompteCourant [carte=" + carte + ", chequier=" + chequier + ", decouvertAutorise=" + decouvertAutorise
                + "]";
    }

    public int getDecouvertAutorise() {
        return decouvertAutorise;
    }

    public void setDecouvertAutorise(int decouvertAutorise) {
        this.decouvertAutorise = decouvertAutorise;
    }

    public boolean isChequier() {
        return chequier;
    }

    public void setChequier(boolean chequier) {
        this.chequier = chequier;
    }

    public int getCarte() {
        return carte;
    }

    public void setCarte(int carte) {
        this.carte = carte;
    }
}
=======
import java.util.Date;

public class CompteCourant extends CompteBancaire {
    private int decouvertAutorise;
    private boolean chequier;
    private int carte;

    public CompteCourant(double solde, int numeroCompte, Date dateOuverture, Client possede, GestionCompte gere,
            int decouvertAutorise, boolean chequier, int carte) {
        super(solde, numeroCompte, dateOuverture, possede, gere);
        this.decouvertAutorise = decouvertAutorise;
        this.chequier = chequier;
        this.carte = carte;
    }

    @Override
    void credit(double somme) {
        this.solde += somme;
    }

    @Override
    void dedit(double somme) {
        if (this.solde - somme < -this.decouvertAutorise) {
            System.out.println("Découvert non autorisé");
        } else {
            this.solde -= somme;
        }
    }

    @Override
    public String toString() {
        return "CompteCourant [carte=" + carte + ", chequier=" + chequier + ", decouvertAutorise=" + decouvertAutorise
                + "]";
    }

    public int getDecouvertAutorise() {
        return decouvertAutorise;
    }

    public void setDecouvertAutorise(int decouvertAutorise) {
        this.decouvertAutorise = decouvertAutorise;
    }

    public boolean isChequier() {
        return chequier;
    }

    public void setChequier(boolean chequier) {
        this.chequier = chequier;
    }

    public int getCarte() {
        return carte;
    }

    public void setCarte(int carte) {
        this.carte = carte;
    }
}
>>>>>>> main
