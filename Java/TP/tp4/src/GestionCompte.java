<<<<<<< HEAD
import java.sql.Date;
import java.time.LocalDate;
import java.util.HashMap;

public class GestionCompte implements ParametresCompteEpargne {
    HashMap<Integer, CompteBancaire> geres;

    public static int nombre = 1;

    public GestionCompte() {
        this.geres = new HashMap<Integer, CompteBancaire>();
    }

    public void creerCompteCourant(Client client, int decouvertAutorise, boolean chequier, int carte) {
        new CompteCourant(0, nombre++, Date.valueOf(LocalDate.now()), client, this, decouvertAutorise, chequier, carte);
    }

    public void creerCompteEpargne(double solde, boolean fiscalite, double remuneration, Client client) {
        if (solde < VERSEMENT_INITIAL) {
            System.out.println("Le virement initiale est inferieur à la valeur requise");
            return;
        }
        new CompteEpargne(solde, nombre++, Date.valueOf(LocalDate.now()), client, this, remuneration, fiscalite);
    }

    public HashMap<Integer, CompteBancaire> getGeres() {
        return geres;
    }

    public void addGere(CompteBancaire compteBancaire) {
        this.geres.put(compteBancaire.getNumeroCompte(), compteBancaire);
    }

    public void setGeres(HashMap<Integer, CompteBancaire> geres) {
        this.geres = geres;
    }

    @Override
    public String toString() {
        return "GestionCompte [geres=" + geres + "]";
    }

}
=======
import java.sql.Date;
import java.time.LocalDate;
import java.util.HashMap;

public class GestionCompte implements ParametresCompteEpargne {
    HashMap<Integer, CompteBancaire> geres;

    public static int nombre = 1;

    public GestionCompte() {
        this.geres = new HashMap<Integer, CompteBancaire>();
    }

    public void creerCompteCourant(Client client, int decouvertAutorise, boolean chequier, int carte) {
        new CompteCourant(0, nombre++, Date.valueOf(LocalDate.now()), client, this, decouvertAutorise, chequier, carte);
    }

    public void creerCompteEpargne(double solde, boolean fiscalite, double remuneration, Client client) {
        if (solde < VERSEMENT_INITIAL) {
            System.out.println("Le virement initiale est inferieur à la valeur requise");
            return;
        }
        new CompteEpargne(solde, nombre++, Date.valueOf(LocalDate.now()), client, this, remuneration, fiscalite);
    }

    public HashMap<Integer, CompteBancaire> getGeres() {
        return geres;
    }

    public void addGere(CompteBancaire compteBancaire) {
        this.geres.put(compteBancaire.getNumeroCompte(), compteBancaire);
    }

    public void setGeres(HashMap<Integer, CompteBancaire> geres) {
        this.geres = geres;
    }

    @Override
    public String toString() {
        return "GestionCompte [geres=" + geres + "]";
    }

}
>>>>>>> main
