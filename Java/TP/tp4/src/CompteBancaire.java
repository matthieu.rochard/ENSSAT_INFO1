<<<<<<< HEAD
import java.util.Date;

public abstract class CompteBancaire {
    protected double solde;
    protected int numeroCompte;
    protected Date dateOuverture;
    private Client possede;
    private GestionCompte gere;

    public CompteBancaire(double solde, int numeroCompte, Date dateOuverture, Client possede, GestionCompte gere) {
        this.solde = solde;
        this.numeroCompte = numeroCompte;
        this.dateOuverture = dateOuverture;
        possede.addPossede(this);
        this.gere = gere;
        gere.addGere(this);
    }

    public int getNumeroCompte() {
        return numeroCompte;
    }

    public void setNumeroCompte(int numeroCompte) {
        this.numeroCompte = numeroCompte;
    }

    public Date getDateOuverture() {
        return dateOuverture;
    }

    public void setDateOuverture(Date dateOuverture) {
        this.dateOuverture = dateOuverture;
    }

    public Client getClient() {
        return possede;
    }

    public void setClient(Client client) {
        this.possede = client;
    }

    @Override
    public String toString() {
        return "CompteBancaire [dateOuverture=" + dateOuverture + ", gere=" + gere + ", numeroCompte=" + numeroCompte
                + ", possede=" + possede + ", solde=" + solde + "]";
    }

    abstract void credit(double somme);

    abstract void dedit(double somme);

    public double getSolde() {
        return solde;
    }

    public void setSolde(double solde) {
        this.solde = solde;
    }

}
=======
import java.util.Date;

public abstract class CompteBancaire {
    protected double solde;
    protected int numeroCompte;
    protected Date dateOuverture;
    private Client possede;
    private GestionCompte gere;

    public CompteBancaire(double solde, int numeroCompte, Date dateOuverture, Client possede, GestionCompte gere) {
        this.solde = solde;
        this.numeroCompte = numeroCompte;
        this.dateOuverture = dateOuverture;
        possede.addPossede(this);
        this.gere = gere;
        gere.addGere(this);
    }

    public int getNumeroCompte() {
        return numeroCompte;
    }

    public void setNumeroCompte(int numeroCompte) {
        this.numeroCompte = numeroCompte;
    }

    public Date getDateOuverture() {
        return dateOuverture;
    }

    public void setDateOuverture(Date dateOuverture) {
        this.dateOuverture = dateOuverture;
    }

    public Client getClient() {
        return possede;
    }

    public void setClient(Client client) {
        this.possede = client;
    }

    @Override
    public String toString() {
        return "CompteBancaire [dateOuverture=" + dateOuverture + ", gere=" + gere + ", numeroCompte=" + numeroCompte
                + ", possede=" + possede + ", solde=" + solde + "]";
    }

    abstract void credit(double somme);

    abstract void dedit(double somme);

    public double getSolde() {
        return solde;
    }

    public void setSolde(double solde) {
        this.solde = solde;
    }

}
>>>>>>> main
