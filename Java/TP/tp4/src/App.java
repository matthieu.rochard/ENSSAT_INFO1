public class App {
    public static void main(String[] args) throws Exception {
        System.out.println("Hello, World!");
        GestionCompte gc = new GestionCompte();

        Client c1 = new Client(1, 12, 22563, "Martin", "Max", "Rue de la Resistance", "Ploudono");
        Client c2 = new Client(2, 178, 36000, "Robert", "Alex", "Avenue de France", "Lannion");
        Client c3 = new Client(3, 10, 45870, "Kornile", "Thibault", "Avenue de Breton", "Paris");

        gc.creerCompteCourant(c1, 200, true, 1);
        gc.creerCompteCourant(c2, 1000, false, 1);
        gc.creerCompteEpargne(0, true, 10, c2);
        gc.creerCompteEpargne(10, false, 30, c3);
        System.out.println(c1.getPossede()[10]);

        c1.getPossede()[10].credit(100);
        c1.getPossede()[10].dedit(500);
        System.out.println(c1.getPossede()[10].getSolde());

    }
}
