<<<<<<< HEAD
import java.awt.GridLayout;
import java.awt.event.ActionEvent;
import java.awt.event.ActionListener;
import java.awt.event.KeyEvent;
import java.awt.event.KeyListener;
/*
import javax.swing.JButton;
import javax.swing.JFrame;
import javax.swing.JOptionPane;
import javax.swing.JPanel;
import javax.swing.JTextField;*/
import javax.swing.*;

public class Fenetre extends JFrame implements ActionListener, KeyListener {
    JButton jbuttonConvertir;
    JButton jbuttonReset;
    JPanel panel;
    JTextField textEuro;
    JTextField textDollar;

    public Fenetre() {
        // delacaration et instanciation de la jframe
        JFrame maFrame = new JFrame();
        // fixer la taille
        maFrame.setSize(500, 150);
        // on centre la fenetre
        maFrame.setLocationRelativeTo(null);
        // mettre un titre
        maFrame.setTitle("ma premiere fenetre");

        // faire en sorte que la fermeture de la fenetre
        // ferme le programme
        maFrame.setDefaultCloseOperation(JFrame.EXIT_ON_CLOSE);

        // instanciation d'un panel
        panel = new JPanel(new GridLayout(3, 3));

        panel.add(new JLabel("Euro"));
        textEuro = new JTextField("0");
        textEuro.addKeyListener(this);
        panel.add(textEuro);

        panel.add(new JLabel("Dollars"));
        textDollar = new JTextField("0");
        textDollar.addKeyListener(this);
        panel.add(textDollar);

        // instanciation d'un bouton
        jbuttonConvertir = new JButton("Convertir");
        // ajoute un listener au bouton
        jbuttonConvertir.addActionListener(this);
        panel.add(jbuttonConvertir);

        // instanciation d'un bouton
        jbuttonReset = new JButton("reset");
        // ajoute un listener au bouton
        jbuttonReset.addActionListener(this);
        panel.add(jbuttonReset);

        // ajoute le panel à la frame
        maFrame.add(panel);

        // rendre visible la fenetre
        maFrame.setVisible(true);
    }

    public static void main(String args[]) {
        Fenetre convTest = new Fenetre();
    }

    @Override
    public void actionPerformed(ActionEvent e) {
        // déclanché si clic sur le bouton
        if (e.getSource() == jbuttonConvertir) {
            this.convertir();
        } else if (e.getSource() == jbuttonReset) {
            this.reset();
        }
    }

    @Override
    public void keyPressed(KeyEvent e) {
        // déclenché si le curseur est dans le jtextfield et appuie sur la touche entrée
        if (e.getSource() == textEuro && e.getKeyCode() == KeyEvent.VK_ENTER) {
            this.convertir();
        } else if (e.getSource() == textDollar && e.getKeyCode() == KeyEvent.VK_ENTER) {
            this.convertir();
        }

    }

    public void convertir() {
        if ((Integer.parseInt(textEuro.getText())) > 0 && Integer.parseInt(textDollar.getText()) < 1) {
            double dollars = Integer.parseInt(textEuro.getText()) * 1.2;
            textDollar.setText("" + dollars);
            System.out.println("---------------------------------------------------------------------------------");
        } else if ((Integer.parseInt(textDollar.getText())) > 0 && (Integer.parseInt(textEuro.getText())) < 1) {
            double euro = Integer.parseInt(textDollar.getText()) / 1.2;
            textEuro.setText("" + euro);
            System.out.println("================================================================================");
        } else {
            JOptionPane.showMessageDialog(null,
                    "Manuel: Inséré la valeur à convertir dans l'un des champs. Conserver le champs de la devise souhaité à 0.");
        }
    }

    public void reset() {
        textEuro.setText("" + 0);
        textDollar.setText("" + 0);
    }

    @Override
    public void keyReleased(KeyEvent arg0) {
        // TODO Auto-generated method stub

    }

    @Override
    public void keyTyped(KeyEvent arg0) {
        // TODO Auto-generated method stub

    }
=======
import java.awt.GridLayout;
import java.awt.event.ActionEvent;
import java.awt.event.ActionListener;
import java.awt.event.KeyEvent;
import java.awt.event.KeyListener;
/*
import javax.swing.JButton;
import javax.swing.JFrame;
import javax.swing.JOptionPane;
import javax.swing.JPanel;
import javax.swing.JTextField;*/
import javax.swing.*;

public class Fenetre extends JFrame implements ActionListener, KeyListener {
    JButton jbuttonConvertir;
    JButton jbuttonReset;
    JPanel panel;
    JTextField textEuro;
    JTextField textDollar;

    public Fenetre() {
        // delacaration et instanciation de la jframe
        JFrame maFrame = new JFrame();
        // fixer la taille
        maFrame.setSize(500, 150);
        // on centre la fenetre
        maFrame.setLocationRelativeTo(null);
        // mettre un titre
        maFrame.setTitle("ma premiere fenetre");

        // faire en sorte que la fermeture de la fenetre
        // ferme le programme
        maFrame.setDefaultCloseOperation(JFrame.EXIT_ON_CLOSE);

        // instanciation d'un panel
        panel = new JPanel(new GridLayout(3, 3));

        panel.add(new JLabel("Euro"));
        textEuro = new JTextField("0");
        textEuro.addKeyListener(this);
        panel.add(textEuro);

        panel.add(new JLabel("Dollars"));
        textDollar = new JTextField("0");
        textDollar.addKeyListener(this);
        panel.add(textDollar);

        // instanciation d'un bouton
        jbuttonConvertir = new JButton("Convertir");
        // ajoute un listener au bouton
        jbuttonConvertir.addActionListener(this);
        panel.add(jbuttonConvertir);

        // instanciation d'un bouton
        jbuttonReset = new JButton("reset");
        // ajoute un listener au bouton
        jbuttonReset.addActionListener(this);
        panel.add(jbuttonReset);

        // ajoute le panel à la frame
        maFrame.add(panel);

        // rendre visible la fenetre
        maFrame.setVisible(true);
    }

    public static void main(String args[]) {
        Fenetre convTest = new Fenetre();
    }

    @Override
    public void actionPerformed(ActionEvent e) {
        // déclanché si clic sur le bouton
        if (e.getSource() == jbuttonConvertir) {
            this.convertir();
        } else if (e.getSource() == jbuttonReset) {
            this.reset();
        }
    }

    @Override
    public void keyPressed(KeyEvent e) {
        // déclenché si le curseur est dans le jtextfield et appuie sur la touche entrée
        if (e.getSource() == textEuro && e.getKeyCode() == KeyEvent.VK_ENTER) {
            this.convertir();
        } else if (e.getSource() == textDollar && e.getKeyCode() == KeyEvent.VK_ENTER) {
            this.convertir();
        }

    }

    public void convertir() {
        if ((Integer.parseInt(textEuro.getText())) > 0 && Integer.parseInt(textDollar.getText()) < 1) {
            double dollars = Integer.parseInt(textEuro.getText()) * 1.2;
            textDollar.setText("" + dollars);
            System.out.println("---------------------------------------------------------------------------------");
        } else if ((Integer.parseInt(textDollar.getText())) > 0 && (Integer.parseInt(textEuro.getText())) < 1) {
            double euro = Integer.parseInt(textDollar.getText()) / 1.2;
            textEuro.setText("" + euro);
            System.out.println("================================================================================");
        } else {
            JOptionPane.showMessageDialog(null,
                    "Manuel: Inséré la valeur à convertir dans l'un des champs. Conserver le champs de la devise souhaité à 0.");
        }
    }

    public void reset() {
        textEuro.setText("" + 0);
        textDollar.setText("" + 0);
    }

    @Override
    public void keyReleased(KeyEvent arg0) {
        // TODO Auto-generated method stub

    }

    @Override
    public void keyTyped(KeyEvent arg0) {
        // TODO Auto-generated method stub

    }
>>>>>>> main
}