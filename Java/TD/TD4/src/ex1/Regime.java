package ex1;

import java.util.ArrayList;
import java.util.Iterator;
import java.util.List;

public class Regime {
    private List<Ingredient> aliments;

    public int calculerPrix() {
        Iterator<Ingredient> ite = aliments.iterator();
        Ingredient ing;
        int s = 0;
        while (ite.hasNext()) {
            ing = ite.next();
            s += ing.getPrix() * ing.getQuantity();
        }
        return s;
    }

    boolean ajouterAliments(Ingredient ingredient) {
        boolean notFound = true;
        for (Ingredient e : aliments) {
            if (e.getNom().equals(ingredient.getNom())) {
                notFound = false;
            }
        }
        if (notFound) {
            return ajouterIngredient(ingredient);
        }
        return false;
    }

    boolean ajouterIngredient(Ingredient ingredient) {
        return this.aliments.add(ingredient);
    }

    /**
     * 
     */
    public Regime() {
        this.aliments = new ArrayList<Ingredient>();
    }

    /**
     * @return the aliments
     */
    public List<Ingredient> getAliments() {
        return aliments;
    }

    /**
     * @param aliments the aliments to set
     */
    public void setAliments(List<Ingredient> aliments) {
        this.aliments = aliments;
    }

}
