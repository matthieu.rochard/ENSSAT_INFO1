package ex1;
import java.util.Arrays;

public class Chimpanze extends Vegetarien {

    /**
     * @param nom
     * @param age
     * @param mange
     * @param possede
     */
    public Chimpanze(String nom, int age, Regime mange, Zoo possede) {
        this.nom = nom;
        this.age = age;
        this.mange = mange;
        this.possede = possede;
        this.enfants = Arrays.asList();
    }

}
