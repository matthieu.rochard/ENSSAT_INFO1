package ex1;
import java.util.Arrays;

public class Tigre extends Carnivore {
    private String couleur;

    /**
     * @param nom
     * @param age
     * @param mange
     * @param possede
     * @param couleur
     */
    public Tigre(String nom, int age, Regime mange, Zoo possede, String couleur) {
        this.nom = nom;
        this.age = age;
        this.mange = mange;
        this.possede = possede;
        this.couleur = couleur;
        this.enfants = Arrays.asList();
    }

    /**
     * @return the couleur
     */
    public String getCouleur() {
        return couleur;
    }

    /**
     * @param couleur the couleur to set
     */
    public void setCouleur(String couleur) {
        this.couleur = couleur;
    }
}
