package ex1;

public class Ingredient {
    private String nom;
    private int quantity;
    private int prix;

    /**
     * @param nom
     * @param quantity
     * @param prix
     */
    public Ingredient(String nom, int quantity, int prix) {
        this.nom = nom;
        this.quantity = quantity;
        this.prix = prix;
    }

    /**
     * @return the nom
     */
    public String getNom() {
        return nom;
    }

    /**
     * @param nom the nom to set
     */
    public void setNom(String nom) {
        this.nom = nom;
    }

    /**
     * @return the quantity
     */
    public int getQuantity() {
        return quantity;
    }

    /**
     * @param quantity the quantity to set
     */
    public void setQuantity(int quantity) {
        this.quantity = quantity;
    }

    /**
     * @return the prix
     */
    public int getPrix() {
        return prix;
    }

    /**
     * @param prix the prix to set
     */
    public void setPrix(int prix) {
        this.prix = prix;
    }

}
