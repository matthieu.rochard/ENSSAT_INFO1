package ex1;
import java.util.Arrays;

public class Lion extends Carnivore {
    private int criniere;

    /**
     * @param nom
     * @param age
     * @param mange
     * @param possede
     * @param criniere
     */
    public Lion(String nom, int age, Regime mange, Zoo possede, int criniere) {
        this.nom = nom;
        this.age = age;
        this.mange = mange;
        this.possede = possede;
        this.criniere = criniere;
        this.enfants = Arrays.asList();
    }

    /**
     * @return the criniere
     */
    public int getCriniere() {
        return criniere;
    }

    /**
     * @param criniere the criniere to set
     */
    public void setCriniere(int criniere) {
        this.criniere = criniere;
    }
}
