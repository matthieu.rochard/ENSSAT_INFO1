package ex1;
import java.util.Arrays;

public class Boa extends Carnivore {
    private int longueur;

    /**
     * @param nom
     * @param age
     * @param mange
     * @param possede
     * @param longueur
     */
    public Boa(String nom, int age, Regime mange, Zoo possede, int longueur) {
        this.nom = nom;
        this.age = age;
        this.mange = mange;
        this.possede = possede;
        this.longueur = longueur;
        this.enfants = Arrays.asList();
    }

    /**
     * @return the longueur
     */
    public int getLongueur() {
        return longueur;
    }

    /**
     * @param longueur the longueur to set
     */
    public void setLongueur(int longueur) {
        this.longueur = longueur;
    }
}
