package ex1;

import java.util.Arrays;
import java.util.Iterator;
import java.util.List;

public class Zoo {
    private String nom;
    private List<Animal> possede;

    /**
     * @param nom
     */
    public Zoo(String nom) {
        this.nom = nom;
        this.possede = Arrays.asList();
    }

    int calculerNbAnimal() {
        return this.possede.size();
    }

    int calculerCarnivore(){
        int nb =0;
        Iterator<Animal> ite = this.possede.iterator();
        while(ite.hasNext()){
            if(ite.next() instanceof Carnivore){
                nb++;
            }
        }
        return nb;
    }

    int calculerVegetarien(){
        int nb =0;
        Iterator<Animal> ite = this.possede.iterator();
        while(ite.hasNext()){
            if(ite.next() instanceof Vegetarien){
                nb++;
            }
        }
        return nb;
    }

    int calculerCout() {
        int s = 0;
        Iterator<Animal> ite = this.possede.iterator();
        while (ite.hasNext()) {
            s += ite.next().getMange().calculerPrix();
        }
        return s;
    }

    boolean ajouterPossede(Animal animal) {
        animal.setPossede(this);
        return ajouterAnimal(animal);
    }

    private boolean ajouterAnimal(Animal animal) {
        return this.possede.add(animal);
    }

    boolean supprimerPossede(Animal animal) {
        if (this.possede.contains(animal)) {
            animal.setPossede(null);
            return supprimerAnimal(animal);
        } else
            return false;
    }

    private boolean supprimerAnimal(Animal animal) {
        return this.possede.remove(animal);
    }

    /**
     * @return the nom
     */
    public String getNom() {
        return nom;
    }

    /**
     * @param nom the nom to set
     */
    public void setNom(String nom) {
        this.nom = nom;
    }

    /**
     * @return the possede
     */
    public List<Animal> getPossede() {
        return possede;
    }

    /**
     * @param possede the possede to set
     */
    public void setPossede(List<Animal> possede) {
        this.possede = possede;
    }

}
