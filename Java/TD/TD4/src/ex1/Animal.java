package ex1;

import java.util.Iterator;
import java.util.List;

public abstract class Animal {
    protected String nom;
    protected int age;
    protected Regime mange;
    protected Zoo possede;
    protected List<Animal> enfants;

    public int nbDeDescendants(){
        int nb = this.enfants.size();
        Iterator<Animal> ite = this.enfants.iterator();
        while (ite.hasNext()) {
            nb+=ite.next().nbDeDescendants();
        }
        return nb;
    }

    /**
     * @return the nom
     */
    public String getNom() {
        return nom;
    }

    /**
     * @param nom the nom to set
     */
    public void setNom(String nom) {
        this.nom = nom;
    }

    /**
     * @return the age
     */
    public int getAge() {
        return age;
    }

    /**
     * @param age the age to set
     */
    public void setAge(int age) {
        this.age = age;
    }

    /**
     * @return the mange
     */
    public Regime getMange() {
        return mange;
    }

    /**
     * @param mange the mange to set
     */
    public void setMange(Regime mange) {
        this.mange = mange;
    }

    /**
     * @return the possede
     */
    public Zoo getPossede() {
        return possede;
    }

    /**
     * @param possede the possede to set
     */
    public void setPossede(Zoo possede) {
        this.possede = possede;
    }

    public List<Animal> getEnfants() {
        return enfants;
    }

    public void setEnfants(List<Animal> enfants) {
        this.enfants = enfants;
    }

    @Override
    public String toString() {
        return "Animal [age=" + age + ", enfants=" + enfants + ", mange=" + mange + ", nom=" + nom + ", possede="
                + possede + "]";
    }

    @Override
    public boolean equals(Object obj) {
        if (this == obj)
            return true;
        if (obj == null)
            return false;
        if (getClass() != obj.getClass())
            return false;
        Animal other = (Animal) obj;
        if (age != other.age)
            return false;
        if (enfants == null) {
            if (other.enfants != null)
                return false;
        } else if (!enfants.equals(other.enfants))
            return false;
        if (mange == null) {
            if (other.mange != null)
                return false;
        } else if (!mange.equals(other.mange))
            return false;
        if (nom == null) {
            if (other.nom != null)
                return false;
        } else if (!nom.equals(other.nom))
            return false;
        if (possede == null) {
            if (other.possede != null)
                return false;
        } else if (!possede.equals(other.possede))
            return false;
        return true;
    }
    
}
