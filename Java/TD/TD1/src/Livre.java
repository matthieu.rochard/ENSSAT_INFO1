public class Livre {

    public static void main(String[] args) {
        Livre l1 = new Livre("a1", "t1");
        Livre l2 = new Livre("a2", "t2");
        System.out.println(l1.getAuteur());
        System.out.println(l2.getAuteur());

        l1.setNbPages(10);
        l2.setNbPages(25);
        System.out.println(l1.getNbPages());
        System.out.println(l2.getNbPages());

        System.out.println(l1.getNbPages() + l2.getNbPages());
        System.out.println(l1);

        Livre l3 = new Livre("t3", "a3", 300, 55);
        Livre l4 = new Livre("t4", "a4");
        l4.setPrix(88);
        Livre l5 = new Livre("t5", "a5", 300, 0);
    }

    // donnees membres
    private String titre, auteur;
    private int nbPages, prix;
    private Boolean prixFixe = false;

    // Constructeur
    public Livre(String unAuteur, String unTitre) {
        auteur = unAuteur;
        titre = unTitre;
    }

    public Livre() {
    }

    /**
     * @param titre
     * @param auteur
     * @param nbPages
     */
    public Livre(String titre, String auteur, int nbPages, int prix) {
        this.titre = titre;
        this.auteur = auteur;
        this.nbPages = nbPages;
        this.prix = prix;
        this.prixFixe = true;
    }

    public void afficheToi() {
        System.out.println("Title: " + this.getTitre());
        System.out.println("Auteur: " + this.getAuteur());
        System.out.println("NbPages: " + this.getNbPages());
    }

    public String toString() {
        String str = "Livre [auteur=" + auteur + ", nbPages=" + nbPages + ", titre=" + titre;
        if (this.prixFixe) {
            str = str.concat(", prix=" + prix);
        } else {
            str = str.concat(", prix pas encore fixé]");
        }
        return str;
    }

    // accesseur
    public String getAuteur() {
        return auteur;
    }

    /**
     * @return the titre
     */
    public String getTitre() {
        return titre;
    }

    /**
     * @return the nbPages
     */
    public int getNbPages() {
        return nbPages;
    }

    /**
     * @return the prix
     */
    public int getPrix() {
        return prix;
    }

    // modificateur
    public void setNbPages(int nb) {
        if (nb > 0) {
            nbPages = nb;
        } else {
            System.out.println("Error");
        }
    }

    /**
     * @param prix the prix to set
     */
    public void setPrix(int prix) {
        if (!this.prixFixe) {
            this.prix = prix;
            this.prixFixe = true;
        } else {
            System.out.println("Error");
        }
    }

    /**
     * @param titre the titre to set
     */
    public void setTitre(String titre) {
        this.titre = titre;
    }

    /**
     * @param auteur the auteur to set
     */
    public void setAuteur(String auteur) {
        this.auteur = auteur;
    }

    public Boolean isPrixFixe() {
        return prixFixe;
    }

}