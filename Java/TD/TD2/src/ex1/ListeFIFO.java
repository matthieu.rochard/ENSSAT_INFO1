package ex1;

public class ListeFIFO {
    public Element debut;

    public ListeFIFO() {
        debut = null;
    }

    public boolean estVide() {
        return debut == null;
    }

    public void afficher() {
        if (this.estVide()) {
            System.out.println("La liste est vide");
        } else {
            Element elt = debut;
            do {
                System.out.print(elt.getValeur() + ",");
                elt = elt.getSuivant();
            } while (elt != null);
            System.out.println("");
        }
    }

    public void empiler(int val) {
        Element elt = debut;
        if (this.estVide()) {
            this.debut = new Element(val);
        } else {
            while (elt.getSuivant() != null) {
                elt = elt.getSuivant();
            }
            elt.setSuivant(new Element(val));
        }
    }

    public int depiler() {
        int val = -1;
        Element elt = this.debut;
        if (!this.estVide()) {
            while (elt.getSuivant() != null) {
                elt = elt.getSuivant();
            }
            val = debut.getValeur();
            this.debut = this.debut.getSuivant();
        }
        return val;
    }

    public int count() {
        Element elt = debut;
        int counter = 0;
        while (elt != null) {
            counter++;
            elt = elt.getSuivant();
        }
        return counter;
    }

    public static void main(String[] args) {
        ListeFIFO liste = new ListeFIFO();
        liste.empiler(0);
        liste.empiler(1);
        liste.empiler(2);
        liste.empiler(3);
        liste.empiler(4);
        liste.afficher();
        liste.depiler();
        liste.depiler();
        liste.afficher();
        liste.empiler(10);
        liste.empiler(20);
        liste.empiler(30);
        liste.empiler(40);
        liste.empiler(50);
        liste.afficher();
        while (!liste.estVide()) {
            liste.depiler();
        }
        liste.afficher();
    }
}