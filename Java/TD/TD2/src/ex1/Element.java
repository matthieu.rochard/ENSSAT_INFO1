package ex1;

public class Element {
    private int valeur;
    private Element suivant;

    /**
     * @param valeur
     */
    public Element(int valeur) {
        this.valeur = valeur;
        this.suivant = null;
    }

    /**
     * @return the valeur
     */
    public int getValeur() {
        return valeur;
    }

    /**
     * @param valeur the valeur to set
     */
    public void setValeur(int valeur) {
        this.valeur = valeur;
    }

    /**
     * @return the suivant
     */
    public Element getSuivant() {
        return suivant;
    }

    /**
     * @param suivant the suivant to set
     */
    public void setSuivant(Element suivant) {
        this.suivant = suivant;
    }

}