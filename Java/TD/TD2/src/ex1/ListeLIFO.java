package ex1;

public class ListeLIFO {
    public Element debut;

    public ListeLIFO() {
        debut = null;
    }

    public boolean estVide() {
        return debut == null;
    }

    public void afficher() {
        if (this.estVide()) {
            System.out.println("La liste est vide");
        } else {
            Element elt = debut;
            do {
                System.out.print(elt.getValeur() + ",");
                elt = elt.getSuivant();
            } while (elt != null);
            System.out.println("");
        }
    }

    public void empiler(int val) {
        Element elt = new Element(val);
        if (this.estVide()) {
            this.debut = elt;
        } else {
            elt.setSuivant(this.debut);
            this.debut = elt;
        }
    }

    public int depiler() {
        int val = -1;
        if (!this.estVide()) {
            val = debut.getValeur();
            this.debut = this.debut.getSuivant();
        }
        return val;
    }

    public int count() {
        Element elt = debut;
        int counter = 0;
        while (elt != null) {
            counter++;
            elt = elt.getSuivant();
        }
        return counter;
    }

    public static void main(String[] args) {
        ListeLIFO liste = new ListeLIFO();
        liste.empiler(0);
        liste.empiler(1);
        liste.empiler(2);
        liste.empiler(3);
        liste.empiler(4);
        liste.afficher();
        liste.depiler();
        liste.depiler();
        liste.afficher();
        liste.empiler(10);
        liste.empiler(20);
        liste.empiler(30);
        liste.empiler(40);
        liste.empiler(50);
        liste.afficher();
        while (!liste.estVide()) {
            liste.depiler();
        }
        liste.afficher();
    }
}