//--------------------------------------------------------------------------//
// TP Microcontrôleurs ASM - ENSSAT SNum/Info  2019 - A.Courtay   					//
//--------------------------------------------------------------------------//
// Codage Suite Fibonacci: Iterative (ASM+C) / Recursive (C)								//
//--------------------------------------------------------------------------//

//----------------------------------------------------------------------------
// Fichiers Inclus
//----------------------------------------------------------------------------
#include "lpc17xx_libcfg_default.h"

//----------------------------------------------------------------------------
// Prototype des fonctions
//----------------------------------------------------------------------------
__asm long Iterative_Fibonacci_ASM (int n);
long Iterative_Fibonacci_C(int n);
long Recursive_Fibonacci_C(int n);

//----------------------------------------------------------------------------
// Variables globales
//----------------------------------------------------------------------------
int n = 3;
int Resultat;

//----------------------------------------------------------------------------
// Fonction principale
//----------------------------------------------------------------------------
int main()
{	
  
	Resultat = Recursive_Fibonacci_C(n);
	
	while(1);
}

//----------------------------------------------------------------------------
// Fibonacci Iterative en ASM
//----------------------------------------------------------------------------
__asm long Iterative_Fibonacci_ASM (int n) 
{
// - Input  : dans les registres d ordre croissant, donc ici: R0 = n
// - Output : dans les registres d ordre croissant, donc ici: R0 = Resultat
	
// Sauvegarde du contexte					
				STMFD  SP,{R1,R2,R3,R4,lr}
// Initialisation
				
				MOV R1,#0x00 //r1=0
				MOV R2,#0x01	//r2=1
				MOV R3,#0x00 //tmp=0
				MOV R4,#0x01	//i=1
// Boucle
boucle	CMP R4,R0
				BGT end //si R3>R4 jmp to end
				ADD R3,R1,R2
				MOV R1,R2
				MOV R2,R3
				ADD R4,#0x01
				B   boucle	//jmp to start boucle
// Restitution du contexte						
end			MOV R0,R1
				LDMFD SP,{R1,R2,R3,R4,pc}
// D apres les convention de passage des parametres ARM

	
// 
}

//----------------------------------------------------------------------------
// Fibonacci Iterative en C
//----------------------------------------------------------------------------
long Iterative_Fibonacci_C (int n)
{
  long	r1 = 0,r2 = 1,tmp=0;
	int i;
	for(i=1;i<=n;i++){
		tmp=r1 + r2;
		r1 = r2;
		r2=tmp;
	}
	return r1;
}

//----------------------------------------------------------------------------
// Fibonacci Recursive en C
//----------------------------------------------------------------------------
long Recursive_Fibonacci_C (int n)
{
	if(1==n || 2==n) return 1;
	else{ return Recursive_Fibonacci_C(n-1)+ Recursive_Fibonacci_C(n-2); }
}

//----------------------------------------------------------------------------
//----------------------------------------------------------------------------
#ifdef  DEBUG
void check_failed(uint8_t *file, uint32_t line) {while(1);}
#endif
