//--------------------------------------------------------------------------//
// TD2 Microcontr�leurs - A.Courtay																					//
//--------------------------------------------------------------------------//
// Fonction principale	- Echantillonnage ADC par Timer et interruption			//
//--------------------------------------------------------------------------//

//----------------------------------------------------------------------------
// Fichiers Inclus
//----------------------------------------------------------------------------
#include "lpc17xx_gpio.h"
#include "lpc17xx_libcfg_default.h"

//----------------------------------------------------------------------------
// Variables et Constantes globales
//----------------------------------------------------------------------------

// Structure sur le GPIO2: 											LPC_GPIO2
// Structure sur le TIMER1:											LPC_TIM1
// Structure sur l'ADC:													LPC_ADC
// Structure sur le PINSEL:											LPC_PINCON
// Structure sur le PCONP:											LPC_SC
// Structure sur le controleur d'interruption: 	NVIC
// Faire un click droit sur la structure et faire Goto Definition pour voir 
// le contenu de la structure

//----------------------------------------------------------------------------
// Fonction principale
//----------------------------------------------------------------------------
int main (void)
{
	//GPIO
	LPC_GPIO2->FIODIR |= (1<<3)|(1<<4)|(1<<5); //mise en output
	LPC_GPIO2->FIOMASK &= ~((1<<3)|(1<<4)|(1<<5)); //maskage
	
	//Timer
	LPC_TIM0->CTCR &= ~((1<<1)|(1<<0)); //en mode TIMER
	LPC_TIM0->EMR=(1<<6)|(1<<7);	 //mode toggle
	LPC_TIM0->MCR =(1<<4);	//remise � 0 de TC lors de match
	LPC_TIM0->PR = 25000;		//1 ms
	LPC_TIM0->MR1 = 999;		// toggle tout les .5s donc activation tout les seconds
	/**LPC_PINCON->PINSEL3 &= (1<<19)|(1<<18); //P1.25 sort MAT1*/
	
	//ADC
	LPC_SC->PCONP |= (1<<12); //enable ADC
	LPC_PINCON->PINSEL1 |= (1<<14); //selection AD0.0 sur P0.23
	LPC_ADC->ADCR |= (1<<0); //AD0.0 pin sera sampled
	LPC_ADC->ADCR |= (1<<8); //fr�quence 12,5MHz
	LPC_ADC->ADCR |= (1<<21); //A/D is operatuinal
	LPC_ADC->ADCR |= (1<<24)|(1<<25)|(1<<26); //start quand edge sur MAT1
	LPC_ADC->ADCR |= (1<<27); //l'edge attendu est rising
	
	//Interrupt
	NVIC->ISER[0] = (1<<22); //enable ADC interrupt
	LPC_ADC->ADINTEN |= (1<<0); //channel 0 completion generates interrupt
			
	LPC_TIM0->TCR |=(1<<0); //activation du timer
	while(1)
	{
		// rien
	}
	return 1;
}
	

//----------------------------------------------------------------------------
// Traitement de l interruption
//----------------------------------------------------------------------------
void ADC_IRQHandler(void){
	int Temperature;
	Temperature = (LPC_ADC->ADGDR>>4) & 0x00000FFF;
	
	if(Temperature<15){
		LPC_GPIO2->FIOPIN = (1<<5);
	} else	if(Temperature>20 ){
		LPC_GPIO2->FIOPIN = (1<<3);
	} else {
		LPC_GPIO2->FIOPIN = (1<<4);
	}
	//aquitement automatique
}

//----------------------------------------------------------------------------
#ifdef  DEBUG
void check_failed(uint8_t *file, uint32_t line) {while(1);}
#endif
