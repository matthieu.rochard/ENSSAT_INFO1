; Programme assembleur correspondant � l'analyse de similitude entre deux vecteurs
; Entr�es R0:adresse de X, R1 : adresse de Y, R2:adresse de Z, R3 : Nombre de points
; Sortie R0:nombre de points dans l'intervalle (nbsimili)

PUSH     {r4-r7,lr} 		; sauvegarde du contexte (�quivalent � STMFD SP!,{R4-R7,LR}
MOV      r4,r0			; sauvegarde de l'adresse de X
MOV      r5,r1			; sauvegarde de l'adresse de Y
MOVS     r0,#0x00			; initialisation de R0 (nbsimili)
		MOVS     r1,#0x00			; initialisation de R1 (variable de boucle i)
		B        0x000001D4 (branch)	; branchement pour v�rifier la variable de boucle
loop		LDR      r6,[r4,r1,LSL #2]	; chargement de X[i] avec incr�mentation de l'adresse grace � R1
		LDR      r7,[r5,r1,LSL #2]	; chargement de Y[i] "
		SUBS     r6,r6,r7			; Z[i]=X[i]-Y[i]
		STR      r6,[r2,r1,LSL #2]	; Sauvegarde de Z[i] "
CMP      r6,#0x03			; Comparison avec VALMAX
		BGE      0x000001D2 (branch2)	; Si Z[i] est plus grand alors pas d'incr�mentation de nbsimili
		CMN      r6,#0x03			; Sinon comparaison avec VALMIN
BLE      0x000001D2 (branch2)	; Si Z[i] est plus petit alors pas d'incr�mentation de nbsimili
		ADDS     r0,r0,#1			; Sinon Z[i] est dans l'intervalle et on incr�mente nbsimili
branch2 	ADDS     r1,r1,#1			; On incr�mente la variable de boucle
branch 	CMP      r1,r3			; Comparaison avec la taille des vecteurs
		BLT      0x000001B0 (loop)	; S'il reste encore des points � analyser on reboucle
		POP      {r4-r7,pc}		; Sinon restauration du contexte et chargement de LR dans PC