//--------------------------------------------------------------------------//
// TD2 Microcontrôleurs - A.Courtay																					//
//--------------------------------------------------------------------------//
// Fonction principale	- Gestion clavier en Interruption										//
//--------------------------------------------------------------------------//

//----------------------------------------------------------------------------
// Fichiers Inclus
//----------------------------------------------------------------------------
#include "lpc17xx_gpio.h"
#include "lpc17xx_libcfg_default.h"    

//----------------------------------------------------------------------------
// Variables et Constantes globales
//----------------------------------------------------------------------------
char Code[4];								// Tableau contenant les 4 caracteres
unsigned char End_Code = 0; // Flag indiquant tableau plein
int j = 0;									// Indice du tableau

// Structure sur le GPIO0: 											LPC_GPIO0
// Structure sur les Interruptions GPIO: 				LPC_GPIOINT
// Structure sur le controleur d'interruption: 	NVIC
// Faire un click droit sur la structure et faire Goto Definition pour voir 
// le contenu de la structure
//----------------------------------------------------------------------------
// Fonction principale
//----------------------------------------------------------------------------
int main(void)
{
	LPC_GPIO0->FIOMASK = ~((1<<6)|(1<<5)|(1<<4)|(1<<3)|(1<<2)|(1<<1)|(1<<0));
	LPC_GPIO0->FIODIR = (1<<4)|(1<<5)|(1<<6);
	LPC_GPIOINT->IO0IntEnF = (1<<0)|(1<<1)|(1<<2)|(1<<3);
	NVIC->ISER[0] = (1<<21);
	
	LPC_GPIO0->FIOCLR = (1<<4)|(1<<5)|(1<<6);
	while(1)
	{

	}
}

//----------------------------------------------------------------------------
// Traitement de l interruption
//----------------------------------------------------------------------------
void EINT3_IRQHandler(void)
{
	int i;
	
	for(i=4;i<7;i++){
		LPC_GPIO0->FIOSET = (1<<4)|(1<<5)|(1<<6);
		LPC_GPIO0->FIOCLR = (1<<i);
		
		switch(LPC_GPIO0->FIOPIN & 0x0000000F){
			case 0x0E: 
				switch(i){
					case 4:	
						Code[j] = 1; j++;
						break;
					case 5: 
						Code[j] = 2; j++;
						break;
					case 6: 
						Code[j] = 3; j++;
						break;
				}
				break;
			case 0x0D:
				switch(i){
					case 4: 
						Code[j] = 4; 
						j++;
						break;
					case 5: 
						Code[j] = 5;
						j++;
						break;
					case 6: 
						Code[j] = 6; 
						j++;
						break;
				}
				break;
			case 0x0B:
				switch(i){
					case 4: 
						Code[j] = 7; 
						j++;
						break;
					case 5: 
						Code[j] = 8; 
						j++;
						break;
					case 6: 
						Code[j] = 9; 
						j++;
						break;
				}
				break;
			case 0x07:
				switch(i){
					case 4:
						End_Code=1;
						j=0;
						break;
					case 5: 
						Code[j] = 0; 
						j++;
						break;
					case 6: 
						j=0;
						break;
				}
				break;
		}
		if(j>=4)
		{
			End_Code=1;
		}
		if(End_Code==1)
		{
			break;
		}
		
	}
	LPC_GPIO0->FIOCLR = (1<<4)|(1<<5)|(1<<6);
	LPC_GPIOINT->IO0IntClr |=(1<<0)|(1<<1)|(1<<2)|(1<<3);
	
}

//----------------------------------------------------------------------------
#ifdef  DEBUG
void check_failed(uint8_t *file, uint32_t line) {while(1);}
#endif
