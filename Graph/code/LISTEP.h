#include <stdbool.h>
#include <stdlib.h>

struct LISTEP
{
    void *val;
    struct LISTEP *lien;
};

void LISTEP_inserer(struct LISTEP **pp_liste, int pos, void *p_element, bool *p_err);
void LISTEP_supprimer(struct LISTEP **pp_liste, int pos, bool *p_err);
int LISTEP_rechercher(struct LISTEP *p_liste, void *p_element);
void LISTEP_selectionner(struct LISTEP *p_liste, int pos, void **pp_element, bool *p_err);
void LISTEP_afficher(struct LISTEP *p_liste);
void LISTEP_liberer(struct LISTEP *p_liste);