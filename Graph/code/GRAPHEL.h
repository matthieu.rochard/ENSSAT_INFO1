#include <stdbool.h>
#include "LISTET.h"
#include "LISTEP.h"

/*********************************************
 * TYPES
 *********************************************/

typedef struct LISTET GRAPHE;

struct SOMMET_ADJ
{
    int id;
    struct LISTEP *successeurs;
};

typedef struct SOMMET_ADJ* SOMMET;

struct DESTINATION_VALEUR {
    int destination;
    float valeur;
};

struct ARC
{
    int source;
    int destination;
    float valeur;
};


/*********************************************
 * OPERATIONS GÉNÉRALES
 *********************************************/

GRAPHE *GRAPHE_creer();
void GRAPHE_liberer(GRAPHE *graphe);
void GRAPHE_afficher_liste(GRAPHE graphe);


/*********************************************
 * OPERATIONS SUR LES SOMMETS
 *********************************************/

int GRAPHE_dernier_sommet(GRAPHE graphe);
bool GRAPHE_existe_sommet(GRAPHE graphe, int sommet_id);
SOMMET GRAPHE_lire(GRAPHE graphe, int sommet_id, bool *p_err);
struct LISTEP *GRAPHE_lister_sommets(GRAPHE graphe);
SOMMET GRAPHE_inserer_nouveau_sommet(GRAPHE *graphe, bool *p_err);
SOMMET GRAPHE_inserer_sommet_existant(GRAPHE *graphe, SOMMET sommet, bool *p_err);


/*********************************************
 * OPERATIONS SUR LES SOMMETS
 *********************************************/

bool GRAPHE_existe_arc(GRAPHE graphe, int source_id, int destination_id);
struct LISTEP *GRAPHE_lister_successeurs(GRAPHE graphe, int source_id, bool *p_err);
struct LISTEP *GRAPHE_lister_arcs(GRAPHE graphe);
void GRAPHE_inserer_arc(GRAPHE *graphe, struct ARC arc, bool *p_err);


/*********************************************
 * FONCTIONS INTERNES À LA BIBLIOTHÈQUE
 *********************************************/

SOMMET SOMMET_creer(int id);
void SOMMET_liberer(SOMMET p_sommet);
void SOMMET_ajouter_successeur(SOMMET p_sommet, int destination, float valeur, bool *p_err);
bool SOMMET_existe_successeur(SOMMET source, int destination_id);
