#include <stdbool.h>
#include <stdio.h>
#include "GRAPHEM.h"

/*********************************************
 * OPERATIONS GÉNÉRALES
 *********************************************/

GRAPHE *GRAPHE_creer()
{
    GRAPHE *g = (GRAPHE *)malloc(sizeof(GRAPHE));
    for (int i = 0; i < MAX_SOMMETS; i++)
    {
        g->sommets[i] = false;
        for (int j = 0; j < MAX_SOMMETS; j++)
        {
            g->arcs[i][j] = false;
        }
    }
    return g;
}

void GRAPHE_liberer(GRAPHE *graphe)
{
    free(graphe);
}

void GRAPHE_afficher_liste(GRAPHE graphe)
{
    // Pour chaque sommet potentiel
    for (int i = 0; i < MAX_SOMMETS; i++)
    {
        if (graphe.sommets[i])
        {
            printf("SOMMET %i : { ", i + 1);
            // Pour chaque successeur potentiel
            for (int j = 0; j < MAX_SOMMETS; j++)
            {
                if (graphe.arcs[i][j])
                {
                    printf("(dest=%i, val=%f) ", j + 1, graphe.valeurs[i][j]);
                }
            }
            printf("}\n");
        }
    }
}

/*********************************************
 * OPERATIONS SUR LES SOMMETS
 *********************************************/

int GRAPHE_dernier_sommet(GRAPHE graphe)
{
    int id_sommet = 0;
    for (int i = 0; i < MAX_SOMMETS; i++)
    {
        if (graphe.sommets[i])
        {
            id_sommet = i + 1;
        }
    }
    return id_sommet;
}

bool GRAPHE_existe_sommet(GRAPHE graphe, int sommet_id)
{
    return (sommet_id > 0 && sommet_id - 1 < MAX_SOMMETS && graphe.sommets[sommet_id - 1]);
}

SOMMET GRAPHE_lire(GRAPHE graphe, int sommet_id, bool *p_err)
{
    if (!GRAPHE_existe_sommet(graphe, sommet_id))
    {
        *p_err = true;
        return 0;
    }
    else
    {
        *p_err = false;
        return graphe.sommets[sommet_id - 1];
    }
}

struct LISTEP *GRAPHE_lister_sommets(GRAPHE graphe)
{
    struct LISTEP *liste = NULL;
    SOMMET sommet;
    bool err = false;
    int nb = 0;
    for (int i = 0; i < MAX_SOMMETS; i++)
    {
        if (graphe.sommets[i])
        {
            int *sommet_id = (int *)malloc(sizeof(int));
            *sommet_id = sommet;
            LISTEP_inserer(&liste, nb + 1, sommet_id, &err);
            nb++;
        }
    }
    return liste;
}

SOMMET GRAPHE_inserer_nouveau_sommet(GRAPHE *graphe, bool *p_err)
{
    // Chercher le plus petit identifiant non encore utilisé
    int nouveau_id = 0;
    for (int i = 0; i < MAX_SOMMETS && nouveau_id == 0; i++)
    {
        if (!graphe->sommets[i])
        {
            nouveau_id = i + 1;
        }
    }

    // Si identifiant trouvé, le réserver
    if (nouveau_id > 0)
    {
        *p_err = false;
        graphe->sommets[nouveau_id - 1] = true;
    }
    // Erreur si aucun identifiant trouvé
    else
    {
        *p_err = true;
    }
}

SOMMET GRAPHE_inserer_sommet_existant(GRAPHE *graphe, SOMMET sommet, bool *p_err)
{
    // Si ID du sommet déjà existant, alors erreur
    if (GRAPHE_existe_sommet(*graphe, sommet))
    {
        *p_err = true;
        return sommet;
    }
    else if (sommet - 1 < MAX_SOMMETS)
    {
        *p_err = false;
        graphe->sommets[sommet - 1] = true;
        return sommet;
    }
    else
    {
        *p_err = true;
        return 0;
    }
}

void GRAPHE_supprimer_sommet(GRAPHE *graphe, SOMMET sommet)
{
    if (sommet < MAX_SOMMETS)
    {
        graphe->sommets[sommet - 1] = false;
    }
}

/*********************************************
 * OPERATIONS SUR LES ARCS
 *********************************************/

bool GRAPHE_existe_arc(GRAPHE graphe, int source_id, int destination_id)
{
    if (GRAPHE_existe_sommet(graphe, source_id) && GRAPHE_existe_sommet(graphe, destination_id))
    {
        return graphe.arcs[source_id][destination_id];
    }
    else
    {
        return false;
    }
}

struct LISTEP *GRAPHE_lister_successeurs(GRAPHE graphe, int source_id, bool *p_err)
{
    struct LISTEP *liste = NULL;
    struct DESTINATION_VALEUR *successeur_courant;
    bool err = false;
    if (GRAPHE_existe_sommet(graphe, source_id))
    {
        *p_err = false;
        for (int j = 0; j < MAX_SOMMETS; j++)
        {
            if (graphe.arcs[source_id - 1][j])
            {
                successeur_courant = (struct DESTINATION_VALEUR *)malloc(sizeof(struct DESTINATION_VALEUR));
                successeur_courant->destination = j + 1;
                successeur_courant->valeur = graphe.valeurs[source_id - 1][j];
                LISTEP_inserer(&liste, 1, successeur_courant, &err);
            }
        }
        return liste;
    }
    else
    {
        *p_err = true;
        return NULL;
    }
}

struct LISTEP *GRAPHE_lister_arcs(GRAPHE graphe)
{
    struct LISTEP *liste = NULL;
    struct ARC *arc_courant;
    bool err = false;
    for (int i = MAX_SOMMETS - 1; i >= 0; i--)
    {
        if (graphe.sommets[i])
        {
            for (int j = 0; j < MAX_SOMMETS; j++)
            {
                if (graphe.sommets[j])
                {
                    if (graphe.arcs[i][j])
                    {
                        arc_courant = (struct ARC *)malloc(sizeof(struct ARC));
                        arc_courant->source = i + 1;
                        arc_courant->destination = j + 1;
                        arc_courant->valeur = graphe.valeurs[i][j];
                        LISTEP_inserer(&liste, 1, arc_courant, &err);
                    }
                }
            }
        }
    }
    return liste;
}

void GRAPHE_inserer_arc(GRAPHE *graphe, struct ARC arc, bool *p_err)
{
    // Obtenir le sommet source (le créer si nécessaire)
    if (!GRAPHE_existe_sommet(*graphe, arc.source))
    {
        GRAPHE_inserer_sommet_existant(graphe, arc.source, p_err);
        if (*p_err)
        {
            return;
        }
    }
    // Créer le sommet destination si nécessaire
    if (!GRAPHE_existe_sommet(*graphe, arc.destination))
    {
        GRAPHE_inserer_sommet_existant(graphe, arc.destination, p_err);
        if (*p_err)
        {
            return;
        }
    }
    graphe->arcs[arc.source - 1][arc.destination - 1] = true;
    graphe->valeurs[arc.source - 1][arc.destination - 1] = arc.valeur;
}

void GRAPHE_supprimer_arc(GRAPHE *graphe, struct ARC arc)
{
    if (arc.source < MAX_SOMMETS && arc.destination < MAX_SOMMETS)
    {
        graphe->arcs[arc.source - 1][arc.destination - 1] = false;
    }
}
