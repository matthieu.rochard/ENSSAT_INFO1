#include <stdbool.h>
#define LMAX 20

struct LISTET
{
    void* t[LMAX];
    int longueur;
};

void LISTET_inserer(struct LISTET *p_liste, int pos, void *p_element, bool *p_err);
void LISTET_supprimer(struct LISTET *p_liste, int pos, bool *p_err);
int LISTET_rechercher(struct LISTET liste, void *p_element);
void LISTET_selectionner(struct LISTET liste, int pos, void **pp_element, bool *p_err);
void LISTET_afficher(struct LISTET liste);
