#include "LISTET.h"
#include "LISTEP.h"

#define MAX_SOMMETS 50

/*********************************************
 * TYPES
 *********************************************/

typedef int SOMMET;

struct GRAPHEM
{
    bool arcs[MAX_SOMMETS][MAX_SOMMETS];
    float valeurs[MAX_SOMMETS][MAX_SOMMETS];
    bool sommets[MAX_SOMMETS];
};

typedef struct GRAPHEM GRAPHE;

struct DESTINATION_VALEUR
{
    int destination;
    float valeur;
};

struct ARC
{
    SOMMET source;
    SOMMET destination;
    float valeur;
};

/*********************************************
 * OPERATIONS GÉNÉRALES
 *********************************************/

GRAPHE *GRAPHE_creer();
void GRAPHE_liberer(GRAPHE *graphe);
void GRAPHE_afficher_liste(GRAPHE graphe);

/*********************************************
 * OPERATIONS SUR LES SOMMETS
 *********************************************/

int GRAPHE_dernier_sommet(GRAPHE graphe);
bool GRAPHE_existe_sommet(GRAPHE graphe, int sommet_id);
SOMMET GRAPHE_lire(GRAPHE graphe, int sommet_id, bool *p_err);
struct LISTEP *GRAPHE_lister_sommets(GRAPHE graphe);
SOMMET GRAPHE_inserer_nouveau_sommet(GRAPHE *graphe, bool *p_err);
SOMMET GRAPHE_inserer_sommet_existant(GRAPHE *graphe, SOMMET sommet, bool *p_err);

/*********************************************
 * OPERATIONS SUR LES SOMMETS
 *********************************************/

bool GRAPHE_existe_arc(GRAPHE graphe, int source_id, int destination_id);
struct LISTEP *GRAPHE_lister_successeurs(GRAPHE graphe, int source_id, bool *p_err);
struct LISTEP *GRAPHE_lister_arcs(GRAPHE graphe);
void GRAPHE_inserer_arc(GRAPHE *graphe, struct ARC arc, bool *p_err);
