#include <stdio.h>
#include "LISTEP.h"

void LISTEP_inserer(struct LISTEP **pp_liste, int pos, void *p_element, bool *p_err)
{
  int i;
  struct LISTEP *p_maillon;
  struct LISTEP *p_maillon_tmp;
  p_maillon = (struct LISTEP *)malloc(sizeof(struct LISTEP));
  if (p_maillon != NULL)
  {
    *p_err = false;             /* a priori on peut inserer */
    p_maillon->val = p_element; /* on affecte elt au nouveau maillon */
    if (pos == 1)
    { /* on insere en tete */
      p_maillon->lien = *pp_liste;
      *pp_liste = p_maillon;
    }
    else
    { /* sinon on va au maillon pos-1, si possible */
      p_maillon_tmp = *pp_liste;
      for (i = 1; i < pos - 1; i++)
      {
        if (p_maillon_tmp != NULL)
        {
          p_maillon_tmp = p_maillon_tmp->lien;
        }
        else
        { /* fin de liste prematuree */
          *p_err = true;
          break;
        }
      }
      if (!*p_err)
      { /* on insere le maillon dans la chaine */
        p_maillon->lien = p_maillon_tmp->lien;
        p_maillon_tmp->lien = p_maillon;
      }
    }
  }
  else
  { /* plus de memoire */
    *p_err = true;
  }
}

void LISTEP_supprimer(struct LISTEP **pp_liste, int pos, bool *p_err)
{
  int i = 1;
  struct LISTEP *p_maillon = *pp_liste;
  struct LISTEP *p_maillon_tmp;
  if (*pp_liste == NULL)
  { /* erreur si liste vide */
    *p_err = true;
  }
  else if (pos == 1)
  { /* suppression en tete */
    *pp_liste = (*pp_liste)->lien;
    free(p_maillon);
    *p_err = false;
  }
  else
  { /* on va a pos-1, si possible */
    *p_err = false;
    while (!*p_err && i < pos - 1)
    {
      p_maillon = p_maillon->lien;
      if (p_maillon != NULL)
      {
        i++;
      }
      else
      {
        *p_err = true;
      }
    } /* si pas d'erreur on supprime a pos */
    if (!*p_err && p_maillon->lien != NULL)
    {
      p_maillon_tmp = p_maillon->lien;
      p_maillon->lien = p_maillon_tmp->lien;
      free(p_maillon_tmp);
    }
    else
    {
      *p_err = true;
    }
  }
}

int LISTEP_rechercher(struct LISTEP *p_liste, void *p_element)
{
  struct LISTEP *p_maillon = p_liste;
  int i = 1;
  /* on avance jusqu'a trouver l'elt */
  while (p_maillon != NULL)
  {
    /* on a trouve */
    if (p_maillon->val == p_element)
    {
      return i;
    }
    p_maillon = p_maillon->lien;
    i++;
  }
  /* on n'a pas trouve */
  return 0;
}

void LISTEP_selectionner(struct LISTEP *p_liste, int pos, void **p_element, bool *p_err)
{
  int i = 1;
  struct LISTEP *p_maillon = p_liste;
  /* on va a pos, si possible */
  while (p_maillon != NULL && i < pos)
  {
    p_maillon = p_maillon->lien;
    i++;
  }
  /* on peut selectionner */
  if (p_maillon != NULL)
  {
    *p_element = p_maillon->val;
    *p_err = false;
  }
  /* on ne peut pas selectionner */
  else
  {
    *p_err = true;
  }
}

void LISTEP_afficher(struct LISTEP *p_liste)
{
  while (p_liste != NULL)
  {
    printf("%p ", p_liste->val);
    p_liste = p_liste->lien;
  }
  printf("\n");
}

void LISTEP_liberer(struct LISTEP *p_liste)
{
  struct LISTEP *p_liste_tmp;
  while (p_liste != NULL)
  {
    p_liste_tmp = p_liste;
    p_liste = p_liste->lien;
    free(p_liste_tmp->val);
    free(p_liste_tmp);
  }
}
