#include <stdio.h>
#include "GRAPHEL.h"



/*********************************************
 * OPERATIONS GÉNÉRALES
 *********************************************/

GRAPHE *GRAPHE_creer()
{
    GRAPHE *g = (GRAPHE *)malloc(sizeof(GRAPHE));
    return g;
}

void GRAPHE_liberer(GRAPHE *graphe)
{
    SOMMET sommet = NULL;
    bool err = false;
    for (int i = 0; i < graphe->longueur; i++)
    {
        sommet = GRAPHE_lire(*graphe, i, &err);
        if (!err)
        {
            SOMMET_liberer(sommet);
        }
    }
    free(graphe);
}



void GRAPHE_afficher_liste(GRAPHE graphe)
{
    struct LISTEP *liste_sommets = GRAPHE_lister_sommets(graphe);
    SOMMET sommet_courant = NULL;
    while (liste_sommets != NULL)
    {
        sommet_courant = (SOMMET )liste_sommets->val;
        struct LISTEP *successeurs = sommet_courant->successeurs;
        struct DESTINATION_VALEUR *successeur_courant;
        printf("SOMMET %i : { ", sommet_courant->id);
        while (successeurs != NULL)
        {
            successeur_courant = (struct DESTINATION_VALEUR *)successeurs->val;
            printf("(dest=%i, val=%f) ", successeur_courant->destination, successeur_courant->valeur);
            successeurs = successeurs->lien;
        }
        printf("}\n");
        // Passer au prochain sommet
        liste_sommets = liste_sommets->lien;
    }
    LISTEP_liberer(liste_sommets);
}


/*********************************************
 * OPERATIONS SUR LES SOMMETS
 *********************************************/

int GRAPHE_dernier_sommet(GRAPHE graphe)
{
    return graphe.longueur;
}

bool GRAPHE_existe_sommet(GRAPHE graphe, int sommet_id)
{
    bool err = false;
    GRAPHE_lire(graphe, sommet_id, &err);
    return !err;
}

SOMMET GRAPHE_lire(GRAPHE graphe, int sommet_id, bool *p_err)
{
    SOMMET sommet = NULL;
    *p_err = false;
    LISTET_selectionner(graphe, sommet_id, (void **)&sommet, p_err);
    if (!*p_err && sommet == NULL)
    {
        *p_err = true;
    }
    return sommet;
}


struct LISTEP *GRAPHE_lister_sommets(GRAPHE graphe)
{
    struct LISTEP *liste = NULL;
    SOMMET sommet = NULL;
    bool err = false;
    int nb = 0;
    for (int i = 0; i < graphe.longueur; i++)
    {
        sommet = GRAPHE_lire(graphe, i, &err);
        if (!err)
        {
            LISTEP_inserer(&liste, nb + 1, sommet, &err);
            nb++;
        }
    }
    return liste;
}



SOMMET GRAPHE_inserer_nouveau_sommet(GRAPHE *graphe, bool *p_err)
{
    int nouveau_id = 0;
    // Trouver le premier identifiant non encore utilisé
    for (int i = 0; i < graphe->longueur && nouveau_id == 0; i++) {
        if (graphe->t[i] == NULL) {
            nouveau_id = i+1;
        }
    }
    if (nouveau_id == 0) {
        nouveau_id = graphe->longueur + 1;
    }
    // Créer le sommet
    SOMMET nouveau_sommet = SOMMET_creer(nouveau_id);
    LISTET_inserer(graphe, nouveau_id, nouveau_sommet, p_err);
    // Erreur si la capacité maximal en nombre de sommets est atteinte
    if (*p_err)
    {
        SOMMET_liberer(nouveau_sommet);
        return NULL;
    }
    else
    {
        return nouveau_sommet;
    }
}



SOMMET GRAPHE_inserer_sommet_existant(GRAPHE *graphe, SOMMET p_sommet, bool *p_err)
{
    // Si ID du sommet déjà existant, alors erreur notée et retourne le sommet actuel
    if (GRAPHE_existe_sommet(*graphe, p_sommet->id))
    {
        *p_err = true;
        bool tmp_err;
        return GRAPHE_lire(*graphe, p_sommet->id, &tmp_err);
    }
    else
    {
        // Remplir les sommets manquants avec des sommets nuls (factices)
        for (int i = graphe->longueur; i < p_sommet->id - 1; i++) {
            LISTET_inserer(graphe, i+1, NULL, p_err);
        }
        // Insérer le sommet voulu
        LISTET_inserer(graphe, p_sommet->id, p_sommet, p_err);
        return p_sommet;
    }
}


/*********************************************
 * OPERATIONS SUR LES ARCS
 *********************************************/


bool GRAPHE_existe_arc(GRAPHE graphe, int source_id, int destination_id)
{
    bool err = false;
    SOMMET source = GRAPHE_lire(graphe, source_id, &err);
    if (!err) {
        return SOMMET_existe_successeur(source, destination_id);
    }
    else {
        return false;
    }
}


struct LISTEP *GRAPHE_lister_successeurs(GRAPHE graphe, int source_id, bool *p_err) {
    SOMMET sommet = GRAPHE_lire(graphe, source_id, p_err);
    return sommet->successeurs;
}


struct LISTEP *GRAPHE_lister_arcs(GRAPHE graphe)
{
    struct LISTEP *liste = NULL;
    struct ARC *arc_courant;
    bool err = false;
    for (int i = graphe.longueur-1; i >= 0; i--)
    {
        if (graphe.t[i] != NULL)
        {
            struct LISTEP *successeurs;
            struct DESTINATION_VALEUR *couple;
            while (successeurs != NULL) {
                couple = (struct DESTINATION_VALEUR *)successeurs->val;
                arc_courant = (struct ARC *)malloc(sizeof(struct ARC));
                arc_courant->source = i+1;
                arc_courant->destination = couple->destination;
                arc_courant->valeur = couple->valeur;
                LISTEP_inserer(&liste, 1, arc_courant, &err);
                successeurs = successeurs->lien;
            }
        }
    }
    return liste;
}


void GRAPHE_inserer_arc(GRAPHE *graphe, struct ARC arc, bool *p_err)
{
    // Obtenir le sommet source (le créer si nécessaire)
    SOMMET source = GRAPHE_lire(*graphe, arc.source, p_err);
    if (*p_err)
    {
        source = SOMMET_creer(arc.source);
        source = GRAPHE_inserer_sommet_existant(graphe, source, p_err);
        if (*p_err)
        {
            return;
        }
    }
    // Créer le sommet destination si nécessaire
    if (!GRAPHE_existe_sommet(*graphe, arc.destination))
    {
        SOMMET destination = SOMMET_creer(arc.destination);
        GRAPHE_inserer_sommet_existant(graphe, destination, p_err);
        if (*p_err)
        {
            return;
        }
    }
    SOMMET_ajouter_successeur(source, arc.destination, arc.valeur, p_err);
}



/*********************************************
 * FONCTIONS INTERNES À LA BIBLIOTHÈQUE
 *********************************************/


SOMMET SOMMET_creer(int id)
{
    SOMMET sommet = (SOMMET )malloc(sizeof(struct SOMMET_ADJ));
    sommet->id = id;
    sommet->successeurs = NULL;
    return sommet;
}



void SOMMET_liberer(SOMMET p_sommet)
{
    LISTEP_liberer(p_sommet->successeurs);
    free(p_sommet);
}


void SOMMET_ajouter_successeur(SOMMET p_sommet, int destination, float valeur, bool *p_err)
{
    // Chercher l'élément
    struct DESTINATION_VALEUR *successeur_courant;
    bool deja_present = false;
    struct LISTEP *successeurs = p_sommet->successeurs;
    while (successeurs != NULL && !deja_present)
    {
        successeur_courant = (struct DESTINATION_VALEUR *)successeurs->val;
        if (successeur_courant->destination == destination)
        {
            deja_present = true;
        }
        successeurs = successeurs->lien;
    }

    // Si non trouvé, alors insérer
    if (!deja_present)
    {
        struct DESTINATION_VALEUR *couple = (struct DESTINATION_VALEUR *)malloc(sizeof(struct DESTINATION_VALEUR));
        couple->destination = destination;
        couple->valeur = valeur;
        LISTEP_inserer(&(p_sommet->successeurs), 1, couple, p_err); // Ajouter en tête (le plus rapide)
    }
    // Sinon, remplacer la valeur de l'arc
    else {
        successeur_courant->valeur = valeur;
    }
}


bool SOMMET_existe_successeur(SOMMET source, int destination_id)
{
    // Chercher l'élément
    struct DESTINATION_VALEUR *successeur_courant;
    bool deja_present = false;
    struct LISTEP *successeurs = source->successeurs;
    while (successeurs != NULL && !deja_present)
    {
        successeur_courant = (struct DESTINATION_VALEUR *)successeurs->val;
        if (successeur_courant->destination == destination_id)
        {
            deja_present = true;
        }
        successeurs = successeurs->lien;
    }
    return deja_present;
}


