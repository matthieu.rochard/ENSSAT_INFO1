#include <stdbool.h>
#include <stdio.h>
#include "LISTET.h"

void LISTET_inserer(struct LISTET *p_liste, int pos, void *p_element, bool *p_err)
{
  int i;
  /* on peut inserer */
  if (p_liste->longueur < LMAX && pos > 0 && pos <= p_liste->longueur + 1)
  {
    /* indicateur d'erreur a FAUX */
    *p_err = false;
    /* decalage des valeurs avant l'insertion a pos */
    for (i = p_liste->longueur; i > pos - 1; i--)
    {
      p_liste->t[i] = p_liste->t[i - 1];
    }
    /* insertion d'elt a pos */
    p_liste->t[pos - 1] = p_element;
    /* maj de la longueur de la liste */
    p_liste->longueur++;
  } /* on ne peut pas inserer */
  else
  {
    /* indicateur d'erreur a VRAI */
    *p_err = true;
  }
}

void LISTET_supprimer(struct LISTET *p_liste, int pos, bool *p_err)
{
  int i;
  if (pos > 0 && pos <= p_liste->longueur)
  { /* on peut effacer */
    *p_err = false;
    /* on decale les elements a gauche a partir de pos */
    for (i = pos - 1; i < p_liste->longueur - 1; i++)
    {
      p_liste->t[i] = p_liste->t[i + 1];
    }
    p_liste->longueur--; /* maj de la longueur de liste */
  }
  else
  { /* on ne peut pas effacer */
    *p_err = true;
  }
}

int LISTET_rechercher(struct LISTET p_liste, void *p_element)
{
  int i = 0;
  /* on avance jusqu'a trouver l'elt ou la fin de liste */
  while (i < p_liste.longueur)
  {
    /* on a trouve */
    if (p_liste.t[i] == p_element)
    {
      return i + 1;
    }
    else
    {
      i++;
    }
  }
  /* on n'a pas trouve */
  return 0;
}

void LISTET_selectionner(struct LISTET liste, int pos, void **pp_element, bool *p_err)
{
  /* on peut selectionner */
  if (pos > 0 && pos <= liste.longueur)
  {
    *p_err = false;
    *pp_element = liste.t[pos - 1];
  }
  /* on ne peut pas selectionner */
  else
  {
    *p_err = true;
  }
}

void LISTET_afficher(struct LISTET liste)
{
  int i;
  for (i = 0; i < liste.longueur; i++)
  {
    printf("%p ", liste.t[i]);
  }
  printf("\n");
}
