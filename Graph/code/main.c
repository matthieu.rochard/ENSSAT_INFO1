#include <stdbool.h>
#include <stdio.h>
#include "GRAPHEM.h"
// ou #include "GRAPHEL.h"

#define INFINI 1000000

/*********************************** QUESTION 1 *****************************************/

/**
 * @brief Cherche l'identifiant du sommet avec la plus petite valeur dans dist parmi
 * la liste des candidats autorisés.
 * 
 * @param dist Tableau des distances actuelles
 * @param candidats Tableau de booléens indiquant si un sommet est candidat (true) ou non (false)
 * @param nb_sommets Nombre de sommets dans les tableaux
 * @return L'identifiant du sommet minimale (compris entre 1 et nb_sommets)
 */
int argmin_dist(float *dist, bool *candidats, int nb_sommets)
{
    //int nb_sommet_tester = 0;
    float distance_min = INFINI;
    int i = 0;
    SOMMET id_min;
    while (i < nb_sommets)
    {
        if (candidats[i])
        {
            printf("%d\n", i);
            if (dist[i] < distance_min)
            {
                id_min = i;
                distance_min = dist[i];
            }
            //nb_sommet_tester++;
        }
        i++;
    }
    return id_min + 1;
}

/*********************************** QUESTION 2 *****************************************/

/**
 * @brief Recherche avec l'algorithme de DIJKSTRA tous les chemins optimaux depuis
 * un sommet source dans un graphe orienté et valué SANS valeurs négatives.
 *
 * @param graphe [ENTRÉE] Le graphe dans lequel rechercher les chemins optimaux
 * @param source_id [ENTRÉE] L'identifiant du sommet source
 * @param dist [SORTIE] Tableau des valeurs des chemins trouvés par l'algorithme
 * @param pred [SORTIE] Tableau des prédécesseurs trouvés par l'algorithme
 * @param p_err [SORTIE] Drapeau d'erreur pour signaler si une erreur durant l'algorithme
 */
void dijkstra_un_vers_tous(GRAPHE graphe, int source_id, float *dist, int *pred, bool *p_err)
{
    SOMMET x, tested_id;
    int nb_sommet = 0;
    float alternative;
    struct LISTEP *sommets = GRAPHE_lister_sommets(graphe);
    //initialisation
    for (struct LISTEP *maillon = sommets; maillon != NULL; maillon = maillon->lien)
    {
        dist[(int)maillon->val] = INFINI;
        pred[(int)maillon->val] = 0;
        nb_sommet++;
    }
    dist[source_id] = 0;
    //traitement
    struct LISTEP *Q = GRAPHE_lister_successeurs(graphe, source_id, p_err);

    while (true)
    {
        LISTEP_selectionner(Q, 1, &tested_id, p_err);
        x = argmin_dist(dist, graphe.arcs[tested_id], nb_sommet);
        printf("min succ found for %d is %d", tested_id, x);
        LISTEP_supprimer(&Q, 1, p_err);
        struct LISTEP *y = GRAPHE_lister_successeurs(graphe, source_id, p_err);
        for (struct LISTEP *maillon = sommets; maillon != NULL; maillon = maillon->lien)
        {
            if (LISTEP_rechercher(y, x) != 0) // Le successeur est dans Q
            {
                struct DESTINATION_VALEUR *couple = (struct DESTINATION_VALEUR *)maillon->val;
                alternative = dist[x] + couple->valeur;
                if (alternative < dist[couple->destination])
                {
                    dist[couple->destination] = alternative;
                    pred[couple->destination] = x;
                    LISTEP_inserer(Q, 1, couple->destination, p_err);
                }
            }
        }
    }
}

/*********************************** QUESTION 3 *****************************************/

/**
 * @brief Recherche avec l'algorithme de DIJKSTRA le chemin optimal entre un sommet source
 * et un sommet destination dans un graphe orienté et valué SANS valeurs négatives.
 *
 * @param graphe [ENTRÉE] Le graphe dans lequel rechercher les chemins optimaux
 * @param source_id [ENTRÉE] L'identifiant du sommet source
 * @param destination_id [ENTRÉE] L'identifiant du sommet destination
 * @param dist [SORTIE] Tableau des valeurs des chemins trouvés par l'algorithme
 * @param pred [SORTIE] Tableau des prédécesseurs trouvés par l'algorithme
 * @param p_err [SORTIE] Drapeau d'erreur pour signaler si une erreur durant l'algorithme
 */
void dijkstra_un_vers_un(GRAPHE graphe, int source_id, int destination_id, float *dist, int *pred, bool *p_err)
{
    // TO DO
    // Remarques :
    // - initialiser pred avec des valeurs 0 (signifiant "aucun")
    // - la constante INFINI est définie (cf. début du fichier)
}

/*********************************** QUESTION 4 *****************************************/

/**
 * @brief Recherche avec l'algorithme de BELLMAN-FORD tous les chemins optimaux depuis
 * un sommet source dans un graphe orienté et valué acceptant des valeurs négatives.
 *
 * @param graphe [ENTRÉE] Le graphe dans lequel rechercher les chemins optimaux
 * @param source_id [ENTRÉE] L'identifiant du sommet source
 * @param dist [SORTIE] Tableau des valeurs des chemins trouvés par l'algorithme
 * @param pred [SORTIE] Tableau des prédécesseurs trouvés par l'algorithme
 * @param p_err [SORTIE] Drapeau d'erreur pour signaler si une erreur durant l'algorithme
 */
void bellman_un_vers_tous(GRAPHE graphe, int source_id, float *dist, int *pred, bool *p_err)
{
    // TO DO
    // Remarques :
    // - initialiser pred avec des valeurs 0 (signifiant "aucun")
    // - la constante INFINI est définie (cf. début du fichier)
}

/**************************************************************************************/

/**
 * @brief Reconstruit un chemin comme une liste d'arcs pour un sommet source et un sommet
 * destination données.
 * 
 * @param source_id [ENTRÉE] Identifiant du sommet source
 * @param destination [ENTRÉE] Identifiant du sommet destination
 * @param dist [ENTRÉE] Tableau des valeurs des chemins trouvés par un algorithme
 * @param pred [ENTRÉE] Tableau des prédécesseurs trouvés par un algorithme
 * @param valeur_chemin [SORTIE] Valeur du chemin optimal trouvé
 * @param p_err [SORTIE] Drapeau d'erreur pour signaler qu'aucun chemin n'a été trouvé
 * @return [SORTIE] Une liste de pointeurs d'arcs (x, y, v)
 */
struct LISTEP *reconstruire_chemin(int source_id,
                                   int destination_id,
                                   float *dist,
                                   int *pred,
                                   int *valeur_chemin,
                                   bool *p_err)
{
    int sommet_courant;
    float precedente_dist;
    struct ARC *arc;
    // Initialiser un chemin vide
    struct LISTEP *liste_arcs;
    // Si aucun prédécesseur
    if (pred[destination_id] == 0)
    {
        // Alors ERREUR, aucun chemin
        *p_err = true;
    }
    // Sinon
    else
    {
        // Valeur du chemin est déjà calculée
        *valeur_chemin = dist[destination_id];
        precedente_dist = dist[destination_id];
        // Tant que sommet courant n'est pas le sommet source
        while (sommet_courant != source_id)
        // Faire
        {
            // Construire l'arc correspond
            arc = (struct ARC *)malloc(sizeof(struct ARC));
            arc->source = pred[sommet_courant];
            arc->destination = sommet_courant;
            arc->valeur = precedente_dist - dist[arc->source];
            // Déduire la valeur de l'arc de celle du chemin en cours de construction
            precedente_dist = arc->valeur;
            // Ajouter l'arc au début du chemin
            LISTEP_inserer(&liste_arcs, 1, arc, p_err);
            // Passer au sommet précédant
            sommet_courant = arc->source;
            // Fin Tant que
        }
        // Fin Si
    }
    // Retourner le chemin
    return liste_arcs;
}

/**************************************************************************************/

/**
 * @brief Affiche une liste d'arc comme un chemin
 * 
 * @param chemin Une liste de pointeurs d'arcs
 */
void afficher_chemin(struct LISTEP *chemin)
{
    struct ARC *arc_courant;
    bool premier = true;
    for (struct LISTEP *maillon = chemin; maillon != NULL; maillon = maillon->lien)
    {

        arc_courant = (struct ARC *)maillon->val;
        if (premier)
        {
            printf("%i", arc_courant->source);
            premier = false;
        }
        printf(" --[ %f ]-> %i", arc_courant->valeur, arc_courant->destination);
    }
}

/**************************************************************************************/

/**
 * @brief Point d'entrée du programme
 */
int main()
{
    GRAPHE *g = GRAPHE_creer();
    struct ARC arcs[] = {{1, 2, 20},
                         {1, 4, 30},
                         {2, 2, 10},
                         {2, 3, 40},
                         {2, 4, 20},
                         {3, 4, 40},
                         {3, 5, 10},
                         {4, 3, 20},
                         {4, 5, 70}};
    int nb_arcs = sizeof(arcs) / sizeof(struct ARC);
    bool err;
    for (int i = 0; i < nb_arcs; i++)
    {
        GRAPHE_inserer_arc(g, arcs[i], &err);
    }

    GRAPHE_afficher_liste(*g);

    float exemple_dist[5] = {7, 5, 2, 1, 4};
    bool exemple_candidats[5] = {true, false, true, false, true};
    printf("Argmin = %i\n", argmin_dist(exemple_dist, exemple_candidats, 5));
    float dist[5];
    int pred[5];
    dijkstra_un_vers_tous(*g, 1, dist, pred, &err);

    /**
    struct LISTEP *successeurs = GRAPHE_lister_successeurs(*g, 2, &err);
    for (struct LISTEP *maillon = successeurs; maillon != NULL; maillon = maillon->lien)
    {
        struct DESTINATION_VALEUR *couple = (struct DESTINATION_VALEUR *)maillon->val;
        printf("successeur = (%i, %f)\n", couple->destination, couple->valeur);
    }
    */
    return EXIT_SUCCESS;
}
