//--------------------------------------------------------------------------//
// TP Introduction aux Microcontrôleurs 									//
//--------------------------------------------------------------------------//
// Fonction principale	- TD1V2																							//
//--------------------------------------------------------------------------//

//----------------------------------------------------------------------------
// Fichiers Inclus
//----------------------------------------------------------------------------
#include "lpc_types.h"
#include "declarations.h"

//----------------------------------------------------------------------------
// Variables et Constantes globales
//----------------------------------------------------------------------------
GPIO_Registres *pGPIO0 ;		// Pointeur sur la structure du GPIO0
GPIO_Registres *pGPIO2 ;
// .... etc pour les autres structures

//----------------------------------------------------------------------------
// Fonction principale
//----------------------------------------------------------------------------
int main(void)
{
	
	int decalage,d;
	int buff;
	// Initialisation des pointeurs sur l'adresse physique des registres
	pGPIO0 = GPIO0_BASE;
	pGPIO2 = GPIO2_BASE;
	
	pGPIO0->FIODIR = (1<<3)|(1<<2)|(1<<1)|(1<<0); 
	pGPIO0->FIOMASK = ~((1<<3)|(1<<2)|(1<<1)|(1<<0));
	pGPIO2->FIODIR = 0;
	pGPIO2->FIOMASK = ~((1<<11)|(1<<10));
	pGPIO0->FIOPIN = (1<<3)|(1<<2)|(1<<1)|(1<<0);
	
	buff = pGPIO2->FIOPIN & (1<<10);
	
	decalage = 0;
	while(1)
  {
		if(buff !=(pGPIO2->FIOPIN & (1<<10)) && !(pGPIO2->FIOPIN & (1<<10)))
		{
			decalage = decalage + 1;
			decalage = decalage%4;
		}
		
		decalage;
		if(!(pGPIO2->FIOPIN & (1<<11)))
		{
			pGPIO0->FIOPIN = ~(1<<d);
		}
		else
		{
			pGPIO0->FIOPIN = (1<<3)|(1<<2)|(1<<1)|(1<<0);
		}
		
		buff = pGPIO2->FIOPIN & (1<<10);		
  }
}

//----------------------------------------------------------------------------
//#ifdef  DEBUG
void check_failed(uint8_t *file, uint32_t line) {while(1);}
//#endif
