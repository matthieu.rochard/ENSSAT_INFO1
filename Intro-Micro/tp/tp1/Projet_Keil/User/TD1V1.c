//--------------------------------------------------------------------------//
// TP Introduction aux Microcontrôleurs 									//
//--------------------------------------------------------------------------//
// Fonction principale	- TD1V1																							//
//--------------------------------------------------------------------------//

//----------------------------------------------------------------------------
// Fichiers Inclus
//----------------------------------------------------------------------------
#include "lpc_types.h"
#include "declarations.h"

//----------------------------------------------------------------------------
// Variables et Constantes globales
//----------------------------------------------------------------------------
GPIO_Registres *pGPIO0 ;		// Pointeur sur la structure du GPIO0
GPIO_Registres *pGPIO2 ;
//----------------------------------------------------------------------------
// Fonction principale
//----------------------------------------------------------------------------
int main(void)
{
	
	pGPIO0 = GPIO0_BASE;
	pGPIO2 = GPIO2_BASE;
	
	pGPIO0->FIODIR = (1<<3)|(1<<2); 
  pGPIO0->FIOMASK = ~((1<<3)|(1<<2));
	pGPIO2->FIODIR = 0;
	pGPIO2->FIOMASK = ~((1<<11)|(1<<10));
	pGPIO0->FIOPIN = (1<<3)|(1<<2);
		
	while(1)
  {
		 pGPIO0->FIOPIN = pGPIO2->FIOPIN>>8;
	}
}

//----------------------------------------------------------------------------
//#ifdef  DEBUG
void check_failed(uint8_t *file, uint32_t line) {while(1);}
//#endif
