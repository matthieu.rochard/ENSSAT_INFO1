//--------------------------------------------------------------------------//
// TP Introduction aux Microcontrôleurs 							//
//--------------------------------------------------------------------------//
// Fichier Header contenant:																								//
// - Les structures de registres de vos peripheriques												//
// - Les adresses de base de ces structures																	//
//--------------------------------------------------------------------------//

//----------------------------------------------------------------------------
// Definition de la structure correspondant aux registres de gestion d'un GPIO
//----------------------------------------------------------------------------
typedef struct 
{
	unsigned int FIODIR;
	unsigned int reserve1;
	unsigned int reserve2;
	unsigned int reserve3;
	unsigned int FIOMASK;
	unsigned int FIOPIN;
}	
GPIO_Registres ;

//----------------------------------------------------------------------------
// Definition des constantes symboliques pour les adresses de base des periphs
//----------------------------------------------------------------------------
#define GPIO0_BASE    (GPIO_Registres *)0x2009C000;
#define GPIO2_BASE    (GPIO_Registres *)0x2009C040;


