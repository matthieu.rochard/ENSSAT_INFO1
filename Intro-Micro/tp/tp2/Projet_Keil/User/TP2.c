//--------------------------------------------------------------------------//
// TP2 Introduction aux Microcontr�leurs 								//
//--------------------------------------------------------------------------//
// Fonction principale	- TP2  																							//
//--------------------------------------------------------------------------//

//----------------------------------------------------------------------------
// Fichiers Inclus
//----------------------------------------------------------------------------
#include "lpc_types.h"
#include "declarations.h"
#define BASE_TIME 4000;

//----------------------------------------------------------------------------
// Variables et Constantes globales
//----------------------------------------------------------------------------
GPIO_Registres 	 	*pGPIO0; 		// Pointeur sur la structure du GPIO0
PINSEL_Registres	*pPINSEL3;
TIMER_Registres 	*pTIMER0;
GPIO_Registres 		*pGPIO2 ;

int KEY0_state(int buff,GPIO_Registres *pGPIO2);

//----------------------------------------------------------------------------
// Fonction principale
//----------------------------------------------------------------------------
int main(void)
{		
	// Initialisation des pointeurs sur l'adresse physique des registres 
	int buff, time;
	pGPIO2 = GPIO2_BASE;
	pPINSEL3 = PINSEL3_BASE;
	pTIMER0 = TIMER0_BASE;
	
	//Initalise les �tats de GPIO2 pour le bouton KEY2
	pGPIO2->FIODIR = 0;									//l'ensemble des bits en entr�e
	pGPIO2->FIOMASK = ~((1<<10));				//mask l'ensemble des bits sauf le 10
	//Selectionne MAT0[0] en sortie de P1.28 	
	pPINSEL3->PINSEL = (1<<24)|(1<<25); 
	//configuration du TIMER0
	pTIMER0->PR = 25000;								//controle la p�cision ici 1ms
	pTIMER0->MR0 = BASE_TIME; 					//attendre les 4000ms = 4s
	pTIMER0->EMR = (1<<5)|(1<<4);				//toggle de MAT0
	pTIMER0->CTCR &= ~((1<<1)|(1<<0));	//mise en mode timer
	pTIMER0->MCR = (1<<1); 							//remise � 0 de TC
	pTIMER0->TCR = (1<<0); 							//enable le timer
	
	buff = pGPIO2->FIOPIN & (1<<10);		//enregistre l'�tat du bouton
	while(1)
	{
		if(KEY0_state(buff,pGPIO2)) 			//si le bouton est push
		{
			time = (pTIMER0->MR0)>>1;				//divise le temps par deux
			if(time<10)											//si le temps est inf�rieur � 10ms
			{																//alors on boucle � 4000ms
				time = BASE_TIME;
			}
			pTIMER0->MR0 = time; 						//enregistre le nouveau temp
			pTIMER0->TCR |= (1<<1);					//reste le compteur
			pTIMER0->TCR &= ~((1<<1));
		}
		buff = pGPIO2->FIOPIN & (1<<10);	//enregistre l'�tat du bouton
	}		
}

//----------------------------------------------------------------------------
//#ifdef  DEBUG
void check_failed(uint8_t *file, uint32_t line) {while(1);}
//#endif

int KEY0_state(int buff,GPIO_Registres *pGPIO2)
{	
	return (buff !=(pGPIO2->FIOPIN & (1<<10)) && !(pGPIO2->FIOPIN & (1<<10)));
}