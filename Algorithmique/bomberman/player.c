// compiler header files
#include <stdbool.h> // bool, true, false
#include <stdlib.h>  // rand
#include <stdio.h>   // printf

// program header file
#include "bomberman.h"

// global declarations
extern const char BOMBERMAN;      // ascii used for bomberman
extern const char WALL;           // ascii used for the walls
extern const char BREAKABLE_WALL; // ascii used for the breakable walls
extern const char PATH;           // ascii used for the pathes
extern const char EXIT;           // ascii used for the EXIT
extern const char BOMB;           // ascii used for a bomb
extern const char BOMB_BONUS;     // ascii used for the bombs
extern const char FLAME_BONUS;    // ascii used for the flames
extern const char FLAME_ENEMY;    // ascii used for the flame ennemy
extern const char GHOST_ENEMY;    // ascii used for the ghost ennemy

extern const int BOMB_DELAY;           // time before a bomb explode
extern const int BREAKABLE_WALL_SCORE; // reward for bombing a breakable wall
extern const int FLAME_ENEMY_SCORE;    // reward for bombing a flame enemy
extern const int GHOST_ENEMY_SCORE;    // reward for bombing a ghost enemy
extern const int BOMB_BONUS_SCORE;     // reward for a bomb bonus, which increases by 1 the number of bombs that can be dropped at a time
extern const int FLAME_BONUS_SCORE;    // reward for a flame bonus, which increses by 1 the explosion range of bombs
extern const int VIEW_DISTANCE;        // bomberman view distance

extern bool DEBUG; // indicates whether the game runs in DEBUG mode

char *binome = "Rochard Matthieu"; // student name goes here

// prototypes of the local functions/procedures
void printAction(action);
void printBoolean(bool);
action findExit(tree map, int remaining_bombs, int explosion_range, bool *_error);
action moveCloser(tree _map, char _elt, int explosion_range, bool *_error);
action findBonus(tree _map, int explosion_range, bool *_error);
action findEscape(tree _map, int _explosion_range, bool *_error);
action dodgeExplosion(tree _map, char _elt, int _explosion_range);
action dodgeFlame(tree _map, int _range, int _explosion_range);
bool isEscape(tree _map, int _explosion_range, int _bombDistance, action _direction);
bool isInView(tree _map, char _elt);
action randomMove(tree _map, int explosion_range, bool *_error);
int distanceTo(tree _map, char _elt, int _distanceFromBomberman);
bool isAllowed(tree _map);
bool isSafe(tree _map);
bool isSoDeep(tree _map, int _depth);
int getDeep(tree _map, int _depth);
bool isInExplosion(tree _map, int _explosion_range);
bool isInDirection(tree _map, char _elt, action _direction, int _range);
tree getMapOfDirection(tree _map, action _direction, bool *_error);
int min(int *_tab, int _start, int _end);
int max(int *_tab, int _start, int _end);

/*
  bomberman function:
  This function randomly select a valid move for BOMBERMAN based on its current position on the game map.
 */
action bomberman(
    tree map,            // 4-ary tree of char modeling a subset of the game map
    action last_action,  // last action made, -1 in the be beginning
    int remaining_bombs, // number of bombs
    int explosion_range  // explosion range for the bombs
)
{
  action a; // action to choose and return

  bool *ok = malloc(sizeof(bool)); // ok will be set to true as soon as a randomly selected action is valid
  if (DEBUG)
  {
    printf("------------------------Debut du tour----------------------------\nLast Action :");
    printAction(last_action);
    printf("  Explosion : %d   BOMBS: %d\n", explosion_range, remaining_bombs);
  }
  if (last_action == BOMBING) //Si la dernière action à été de poser une bombe
  {
    a = findEscape(map, explosion_range, ok); //On trouve une direction par laquelle on peut s'enfuir

    if (DEBUG)
    {
      printf("\n>>-------------------------------Finding escape was success:\n");
      printBoolean(*ok);
      if (*ok)
      {
        printf("\nIt recommended we :");
        printAction(a);
      }
      printf("\n");
    }
  }
  else if (isInExplosion(map, explosion_range)) //Si on est dans une explosion
  {
    if (DEBUG)
    {
      printf("\nDodging bomb------------------------------------------------->>\n");
    }
    a = dodgeExplosion(map, BOMB, explosion_range); //On cherche un chemin pour s'échapper
  }
  else if (isInView(map, FLAME_ENEMY) && distanceTo(map, FLAME_ENEMY, 0) < 3) //Si un FLAME est tout près
  {
    if (DEBUG)
    {
      printf("\nEscaping Flame----------------------------------------------->>\n");
    }
    //On cherche un chemin pour s'échapper
    a = dodgeFlame(map, 3, explosion_range);
  }
  else //Sinon, s'il n'y à rien à évité
  {
    //On cherche s'il y a des bonus
    a = findBonus(map, explosion_range, ok);
    if (!*ok) //S'il n'y pas de bonus
    {
      if (DEBUG)
      {
        printf("\nFinding exit--------------------------------------------------->>\n");
      }
      a = findExit(map, remaining_bombs, explosion_range, ok); //On cherche la sortie
      if (DEBUG)
      {
        printf("\n>>---------------------------------Finding exit was success:");
        printBoolean(ok);
        if (ok)
        {
          printf("\nIt recommended we :");
          printAction(a);
        }
        printf("\n");
      }
    }
  }

  return a; // answer to the game engine
}

/**
 * @brief Nous rapproche de la sortie. Si la sortie n'est pas visible alors on cherche à casser un mur. 
 * Un mur ne peut être cassé uniquement si il y a un moyen de s'échapper. 
 * @param _map 
 * @param remaining_bombs 
 * @param explosion_range 
 * @param _error 
 * @return action 
 */
action findExit(tree _map, int remaining_bombs, int explosion_range, bool *_error)
{
  action act;
  bool ok = true;            // booléen de confirmation de réussit de la dernière fonction appeler
  if (!isInView(_map, EXIT)) //Si la sortie n'est pas visible
  {
    if (!isInView(_map, BREAKABLE_WALL)) //si aucun mur cassable n'est visible
    {
      if (DEBUG)
      {
        printf("\n->No breakable walls found\n");
        printf("--->distanceTo is %d\n", distanceTo(_map, BREAKABLE_WALL, 0));
      }
      act = randomMove(_map, explosion_range, &ok); //alors on prend une direction aléatoire
      if (DEBUG && !ok)
      {
        printf("\n->Random Move ln:166 failed: ");
        printAction(act);
      }
    }
    else //Si in mur cassable est visible
    {
      if (DEBUG)
      {
        printf("\n->Breakable walls found\n");
      }
      if (distanceTo(_map, BREAKABLE_WALL, 0) == 1) //si le mur est à côté de bomberman
      {
        if (DEBUG)
        {
          printf("\n->Next to breakable wall\n");
        }

        findEscape(_map, explosion_range + 1, &ok); //on cherche un chemin pour s'échapper après avoir posé la bombe.
        if (ok && remaining_bombs > 0)              //si un chemin pour s'échapper est trouvé et s'il reste des bombes
        {
          if (DEBUG)
          {
            printf("\n->Can Bomb\n");
          }
          act = BOMBING; //pose une bombe
        }
        else //Si on ne peut pas poser de bombe
        {
          if (DEBUG)
          {
            printf("\n->Cannot Bomb\n");
          }
          act = randomMove(_map, explosion_range, &ok); //alors on prend une direction aléatoire en attendent
          if (DEBUG && !ok)
          {
            printf("\n->Random Move ln:201 failed: ");
            printAction(act);
            printf("\n");
          }
        }
      }
      else //Si le mur cassable visible n'est pas à côter
      {
        if (DEBUG)
        {
          printf("\n->Moving closer to Breakable Wall");
        }
        act = moveCloser(_map, BREAKABLE_WALL, explosion_range, &ok); //On s'en rapproche
        if (DEBUG && !ok)
        {
          printf("\n->Move Closer ln:213 failed: ");
          printAction(act);
        }
      }
    }
  }
  else //Si la sortie est visible
  {
    if (DEBUG)
    {
      printf("\n->Moving closer to Exit");
    }
    act = moveCloser(_map, EXIT, explosion_range, &ok); // on s'en rapproche
    if (DEBUG && !ok)
    {
      printf("\n->Move Closer ln:225 failed: ");
      printAction(act);
    }
  }
  if (!ok)
    act = randomMove(_map, explosion_range, &ok);
  *_error = ok;
  return act;
}

/**
 * @brief brief Fournie l'action à prendre pour se rapprocher l'élément fourni.
 * Regardant successivement le NORD,EST,SUD, OUEST et s'assurant que le chemin ne met pas en danger : 
 * L'on calcule la distance vers la cible et l'on retient la direction pour laquelle la distance est la plus courte.
 * 
 * @param _map 
 * @param _elt 
 * @param explosion_range 
 * @param _error 
 * @return action l'action à effectuer
 */
action moveCloser(tree _map, char _elt, int explosion_range, bool *_error)
{
  int distance = 255, tmpDistance, distanceToFlame;
  action direction = BOMBING;
  distanceToFlame = distanceTo(_map, FLAME_ENEMY, 0);                                                               //distance vers le flame le plus proche
  if (isInView(_map->n, _elt) && isAllowed(_map->n) && isSafe(_map->n) && !isInExplosion(_map->n, explosion_range)) //Si l'élément est au NORD
  {                                                                                                                 //Si l'élément se trouve au nord et que l'on n'est pas dans une explosion :
    tmpDistance = distanceTo(_map->n, _elt, 1);                                                                     //alors la distance la plus court est celle au nord
    if (DEBUG)
    {
      printf("\n%c is visible %d flame is  %d north", _elt, tmpDistance, distanceToFlame);
      printBoolean((tmpDistance < distance));
      printBoolean((tmpDistance * 2 < distanceToFlame || distanceToFlame == -1));
    }
    if (tmpDistance * 2 < distanceToFlame || distanceToFlame == -1) //on souhaite pouvoir atteindre l'objectif et en échapper sans être bloquée par un flame
    {
      distance = tmpDistance;
      direction = NORTH;
    }
  }
  //On applique le même exercice à chaque direction. En plus on regard si la distance est plus court
  if (isInView(_map->e, _elt) && isAllowed(_map->e) && isSafe(_map->e) && !isInExplosion(_map->e, explosion_range)) //si l'élément est a l'EST
  {
    tmpDistance = distanceTo(_map->e, _elt, 1);
    if (DEBUG)
    {
      printf("\n%c is visible %d flame is  %d east", _elt, tmpDistance, distanceToFlame);
      printBoolean((tmpDistance < distance));
      printBoolean((tmpDistance * 2 < distanceToFlame || distanceToFlame == -1));
    }
    if (tmpDistance < distance && (tmpDistance * 2 < distanceToFlame || distanceToFlame == -1)) //si la distance en passant par l'Est est plus court
    {
      distance = tmpDistance; //On sauvegarde la nouvelle distance la plus courte
      direction = EAST;       //direction à prendre est EAST
    }
  }
  if (isInView(_map->s, _elt) && isAllowed(_map->s) && isSafe(_map->s) && !isInExplosion(_map->s, explosion_range))
  {
    tmpDistance = distanceTo(_map->s, _elt, 1);
    if (DEBUG)
    {
      printf("\n%c is visible %d flame is  %d south", _elt, tmpDistance, distanceToFlame);
      printBoolean((tmpDistance < distance));
      printBoolean((tmpDistance * 2 < distanceToFlame) || distanceToFlame == -1);
    }
    if (tmpDistance < distance && (tmpDistance * 2 < distanceToFlame || distanceToFlame == -1))
    {
      distance = tmpDistance;
      direction = SOUTH;
    }
  }
  if (isInView(_map->w, _elt) && isAllowed(_map->w) && isSafe(_map->w) && !isInExplosion(_map->w, explosion_range))
  {
    tmpDistance = distanceTo(_map->w, _elt, 1);
    if (DEBUG)
    {
      printf("\n%c is visible %d flame is  %d west", _elt, tmpDistance, distanceToFlame);
      printBoolean((tmpDistance < distance));
      printBoolean((tmpDistance * 2 < distanceToFlame || distanceToFlame == -1));
    }
    if (tmpDistance < distance && (tmpDistance * 2 < distanceToFlame || distanceToFlame == -1))
    {
      distance = tmpDistance;
      direction = WEST;
    }
  }
  *_error = !(direction == BOMBING); //si direction n'a pas changé alors c'est qu'on ne peut pas se rapprocher
  return direction;                  //retourne la direction trouvée
}

/**
 * @brief Trouve et donne la direction vers le bonus le plus proche donnant le plus de point
 * 
 * @param _map 
 * @param _error false: aucun bonnus trouvé
 * @return action 
 */
action findBonus(tree _map, int explosion_range, bool *_error)
{
  *_error = true;
  if (isInView(_map, FLAME_BONUS) && isInView(_map, BOMB_BONUS)) //si flame bonus et bomb bonus visible
  {
    if (FLAME_BONUS_SCORE > BOMB_BONUS_SCORE) //On se dirige vers celui qui donne le plus de point
    {
      return moveCloser(_map, FLAME_BONUS, explosion_range, _error); //sa direction
    }
    else
    {
      return moveCloser(_map, BOMB_BONUS, explosion_range, _error); //sa direction
    }
  }
  else if (isInView(_map, FLAME_BONUS)) //si flame bonus visible
  {
    return moveCloser(_map, FLAME_BONUS, explosion_range, _error); //sa direction
  }
  else if (isInView(_map, BOMB_BONUS)) //si flame bonus visible
  {
    return moveCloser(_map, BOMB_BONUS, explosion_range, _error); //sa direction
  }
  else
  {
    *_error = false; //Si aucun bonus n'est trouvé
    return BOMBING;
  }
}

/**
 * @brief Trouve la direction à prendre pour éviter la bombe qui vient être posée.
 * On cherche à s'échapper successivement par le NORD, EST, OUEST,SUD en retenant la direction qui fonctionne
 * @param _map 
 * @param _explosion_range 
 * @return action la direction à prendre
 */
action findEscape(tree _map, int explosion_range, bool *_error)
{
  action direction = 0;
  bool tryNorth, tryEast, trySouth, tryWest, ok;
  int distanceToBomb = 1;
  //si la bombe n'est pas au nord
  if (!isInDirection(_map, BOMB, NORTH, explosion_range))
  {
    tryNorth = isEscape(_map, explosion_range, distanceToBomb, NORTH); //alors on regard pour s'échapper au nord
    if (DEBUG)
      printf("\n->Can escape NORTH depth is %d: %d\n", isSoDeep(getMapOfDirection(_map, NORTH, &ok), 2), tryNorth);
    if (tryNorth)
      direction = NORTH;
  }
  //Même exercice dans les autres directions
  if (!isInDirection(_map, BOMB, EAST, explosion_range))
  {
    tryEast = isEscape(_map, explosion_range, distanceToBomb, EAST);
    if (DEBUG)
      printf("\n->Can escape EAST depth is %d: %d\n", isSoDeep(getMapOfDirection(_map, EAST, &ok), 2), tryEast);
    if (tryEast)
      direction = EAST;
  }
  if (!isInDirection(_map, BOMB, SOUTH, explosion_range))
  {
    trySouth = isEscape(_map, explosion_range, distanceToBomb, SOUTH);
    if (DEBUG)
      printf("\n->Can escape SOUTH depth is %d: %d\n", isSoDeep(getMapOfDirection(_map, SOUTH, &ok), 2), trySouth);
    if (trySouth)
      direction = SOUTH;
  }
  if (!isInDirection(_map, BOMB, WEST, explosion_range))
  {
    tryWest = isEscape(_map, explosion_range, distanceToBomb, WEST);
    if (DEBUG)
      printf("\n->Can escape WEST depth is %d: %d\n", isSoDeep(getMapOfDirection(_map, WEST, &ok), 2), tryWest);
    if (tryWest)
      direction = WEST;
  }
  if (direction == BOMBING) //il peut arriver qu'aucun chemin ne soit idéal.
  {                         //Il vaut mieux ne pas être dans cette circonstance, car il y a de bonne chance de ne pas survivre
    direction = randomMove(_map, explosion_range, &ok);
    *_error = false;
  }
  else
    *_error = true;
  return direction;
}

/**
 * @brief Lorsque bomberman est dans l'explosion d'une bombe
 * On va alors faire 'face' à la bombe et obtenir les directions à gauche et droite. On tente alors de se déplacer dans ces directions, car cela nous sort immédiatement de l'explosion.
 * Sinon on recule.
 * @param _map 
 * @param _explosion_range 
 * @return action 
 */
action dodgeExplosion(tree _map, char _elt, int _explosion_range)
{
  bool ok = true;
  if (DEBUG)
  {
    printf("dodging %c to range %d\n", _elt, _explosion_range);
  }
  action direction, leftDirection, rightDirection, oppDirection, bombDirection;
  int distanceFromBomb = distanceTo(_map, _elt, 0);
  //Détermine les directions à gauche, droite et opposé à la bombe
  if (isInDirection(_map, _elt, NORTH, _explosion_range))
  {
    bombDirection = NORTH;
    leftDirection = WEST;
    rightDirection = EAST;
    oppDirection = SOUTH;
  }
  else if (isInDirection(_map, _elt, EAST, _explosion_range))
  {
    bombDirection = EAST;
    leftDirection = NORTH;
    rightDirection = SOUTH;
    oppDirection = WEST;
  }
  else if (isInDirection(_map, _elt, SOUTH, _explosion_range))
  {
    bombDirection = SOUTH;
    leftDirection = EAST;
    rightDirection = WEST;
    oppDirection = NORTH;
  }
  else if (isInDirection(_map, _elt, WEST, _explosion_range))
  {
    bombDirection = WEST;
    leftDirection = SOUTH;
    rightDirection = NORTH;
    oppDirection = EAST;
  }
  if (DEBUG)
  {
    printf("Bomb wwas found to the :");
    printAction(bombDirection);
    printf("\n");
  }
  //en commençant par la gauche puis la droite et enfin l'arrière :
  //le chemin doit être possible et ne pas nous emmener dans le rayon d'une explosion
  //de plus le chemin doit avoir une profondeur de au moins 2 pour nous permettre de faire des aller-retour jusqu'à l'explosion en cas de cul-de-sac.
  //enfin il faut ne pas voir de FLAME ou que le FLAME ne bloque pas le chemin
  if (isAllowed(getMapOfDirection(_map, leftDirection, &ok)) &&
      isSafe(getMapOfDirection(_map, leftDirection, &ok)) &&
      !isInExplosion(getMapOfDirection(_map, leftDirection, &ok), _explosion_range) &&
      isSoDeep(getMapOfDirection(_map, leftDirection, &ok), 2) &&
      (!isInView(getMapOfDirection(_map, leftDirection, &ok), FLAME_ENEMY) ||
       distanceTo(getMapOfDirection(_map, leftDirection, &ok), FLAME_ENEMY, 0) < getDeep(getMapOfDirection(_map, leftDirection, &ok), 1)))
  {
    direction = leftDirection;
  }
  else if (isAllowed(getMapOfDirection(_map, rightDirection, &ok)) &&
           isSafe(getMapOfDirection(_map, rightDirection, &ok)) &&
           !isInExplosion(getMapOfDirection(_map, rightDirection, &ok), _explosion_range) &&
           isSoDeep(getMapOfDirection(_map, rightDirection, &ok), 2) &&
           (!isInView(getMapOfDirection(_map, rightDirection, &ok), FLAME_ENEMY) ||
            distanceTo(getMapOfDirection(_map, rightDirection, &ok), FLAME_ENEMY, 0) < getDeep(getMapOfDirection(_map, leftDirection, &ok), 1)))
  {
    direction = rightDirection;
  }
  else if (isAllowed(getMapOfDirection(_map, oppDirection, &ok)) &&
           isSafe(getMapOfDirection(_map, oppDirection, &ok)) &&
           !isInExplosion(getMapOfDirection(_map, oppDirection, &ok), _explosion_range) &&
           isSoDeep(getMapOfDirection(_map, oppDirection, &ok), _explosion_range - distanceFromBomb) &&
           (!isInView(getMapOfDirection(_map, oppDirection, &ok), FLAME_ENEMY) ||
            distanceTo(getMapOfDirection(_map, oppDirection, &ok), FLAME_ENEMY, 0) < getDeep(getMapOfDirection(_map, leftDirection, &ok), 1)))
  {
    direction = oppDirection;
  }
  else
  {
    direction = randomMove(_map, _explosion_range, &ok);
  }
  return direction;
}

/**
 * @brief Lorsque bomberman est proche d'un FLAME
 * On va alors faire 'face' au FLAME et obtenir les directions à gauche et droite. On tente alors de partir à gauche ou à droite si ces directions ne mettent pas en danger
 * Sinon on recule
 * @param _map 
 * @param _range 
 * @param explosion_range 
 * @return action 
 */
action dodgeFlame(tree _map, int _range, int explosion_range)
{
  bool ok = true;
  if (DEBUG)
  {
    printf("\ndodging %c to range %d", FLAME_ENEMY, _range);
  }
  action direction, leftDirection, rightDirection, oppDirection, flameDirection;
  int distanceFromFlame = distanceTo(_map, FLAME_ENEMY, 0);
  //Détermine les directions à gauche, droite et opposé au FLAME
  if (distanceTo(getMapOfDirection(_map, NORTH, &ok), FLAME_ENEMY, 1) < _range &&
      isInView(getMapOfDirection(_map, NORTH, &ok), FLAME_ENEMY))
  {
    flameDirection = NORTH;
    leftDirection = WEST;
    rightDirection = EAST;
    oppDirection = SOUTH;
  }
  else if (distanceTo(getMapOfDirection(_map, EAST, &ok), FLAME_ENEMY, 1) < _range &&
           isInView(getMapOfDirection(_map, EAST, &ok), FLAME_ENEMY))
  {
    flameDirection = EAST;
    leftDirection = NORTH;
    rightDirection = SOUTH;
    oppDirection = WEST;
  }
  else if (distanceTo(getMapOfDirection(_map, SOUTH, &ok), FLAME_ENEMY, 1) < _range &&
           isInView(getMapOfDirection(_map, SOUTH, &ok), FLAME_ENEMY))
  {
    flameDirection = SOUTH;
    leftDirection = EAST;
    rightDirection = WEST;
    oppDirection = NORTH;
  }
  else if (distanceTo(getMapOfDirection(_map, WEST, &ok), FLAME_ENEMY, 1) < _range &&
           isInView(getMapOfDirection(_map, WEST, &ok), FLAME_ENEMY))
  {
    flameDirection = WEST;
    leftDirection = SOUTH;
    rightDirection = NORTH;
    oppDirection = EAST;
  }
  if (DEBUG)
  {
    printf("Flame was found to the :");
    printAction(flameDirection);
    printf("\n");
  }
  //en commençant par la gauche puis la droite et enfin l'arrière :
  //le chemin doit être possible et ne pas nous emmener dans le rayon d'une explosion
  //enfin il faut ne pas voir de FLAME ou que le Flame soit plus loin que le flame que l'on évite
  if (isAllowed(getMapOfDirection(_map, leftDirection, &ok)) &&
      isSafe(getMapOfDirection(_map, leftDirection, &ok)) &&
      !isInExplosion(getMapOfDirection(_map, leftDirection, &ok), explosion_range) &&
      isSoDeep(getMapOfDirection(_map, leftDirection, &ok), BOMB_DELAY) &&
      (!isInView(getMapOfDirection(_map, leftDirection, &ok), FLAME_ENEMY) ||
       distanceTo(getMapOfDirection(_map, leftDirection, &ok), FLAME_ENEMY, 0) > distanceFromFlame))
  {
    direction = leftDirection;
  }
  else if (isAllowed(getMapOfDirection(_map, rightDirection, &ok)) &&
           isSafe(getMapOfDirection(_map, rightDirection, &ok)) &&
           !isInExplosion(getMapOfDirection(_map, rightDirection, &ok), explosion_range) &&
           isSoDeep(getMapOfDirection(_map, rightDirection, &ok), BOMB_DELAY) &&
           (!isInView(getMapOfDirection(_map, rightDirection, &ok), FLAME_ENEMY) ||
            distanceTo(getMapOfDirection(_map, rightDirection, &ok), FLAME_ENEMY, 0) > distanceFromFlame))
  {
    direction = rightDirection;
  }
  else if (isAllowed(getMapOfDirection(_map, oppDirection, &ok)) &&
           isSafe(getMapOfDirection(_map, oppDirection, &ok)) &&
           !isInExplosion(getMapOfDirection(_map, oppDirection, &ok), explosion_range) &&
           isSoDeep(getMapOfDirection(_map, oppDirection, &ok), BOMB_DELAY - distanceFromFlame) &&
           (!isInView(getMapOfDirection(_map, oppDirection, &ok), FLAME_ENEMY) ||
            distanceTo(getMapOfDirection(_map, oppDirection, &ok), FLAME_ENEMY, 0) > distanceFromFlame))
  {
    direction = oppDirection;
  }
  else
  {
    direction = randomMove(_map, explosion_range, &ok);
  }
  return direction;
}

/**
 * @brief Regard si la direction fournie permet de s'échapper du point fournie
 * Après s'être assuré que le point dans la direction donnée ne met pas en danger:
 * Si le l'on est toujours à porter de la bombe qui vient être posée on regard si les points adjacent (sauf en direction de la bombe bien sûr)
 * permet de s'échapper
 * @param _map 
 * @param _bombDistance 
 * @param explosion_range 
 * @param _direction Direction à vérifier
 * @return true 
 * @return false 
 */
bool isEscape(tree _map, int explosion_range, int _bombDistance, action _direction)
{
  bool ok = true;
  action leftDirection, rightDirection;
  bool checkFront = false, checkLeft = false, checkRight = false;
  if (_map == NULL)
    return false;
  tree map = getMapOfDirection(_map, _direction, &ok);

  if (isAllowed(map) && isSafe(map) && !isInExplosion(map, explosion_range) && isSoDeep(map, 1))
  {
    if (isInView(map, FLAME_ENEMY))
    {
      if (distanceTo(map, FLAME_ENEMY, 0) <= (BOMB_DELAY - _bombDistance))
      {
        return false; //Il y a une chance que le FLAME block le chemin avant que la bombe n'explose
      }
    }
    if (_bombDistance < explosion_range) //on est encore dans l'explosion
    {
      //Calcule des directions de gauche et de droite
      leftDirection = (_direction - 1) % 4;
      if (leftDirection == 0)
        leftDirection = 4;
      rightDirection = (_direction + 1) % 4;
      if (rightDirection == 0)
        rightDirection = 4;
      //La direction doit permettre de faire des aller-retour
      //si c'est le cas alors on regard si c'est une direction par là quelle on peut s'échapper
      if (isSoDeep(getMapOfDirection(map, leftDirection, &ok), 2))
      {
        checkLeft = isEscape(map, explosion_range, explosion_range, leftDirection);
      }
      if (isSoDeep(getMapOfDirection(map, rightDirection, &ok), 2))
      {
        checkRight = isEscape(map, explosion_range, explosion_range, rightDirection);
      }
      if (isSoDeep(getMapOfDirection(map, _direction, &ok), BOMB_DELAY + 1 - _bombDistance))
      {
        checkFront = isEscape(map, explosion_range, _bombDistance + 1, _direction);
      }
      return checkLeft || checkRight || checkFront;
    }
    else //On n'est plus dans l'explosion
      return true;
  } //Le direction n'est pas posible
  return false;
}

/**
 * @brief Vérifie si l'élément est sur la carte fournie
 * 
 * @param _map 
 * @param _elt 
 * @return true if it is
 * @return false if it isn't
 */
bool isInView(tree _map, char _elt)
{
  return (distanceTo(_map, _elt, 0) != -1);
}

/**
 * @brief Donne un déplacement que le BOMBERMAN peut faire. Ce déplacement est choisi aléatoirement, mais en respectant les consignes de sécurité.
 * Toutefois si l'ensemble des règles de sécurité ne peut-être attient alors il suffit que la direction soit autorisée par le jeu.
 * @param _map 
 * @param _error false: aucun déplacement semble posible
 * @return action 
 */
action randomMove(tree _map, int explosion_range, bool *_error)
{
  int tries = 0;
  bool ok = false, error1, error2;
  action a = 1;
  do
  {
    a = rand() % 4 + 1; // ramdomly select an movement

    if (DEBUG)
    { // print the randomly selected action, only in DEBUG mode
      printf("Candidate action is: ");
      printAction(a);
      printf("\n");
    }

    if (isAllowed(getMapOfDirection(_map, a, &error1)) && isSafe(getMapOfDirection(_map, a, &error2)) && !isInExplosion(getMapOfDirection(_map, a, &error2), explosion_range))
      ok = true;
    if (DEBUG)
    { // print whether the randomly selected action is valid, only in DEBUG mode
      printf("Is this candidate action valide? ");
      printBoolean(ok);
      printf("\n");
    }
    tries++;
  } while (!ok && tries < 50); //Si au bout de 50 directions aléatoire on n'a pas trouvé de solution alors on n'en trouvera pas
  if (!(tries < 50))
  { //Si l'on à pas trouvé de solution alors on cherche simplement à se déplacer
    tries = 0;
    do                    //ce cas existe pour empêcher bomberman de prendre une action invalide lorsque aucun déplacement ne convient.
    {                     //Il s'agit d'un déplacement risqué
      a = rand() % 4 + 1; // ramdomly select an movement

      if (DEBUG)
      { // print the randomly selected action, only in DEBUG mode
        printf("Candidate action is: ");
        printAction(a);
        printf("\n");
      }

      if (isAllowed(getMapOfDirection(_map, a, &error1)))
        ok = true;
      if (DEBUG)
      { // print whether the randomly selected action is valid, only in DEBUG mode
        printf("Is this candidate action valide? ");
        printBoolean(ok);
        printf("\n");
      }
      tries++;
    } while (!ok && tries < 50); //Si au bout de 50 directions aléatoire, on n'a pas trouvé de solution alors on n'en trouvera pas
  }
  *_error = (tries < 50);
  return a;
}

/**
 * @brief Retourne la distance vers l'élément donné le plus proche. Parcours uniquement les casses autorisées au bomberman
 * 
 * @param _map 
 * @param _elt la cible recherchée
 * @param _distanceFromBomberman distance du point donné du bomberman
 * @return int distance minimum
 */
int distanceTo(tree _map, char _elt, int _distanceFromBomberman)
{
  int returnDistance;
  int dis[4];
  if (_map != NULL) //Si l'on à atteint la fin de l'arbre alors l'élément n'y est pas
  {
    if (_map->c == _elt) //Si l'on à trouvé l'élément alors il est à la distance du point fournie du bomberman
      returnDistance = _distanceFromBomberman;
    else if (isAllowed(_map)) //Sinon si la case peut quand même être parcourue
    {
      //Cherche l'élément dans les 4 sous-arbres, bien sur la distance du bomberman augment alors de 1
      dis[0] = distanceTo(_map->n, _elt, _distanceFromBomberman + 1);
      dis[1] = distanceTo(_map->e, _elt, _distanceFromBomberman + 1);
      dis[2] = distanceTo(_map->s, _elt, _distanceFromBomberman + 1);
      dis[3] = distanceTo(_map->w, _elt, _distanceFromBomberman + 1);
      //min retourne la distance la plus court en ignorant les -1, sauf si aucune autre option n'est possible
      returnDistance = min(dis, 0, 4);
    }
    else //On n'a pas trouvé l'élément et la casse ne peut pas être parcourue
      returnDistance = -1;
  }
  else //On à atteint la fin de l'arbre
    returnDistance = -1;
  return returnDistance;
}

/**
 * @brief Si le parcourt de ce point est autorisé au bomberman. Sont autorisés les BONUS, les PATH, le BOMBERMAN et EXIT
 * 
 * @param _map 
 * @return true le parcourt est autorisé
 * @return false le parcourt n'est pas autorisé
 */
bool isAllowed(tree _map)
{
  if (_map == NULL)
    return false;
  return (_map->c == FLAME_BONUS || _map->c == BOMB_BONUS || _map->c == PATH || _map->c == BOMBERMAN || _map->c == EXIT);
}

/**
 * @brief Vérifie que le point donné ne met pas en danger
 * 
 * @param _map 
 * @return true 
 * @return false 
 */
bool isSafe(tree _map)
{
  if (_map == NULL)
    return false;
  bool checks = !(_map->c == FLAME_ENEMY || _map->c == GHOST_ENEMY || _map->c == BOMB || _map->c == BOMBERMAN);
  checks = checks && (!isInView(_map, FLAME_ENEMY) || distanceTo(_map, FLAME_ENEMY, 0) > 0);
  if (checks)
    return true;
  else
    return false;
}

/**
 * @brief Vérifie que l'arbre donné est d'au moins la profondeur donnée
 * 
 * @param _map 
 * @param _depth 
 * @return true 
 * @return false 
 */
bool isSoDeep(tree _map, int _depth)
{
  if (_map == NULL) //Si l'on à atteint la fin de l'arbre alors ca veut dire que l'arbre n'est pas de la profondeur donnée
    return false;
  else if (_depth < 1) //la profondeur donnée est inférieure à 1 alors on s'assure que la case est parcourable
  {
    return isAllowed(_map) && isSafe(_map);
  }
  else if (isAllowed(_map) && isSafe(_map)) //Sinon si la case est parcourable on vérifie si l'un de ces sous-arbres est de la profondeur recherché en retirent 1 pour la case actuelle
  {
    return isSoDeep(_map->n, _depth - 1) || isSoDeep(_map->e, _depth - 1) || isSoDeep(_map->s, _depth - 1) || isSoDeep(_map->w, _depth - 1);
  }
  else
    return false;
}

/**
 * @brief Retourne la profondeur maximum d'un arbre
 * 
 * @param _map 
 * @param _depth 
 * @return int 
 */
int getDeep(tree _map, int _depth)
{
  int depth[4];

  if (_map == NULL) //Si l'on a atteint la fin de l'arbre alors on à trouvé la profondeur
    return _depth;
  else if (isAllowed(_map) && isSafe(_map)) //Sinon si la case est parcourable
  {
    //on la profondeur des sous arbres
    depth[0] = getDeep(_map->n, _depth + 1);
    depth[1] = getDeep(_map->e, _depth + 1);
    depth[2] = getDeep(_map->s, _depth + 1);
    depth[3] = getDeep(_map->w, _depth + 1);
    //Et on retourne la profondeur la plus grande
    return max(depth, 0, 4);
  }
  else
    return (_depth);
}

/**
 * @brief Retourne la map de la direction demandée
 * 
 * @param _map 
 * @param _direction 
 * @param _error true: aucune erreur; false: erreur
 * @return tree 
 */
tree getMapOfDirection(tree _map, action _direction, bool *_error)
{
  *_error = true;
  if (_map == NULL)
  {
    *_error = false;
    return _map;
  }
  switch (_direction)
  {
  case NORTH:
    return _map->n;
    break;
  case EAST:
    return _map->e;
    break;
  case SOUTH:
    return _map->s;
    break;
  case WEST:
    return _map->w;
    break;
  default:
    *_error = false; //Si BOMBING est donnée
    return _map;
    break;
  }
}

/**
 * @brief Vérifie que le point ne met pas dans la porter d'une explosion
 * "Regard" en ligne and les quatre directions si un nombre est à porté
 * @param _map 
 * @param _explosion_range 
 * @param _error 
 * @return true a porter d'une explosion
 * @return false pas à porter d'une explosion
 */
bool isInExplosion(tree _map, int _explosion_range)
{
  bool checkNorth, checkEast, checkSouth, checkWest;
  if (!isAllowed(_map) || (_explosion_range < 0)) //aucune bombe de trouver
  {
    return false;
  }
  else if (_map->c == BOMB)
  { //Si c'est une bombe
    return true;
  }
  else
  { //sinon s'assure qu'il n'y a pas de bombe qui peuvent atteindre bomberman
    checkNorth = isInDirection(_map, BOMB, NORTH, _explosion_range);
    checkEast = isInDirection(_map, BOMB, EAST, _explosion_range);
    checkSouth = isInDirection(_map, BOMB, SOUTH, _explosion_range);
    checkWest = isInDirection(_map, BOMB, WEST, _explosion_range);
    return (checkNorth || checkEast || checkSouth || checkWest);
  }
}

/**
 * @brief Regard dans la direction donnée pour l'élément recherché sur une longueur égal à _range
 * 
 * @param _map 
 * @param _elt 
 * @param _direction 
 * @param _range 
 * @return true 
 * @return false 
 */
bool isInDirection(tree _map, char _elt, action _direction, int _range)
{
  bool _error;
  //récupère la map de la direction
  tree map = getMapOfDirection(_map, _direction, &_error);
  if (map == NULL) //Si l'on est en fin d'arbre alors la cible n'est pas dans cette direction
    return false;
  else if (map->c == _elt && _range >= 0) //si on a trouvé l'élément
  {
    return true;
  }
  else if (!isAllowed(map) || _range < 0) //S'il ne sert plus à rien chercher l'élément
  {
    return false;
  }
  else //sinon on cherche une case plus loin
    return isInDirection(map, _elt, _direction, _range - 1);
}

/**
 * @brief Retourne la plus petite valeur du tableau
 * 
 * @param _tab pointeur sur le début de tableau
 * @param _start indice du début de tableau
 * @param _end nombre d'éléments dans le tableau
 * @return int valeur minimum
 */
int min(int *_tab, int _start, int _end)
{
  int mn, mid, tab[2];
  //S'il y a deux éléments dans le tableau
  if ((_end - _start) == 2)
  { //retourne le plus petit des deux, ou -1 si les deux sont à -1
    if ((_tab[_start] < _tab[_start + 1] && _tab[_start] != -1) || _tab[_start + 1] == -1)
      mn = _tab[_start];
    else
      mn = _tab[_start + 1];
  } //S'il n'y a qu'un élément alors on le retourne
  else if ((_end - _start) == 1)
    mn = _tab[_start];
  else
  { //divise le tableau en deux et calcule le plus petit éléments de chaque sous-tableau
    mid = _end / 2 + _start;
    tab[0] = min(_tab, _start, mid);
    tab[1] = min(_tab, mid, _end);
    //retourne le plus petit élément des plus petits éléments
    mn = min(tab, 0, 2);
  }
  return mn;
}

/**
 * @brief Retourne la plus grande valeur du tableau
 * 
 * @param _tab pointeur sur le début de tableau
 * @param _start indice du début de tableau
 * @param _end nombre d'éléments dans le tableau
 * @return int valeur maximum
 */
int max(int *_tab, int _start, int _end)
{
  int mx, mid, tab[2];
  //S'il y a deux éléments dans le tableau
  if ((_end - _start) == 2)
  { //retourne le plus grand des deux, ou -1 si les deux sont à -1
    if ((_tab[_start] > _tab[_start + 1] && _tab[_start] != -1) || _tab[_start + 1] == -1)
      mx = _tab[_start];
    else
      mx = _tab[_start + 1];
  } //S'il n'y a qu'un élément alors on le retourne
  else if ((_end - _start) == 1)
    mx = _tab[_start];
  else
  { //divise le tableau en deux et calcule le plus grand éléments de chaque sous tableau
    mid = _end / 2 + _start;
    tab[0] = max(_tab, _start, mid);
    tab[1] = max(_tab, mid, _end);
    //retourne le plus grand élément des plus grand éléments
    mx = max(tab, 0, 2);
  }
  return mx;
}

/*
  printAction procedure:
  This procedure prints the input action name on screen.
 */
void printAction(action a)
{
  switch (a)
  {
  case BOMBING:
    printf("BOMBING");
    break;
  case NORTH:
    printf("NORTH");
    break;
  case EAST:
    printf("EAST");
    break;
  case SOUTH:
    printf("SOUTH");
    break;
  case WEST:
    printf("WEST");
    break;
  }
}

/*
  printBoolean procedure:
  This procedure prints the input boolan value on screen.
 */
void printBoolean(bool b)
{
  if (b == true)
  {
    printf("true");
  }
  else
  {
    printf("false");
  }
}
