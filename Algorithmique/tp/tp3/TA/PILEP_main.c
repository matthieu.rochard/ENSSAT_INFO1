#include "PILEP.h"

int main(void) {
  int i;
  bool err;
  PILEP p=PILE_creer();

  for(i=1;i<10;i++) {
    PILE_empiler(&p,i,&err);
  }

  PILE_afficher(p);
  PILE_lire(p,&i,&err);
  if(err) {printf("Pile vide\n");} 
  else {printf("Sommet lu : %d\n",i);}

  for(i=9;i>=0;i--) {
    PILE_depiler(&p,&err);
    if(err) {printf("Depilement impossible : Pile vide.\n");}
  }

  PILE_lire(p,&i,&err);
  if(err) {printf("Pile vide\n");} 
  else {printf("Sommet lu : %d\n",i);}
  
  return 0;
}
