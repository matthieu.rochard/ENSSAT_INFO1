#include <stdbool.h>
#include <stdlib.h>
typedef int VAL_TYPE;
struct LISTEP {VAL_TYPE val; struct LISTEP * lien;};

void LISTE_inserer(struct LISTEP **, int, VAL_TYPE, bool *);
void LISTE_supprimer(struct LISTEP **, int, bool *);
int LISTE_rechercher(struct LISTEP *, VAL_TYPE);
void LISTE_selectionner(struct LISTEP *, int, VAL_TYPE *, bool *);
void LISTE_afficher(struct LISTEP *);
