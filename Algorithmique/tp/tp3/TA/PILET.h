#include <stdio.h>
#include "LISTET.h"

typedef struct LISTET PILET;

PILET PILE_creer(void);
void PILE_empiler(PILET *, int, bool *);
void PILE_depiler(PILET *, bool *);
void PILE_lire(PILET, int *, bool *);
void PILE_afficher(PILET);
