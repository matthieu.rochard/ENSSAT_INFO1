#include <stdbool.h>
#define LMAX 10
struct LISTET {int t[LMAX]; int longueur;};

void LISTE_inserer(struct LISTET *, int, int, bool *);
void LISTE_supprimer(struct LISTET *, int, bool *);
int LISTE_rechercher(struct LISTET, int);
void LISTE_selectionner(struct LISTET, int, int *, bool *);
void LISTE_afficher(struct LISTET);
