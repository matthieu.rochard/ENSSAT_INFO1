#include <stdbool.h>
typedef struct {struct LISTEP * liste; int longueur;} FILEP;

FILEP FILE_creer(void);
void FILE_enfiler(FILEP *, VAL_TYPE, bool *);
void FILE_defiler(FILEP *, bool *);
void FILE_lire(FILEP, VAL_TYPE *, bool *);
void FILE_afficher(FILEP);
