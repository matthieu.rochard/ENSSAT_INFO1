#include <stdio.h>
#include "FILET.h"

FILET FILE_creer(void) {
  FILET f;
  f.longueur=0;
  return f;
}

void FILE_enfiler(FILET * pfile, int elt, bool * perr) {
  LISTE_inserer(pfile,(pfile->longueur)+1,elt,perr);
}

void FILE_defiler(FILET * pfile, bool * perr) {
  LISTE_supprimer(pfile,1,perr);
}

void FILE_lire(FILET file, int * pelt, bool * perr) {
  LISTE_selectionner(file,1,pelt,perr);
}

void FILE_afficher(FILET file) {
  int i;
  int elt;
  bool err;
  for(i=1;i<=file.longueur;i++) {
    LISTE_selectionner(file,i,&elt,&err);
    printf("%d<-",elt);
  }
  printf("\n");
}
