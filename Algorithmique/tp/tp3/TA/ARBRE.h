typedef int VAL_TYPE;

struct Noeud
{
    VAL_TYPE val;
    struct Noeud *g;
    struct Noeud *d;
};
typedef struct Noeud *ARBRE;

ARBRE ARBRE_creer(VAL_TYPE, ARBRE, ARBRE);
void ARBRE_lire(ARBRE, VAL_TYPE *, bool *);
void ARBRE_ss_ag(ARBRE, ARBRE *, bool *);
void ARBRE_ss_ad(ARBRE, ARBRE *, bool *);
void ARBRE_aff_valeur(ARBRE, VAL_TYPE, bool *);
void ARBRE_aff_ss_ag(ARBRE, ARBRE, bool *);
void ARBRE_aff_ss_ad(ARBRE, ARBRE, bool *);
ARBRE ARBRE_rechercher(ARBRE, VAL_TYPE);
void ARBRE_afficher(ARBRE);
