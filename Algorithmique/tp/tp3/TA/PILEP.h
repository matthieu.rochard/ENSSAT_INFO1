#include <stdio.h>
#include "LISTEP.h"

typedef struct LISTEP * PILEP;

PILEP PILE_creer(void);
void PILE_empiler(PILEP *, VAL_TYPE, bool *);
void PILE_depiler(PILEP *, bool *);
void PILE_lire(PILEP, VAL_TYPE *, bool *);
void PILE_afficher(PILEP);
