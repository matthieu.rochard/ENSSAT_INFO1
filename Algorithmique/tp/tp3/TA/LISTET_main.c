#include <stdbool.h>
#include <stdio.h>
#include <stdlib.h>
#include <time.h>
#include "LISTET.h"

int main(void) {
  struct LISTET l;
  bool e;
  int i;
  int pos;
  int elt;
  l.longueur=0;
  for(i=1;i<=10;i++) {
    pos=(rand()%i)+1;
    LISTE_inserer(&l,pos,i,&e);
    if(e) {
      printf("L'insertion de %d a echoue.\n",i);
    }
  }

  LISTE_afficher(l);  

  for(i=1;i<=11;i++) {
    pos=LISTE_rechercher(l,i);
    printf("%d est a %d\n",i,pos);
  }

  for(i=1;i<=11;i++) {
    LISTE_selectionner(l,i,&elt,&e);
    if(!e)    printf("%d est a %d\n",elt,i);
  }

  return 0;
}

void affiche(struct LISTET l) {
  int i;
  for(i=0;i<l.longueur;i++) {
    printf("%d ",l.t[i]);
  }
  printf("\n");
}
