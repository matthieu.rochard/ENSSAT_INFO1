/**
 * @file exercice2.c
 * @author your name (you@domain.com)
 * @brief 
 * @version 0.1
 * @date 2020-12-15
 * 
 * @copyright Copyright (c) 2020
 * 
 */
#include <stdio.h>
#include <stdlib.h>

void hanoi(int n, int d, int i, int a);

int main()
{
    hanoi(4, 1, 2, 3);
}

void hanoi(int n, int d, int i, int a)
{
    if (n > 0)
    {
        hanoi(n - 1, d, a, i);
        printf("Deplacer le disque du piquet %d vers le piquet %d\n", d, a);
        hanoi(n - 1, i, d, a);
    }
}