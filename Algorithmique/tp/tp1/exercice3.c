/**
 * @file exercice3.c
 * @author your name (you@domain.com)
 * @brief 
 * @version 0.1
 * @date 2020-12-15
 * 
 * @copyright Copyright (c) 2020
 * 
 */
#include <stdio.h>
#include <stdlib.h>
#include <time.h>

int sommei(int n)
{
    int i = 1, s = 0;
    while (i <= n)
    {
        s = s + i;
        i++;
    }
    return s;
}

int sommer(int n)
{
    if (n > 0)
    {
        return sommer(n - 1) + n;
    }
    else
    {
        return n;
    }
}

int main()
{
    int s;
    clock_t t1, t2;
    float ratio = 1. / CLOCKS_PER_SEC;

    t1 = clock();
    s = sommei(10000);
    t2 = clock();
    t2 = t2 - t1;
    printf("%d en %f s\n", s, t2 * ratio);

    t1 = clock();
    s = sommer(10000);
    t2 = clock();
    t2 = t2 - t1;
    printf("%d en %f s\n", s, t2 * ratio);
}