/**
 * @file exercice1.c
 * @author ROCHARD Matthieu (matthieurochard@yahoo.fr)
 * @brief 
 * @version 0.1
 * @date 2020-12-15
 * 
 * @copyright Copyright (c) 2020
 * 
 */
#include <stdio.h>
#include <stdlib.h>

int pgcdR(int, int);
int pgcdI(int, int);

int main()
{
    printf("%d\n", pgcdR(30, 24));
    printf("%d\n", pgcdI(30, 24));
}

int pgcdR(int a, int b)
{
    if (b == 0)
    {
        return a;
    }
    else
    {
        return pgcdR(b, a % b);
    }
}

int pgcdI(int a, int b)
{
    int tmp;
    while (b != 0)
    {
        tmp = b;
        b = a % b;
        a = tmp;
    }
    return a;
}