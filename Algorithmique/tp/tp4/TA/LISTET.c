#include <stdbool.h>
#include <stdio.h>
#include "LISTET.h"

void LISTE_inserer(struct LISTET * pl, int pos, int elt, bool * perr) {
  int i;
  /* on peut inserer */
  if(pl->longueur<LMAX && pos>0
     && pos<=pl->longueur+1) {
    /* indicateur d'erreur a FAUX */
    *perr=false;
    /* decalage des valeurs avant l'insertion a pos */
    for(i=pl->longueur;i>pos-1;i--) {
      pl->t[i]=pl->t[i-1];
    }
    /* insertion d'elt a pos */
    pl->t[pos-1]=elt;
    /* maj de la longueur de la liste */
    pl->longueur++;
  } /* on ne peut pas inserer */
  else {
    /* indicateur d'erreur a VRAI */
    *perr=true;
  }
}

void LISTE_supprimer(struct LISTET * pl, int pos, bool * perr) {
  int i;
  if(pos>0 && pos<=pl->longueur) { /* on peut effacer */
    *perr=false;
    /* on decale les elements a gauche a partir de pos */
    for(i=pos-1;i<pl->longueur-1;i++) {
      pl->t[i]=pl->t[i+1];
    }
    pl->longueur--; /* maj de la longueur de liste */
  } else { /* on ne peut pas effacer */
    *perr=true;
  }
}

int LISTE_rechercher(struct LISTET pl, int elt) {
  int i=0;
  /* on avance jusqu'a trouver l'elt ou la fin de liste */
  while(i<pl.longueur) {
    /* on a trouve */
    if(pl.t[i]==elt) {
      return i+1;
    } else {
      i++;
    }
  }
  /* on n'a pas trouve */
  return 0;
}

void LISTE_selectionner(struct LISTET l, int pos, int * elt, bool * perr) {
  /* on peut selectionner */
  if(pos>0 && pos<=l.longueur) {
    *perr=false;
    *elt= l.t[pos-1];
    }
  /* on ne peut pas selectionner */
  else {
    *perr=true;
  }
}

void LISTE_afficher(struct LISTET l) {
  int i;
  for(i=0;i<l.longueur;i++) {
    printf("%d ",l.t[i]);
  }
  printf("\n");
}
