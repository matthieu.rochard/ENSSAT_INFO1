#include "LISTET.h"

typedef struct LISTET FILET;

FILET FILE_creer(void);
void FILE_enfiler(FILET *, int, bool *);
void FILE_defiler(FILET *, bool *);
void FILE_lire(FILET, int *, bool *);
void FILE_afficher(FILET);
