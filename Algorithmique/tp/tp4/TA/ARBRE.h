typedef int VAR_TYPE;

struct Noeud
{
    VAR_TYPE val;
    struct Noeud *g;
    struct Noeud *d;
};
typedef struct Noeud *ARBRE;

ARBRE ARBRE_creer(VAR_TYPE, ARBRE, ARBRE);
void ARBRE_lire(ARBRE, VAR_TYPE *, bool *);
void ARBRE_ss_ag(ARBRE, ARBRE *, bool *);
void ARBRE_ss_ad(ARBRE, ARBRE *, bool *);
void ARBRE_aff_valeur(ARBRE, VAR_TYPE, bool *);
void ARBRE_aff_ss_ag(ARBRE, ARBRE, bool *);
void ARBRE_aff_ss_ad(ARBRE, ARBRE, bool *);
ARBRE ARBRE_rechercher(ARBRE, VAR_TYPE);
void ARBRE_afficher(ARBRE);
