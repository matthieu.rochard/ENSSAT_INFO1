#include <stdbool.h>
#include <stdio.h>
#include <stdlib.h>
#include <time.h>
#include "LISTEP.c"

int main(void)
{
  struct LISTEP *pl = NULL;
  bool e;
  int i;
  int pos;
  int elt;

  srand(time(NULL));
  for (i = 1; i <= 10; i++)
  {
    pos = (rand() % i) + 1;
    LISTE_inserer(&pl, pos, i, &e);
    if (e)
    {
      printf("L'insertion de %d a echoue.\n", i);
    }
  }
  LISTE_afficher(pl);

  LISTE_add(&pl,1000);
  LISTE_afficher(pl);
  
  for (i = 1; i <= 11; i++)
  {
    pos = LISTE_rechercher(pl, i);
    printf("%d est a %d\n", i, pos);
  }

  for (i = 1; i <= 11; i++)
  {
    LISTE_selectionner(pl, i, &elt, &e);
    if (!e)
      printf("%d est a %d\n", elt, i);
  }

  return 0;
}
