#include <stdio.h>
#include "FILEP.h"

int main(void) {
  FILEP f=FILE_creer();
  int i;
  bool err;
  for(i=1;i<=10;i++) {
    FILE_enfiler(&f,i,&err);
    if(err) printf("Erreur d'enfilement : memoire pleine.\n");
  }
  FILE_afficher(f);
  FILE_lire(f,&i,&err);
  if(err) {
    printf("Erreur de lecture : file vide.\n");
  } else {
    printf("Tete=%d\n",i);
  }
  
  for(i=10;i>=0;i--) {
    FILE_defiler(&f,&err);
    if(err) printf("Erreur de defilement : file vide.\n");
  }
  FILE_afficher(f);
  FILE_lire(f,&i,&err);
  if(err) {
    printf("Erreur de lecture : file vide.\n");
  } else {
    printf("Tete=%d\n",i);
  }
}
