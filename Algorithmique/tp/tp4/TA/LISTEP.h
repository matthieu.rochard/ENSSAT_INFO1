#include <stdbool.h>
#include <stdlib.h>
typedef int VAR_TYPE;

struct LISTEP {VAR_TYPE val; struct LISTEP * lien;};

void LISTE_inserer(struct LISTEP **, int, int, bool *);
void LISTE_supprimer(struct LISTEP **, int, bool *);
int LISTE_rechercher(struct LISTEP *, VAR_TYPE);
void LISTE_selectionner(struct LISTEP *, int, VAR_TYPE *, bool *);
void LISTE_afficher(struct LISTEP *);
