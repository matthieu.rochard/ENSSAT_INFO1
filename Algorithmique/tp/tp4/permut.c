/**
 * @file permute.c
 * @author ROCHARD Matthieu (you@domain.com)
 * @brief Code C du tp5 d'algo2
 * @version 0.1
 * @date 2021-01-13
 * 
 * @copyright Copyright (c) 2021
 * 
 */
#include <stdio.h>
#include <stdlib.h>
#include "TA/ARBRE.c"
#include "TA/LISTEP.c"

bool ARBRE_permut(ARBRE, VAR_TYPE, VAR_TYPE);
void ARBRE_symetrie(ARBRE);
void ARBRE_toList(ARBRE,struct LISTEP**);
void printBool(bool);

int main()
{
    struct LISTEP *pl = NULL;
    ARBRE a =
        ARBRE_creer(1,
                    ARBRE_creer(2,
                                ARBRE_creer(3, NULL, NULL),
                                ARBRE_creer(4, NULL, NULL)),
                    ARBRE_creer(5,
                                ARBRE_creer(6, NULL, NULL),
                                NULL));
    printf("Création de la liste...\n");
    ARBRE_afficher(a);
    printf("\n");

    printf("Test de la permutation...\n");
    printf("--->Success(True attendu): ");
    printBool(ARBRE_permut(a, 5, 6));
    printf("\n");
    ARBRE_afficher(a);
    printf("\n");

    printf("--->Success(False attendu): ");
    printBool(ARBRE_permut(a, 9, 6));
    printf("\n");
    ARBRE_afficher(a);
    printf("\n");

    printf("--->Success(False attendu): ");
    printBool(ARBRE_permut(a, 9, 10));
    printf("\n");
    ARBRE_afficher(a);
    printf("\n");
    
    printf("Test de la symetrie...\n");
    ARBRE_symetrie(a);
    ARBRE_afficher(a);
    printf("\n");
    
    printf("Test de la converstion en liste...\n");    
    ARBRE_toList(a,&pl);
    LISTE_afficher(pl);

    return 0;
}

/**
 * @brief permute les deux éléments s'ils sont présents dans l'arbre
 * 
 * @param a l'arbre dans le quel la permutation doit ce faire 
 * @param elt1 l'un des éléments à retrouver et à permuter 
 * @param elt2 l'un des éléments à retrouver et à permuter 
 * @return true si les deux éléments ont été trouvés dans l'arbre et ont donc pu être permutés
 * @return false si l'un ou les deux éléments n'ont été trouvés dans l'arbre et n'ont donc pu être permutés
 */
bool ARBRE_permut(ARBRE a, VAR_TYPE elt1, VAR_TYPE elt2)
{
    ARBRE a_elt1, a_elt2;

    a_elt1 = ARBRE_rechercher(a, elt1);
    if (a_elt1 == NULL)
        return false;

    a_elt2 = ARBRE_rechercher(a, elt2);
    if (a_elt2 == NULL)
        return false;

    a_elt1->val = elt2;
    a_elt2->val = elt1;
    return true;
}

/**
 * @brief Retourne un nouvel arbre symétrique à celui fournie.
 * 
 * @param a l'arbre à modifier.
 */
void ARBRE_symetrie(ARBRE a){
    ARBRE tmp;
    tmp = a->g;
    a->g = a->d;
    a->d = tmp;
    if(a->g!=NULL) ARBRE_symetrie(a->g);
    if(a->d!=NULL) ARBRE_symetrie(a->d);
}

/**
 * @brief prend en entrée un arbre et le range dans une liste selon un parcours préfixe, infixe ou sufixe.
 * 
 * @param a l'arbre à convertir.
 * @param ppl Pointer sur le pointer de la structure liste qui contindera la liste finale.
 */
void ARBRE_toList(ARBRE a,struct LISTEP** ppl){
    bool ierr;
    if(a->d!=NULL) ARBRE_toList(a->d,ppl);
    
    LISTE_inserer(ppl,1,a->val,&ierr);
    
    if(a->g!=NULL) ARBRE_toList(a->g,ppl);   
}

void printBool(bool b){
    if(b) printf("True");
    else printf("False");
}