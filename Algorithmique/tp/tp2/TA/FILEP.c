#include "LISTEP.h"
#include "FILEP.h"
#include <stdio.h>
#include <stdlib.h>

FILEP FILE_creer(void) {
  FILEP f;
  f.liste=NULL;
  f.longueur=0;
  return f;
}

void FILE_enfiler(FILEP * pfile, int elt, bool * perr) {
  LISTE_inserer(&(pfile->liste),pfile->longueur+1,elt,perr);
  if(!*perr) {
    pfile->longueur++;
  }
}

void FILE_defiler(FILEP * pfile, bool * perr) {
  LISTE_supprimer(&(pfile->liste),1,perr);
  if(!*perr) {
    pfile->longueur--;
  }
}

void FILE_lire(FILEP file, int * pelt, bool * perr) {
  LISTE_selectionner(file.liste,1,pelt,perr);
}

void FILE_afficher(FILEP file) {
  int i;
  int elt;
  bool err;
  for(i=1;i<=file.longueur;i++) {
    LISTE_selectionner(file.liste,i,&elt,&err);
    printf("%d<-",elt);
  }
  printf("\n");
}
