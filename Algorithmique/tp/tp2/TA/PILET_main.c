#include "PILET.h"

int main(void) {
  PILET p=PILE_creer();
  int i;
  bool err;

  for(i=1;i<=LMAX+1;i++) {
    PILE_empiler(&p,i,&err);
    if(err) {printf("Pile pleine, sommet a %d\n",p.longueur);}
  }

  PILE_afficher(p);
  PILE_lire(p,&i,&err);
    if(err) {printf("Pile vide, sommet a %d\n",p.longueur);} 
    else {printf("Sommet lu : %d\n",i);}

  for(i=LMAX+1;i>=0;i--) {
    PILE_depiler(&p,&err);
    if(err) {printf("Pile vide, sommet a %d\n",p.longueur);}
  }

  PILE_afficher(p);
  printf("long=%d\n",p.longueur);
  PILE_lire(p,&i,&err);
    if(err) {printf("Pile vide, sommet a %d\n",p.longueur);} 
    else {printf("Sommet lu : %d\n",i);}
  return 0;
}
