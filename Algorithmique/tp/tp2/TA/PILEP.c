#include "PILEP.h"

PILEP PILE_creer(void) {
  return NULL;
}

void PILE_empiler(PILEP * ppile, int elt, bool * perr) {
  LISTE_inserer(ppile,1,elt,perr); 
}

void PILE_depiler(PILEP * ppile, bool * perr) {
  LISTE_supprimer(ppile,1,perr);
}

void PILE_lire(PILEP pile, int * pelt, bool * perr) {
  LISTE_selectionner(pile,1,pelt,perr);
}

void PILE_afficher(PILEP pile) {
  while(pile!=NULL) {
    printf("+-+\n|%d|\n",pile->val);
    pile=pile->lien;
  }
  printf("+-+\n");
}
