
struct Noeud
{
    float val;
    struct Noeud *g;
    struct Noeud *d;
};
typedef struct Noeud *ARBRE;

ARBRE ARBRE_creer(float, ARBRE, ARBRE);
void ARBRE_lire(ARBRE, float *, bool *);
void ARBRE_ss_ag(ARBRE, ARBRE *, bool *);
void ARBRE_ss_ad(ARBRE, ARBRE *, bool *);
void ARBRE_aff_valeur(ARBRE, float, bool *);
void ARBRE_aff_ss_ag(ARBRE, ARBRE, bool *);
void ARBRE_aff_ss_ad(ARBRE, ARBRE, bool *);
ARBRE ARBRE_rechercher(ARBRE, float);
void ARBRE_afficher(ARBRE);
