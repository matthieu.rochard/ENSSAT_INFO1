#include <stdbool.h>
#include <stdlib.h>

struct LISTEP {int val; struct LISTEP * lien;};

void LISTE_inserer(struct LISTEP **, int, int, bool *);
void LISTE_supprimer(struct LISTEP **, int, bool *);
int LISTE_rechercher(struct LISTEP *, int);
void LISTE_selectionner(struct LISTEP *, int, int *, bool *);
void LISTE_afficher(struct LISTEP *);
