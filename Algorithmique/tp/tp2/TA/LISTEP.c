#include <stdio.h>
#include "LISTEP.h"

void LISTE_inserer(struct LISTEP * * ppl, int pos, int elt, bool * perr) {
  int i;
  struct LISTEP * pmaillon;
  struct LISTEP * pmaillontmp;
  pmaillon = (struct LISTEP *) malloc(sizeof(struct LISTEP));
  if(pmaillon!=NULL) { 
    *perr=false; /* a priori on peut inserer */
    pmaillon->val=elt; /* on affecte elt au nouveau maillon */
    if(pos==1) {/* on insere en tete */
      pmaillon->lien=*ppl;
      *ppl=pmaillon;
    } else { /* sinon on va au maillon pos-1, si possible */
      pmaillontmp=*ppl;
      for(i=1;i<pos-1;i++) {
	if(pmaillontmp!=NULL) {
	  pmaillontmp=pmaillontmp->lien;
	} else { /* fin de liste prematuree */
	  *perr=true;
	  break; }
      }
      if(!*perr) { /* on insere le maillon dans la chaine */
	pmaillon->lien=pmaillontmp->lien;
	pmaillontmp->lien=pmaillon; }
    }
  } else { /* plus de memoire */
    *perr=true; }
}

void LISTE_supprimer(struct LISTEP * * ppl, int pos, bool * perr) {
  int i=1;
  struct LISTEP * pmaillon=*ppl;
  struct LISTEP * pmaillontmp;
  if(*ppl==NULL) { /* erreur si liste vide */
    *perr=true;
  } else if (pos==1) { /* suppression en tete */
    *ppl=(*ppl)->lien;
    free(pmaillon);
    *perr=false;
  } else { /* on va a pos-1, si possible */
    *perr=false;
    while(!*perr && i<pos-1) {
      pmaillon=pmaillon->lien;
      if(pmaillon!=NULL) {
	i++;
      } else {
	*perr=true;
      }
    } /* si pas d'erreur on supprime a pos */
    if(!*perr && pmaillon->lien!=NULL) {
      pmaillontmp=pmaillon->lien;
      pmaillon->lien=pmaillontmp->lien;
      free(pmaillontmp);
    } else {
      *perr=true;
    }
  }
}

int LISTE_rechercher(struct LISTEP * pl, int elt) {
  struct LISTEP * pltmp=pl;
  int i=1;
  /* on avance jusqu'a trouver l'elt */
  while(pltmp!=NULL) {
    /* on a trouve */
    if(pltmp->val==elt) {
      return i;
    }
    pltmp=pltmp->lien;
    i++;
  }
  /* on n'a pas trouve */
  return 0;
}

void LISTE_selectionner(struct LISTEP * pl, int pos, int * pelt, bool * perr) {
  int i=1;
  struct LISTEP * pltmp=pl;
  /* on va a pos, si possible */
  while(pltmp!=NULL && i<pos) {
    pltmp=pltmp->lien;
    i++;
  }
  /* on peut selectionner */
  if(pltmp!=NULL) {
    *pelt=pltmp->val;
    *perr=false;
  } 
  /* on ne peut pas selectionner */
  else {
    *perr=true;
  }
}


void LISTE_afficher(struct LISTEP * pl) {
  while(pl!=NULL) {
    printf("%d ",pl->val);
    pl=pl->lien;
  }
  printf("\n");
}
