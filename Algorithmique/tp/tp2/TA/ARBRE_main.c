#include <stdbool.h>
#include <stdio.h>
#include <stdlib.h>
#include "ARBRE.h"

int main(void) {
  ARBRE a=
    ARBRE_creer(1,
	  ARBRE_creer(2,
		ARBRE_creer(3,NULL,NULL),
		ARBRE_creer(4,NULL,NULL)),
	  ARBRE_creer(5,
		ARBRE_creer(6,NULL,NULL),
		NULL));
  ARBRE_afficher(a);
  printf("\n");

  ARBRE_afficher(ARBRE_rechercher(a,8));
  printf("\n");
  return 0;
}
