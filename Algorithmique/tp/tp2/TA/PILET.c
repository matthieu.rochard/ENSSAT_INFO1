#include "PILET.h"

PILET PILE_creer(void) {
  PILET p;
  p.longueur=0;
  return p;
}

void PILE_empiler(PILET * ppile, int elt, bool * perr) {
  LISTE_inserer(ppile,(ppile->longueur)+1,elt,perr);
}

void PILE_depiler(PILET * ppile, bool * perr) {
  LISTE_supprimer(ppile,ppile->longueur,perr);
}

void PILE_lire(PILET pile, int * pelt, bool * perr) {
  LISTE_selectionner(pile,pile.longueur,pelt,perr);
}

void PILE_afficher(PILET p) {
  int i;

  for(i=p.longueur-1;i>=0;i--) {
    printf("+-+\n|%d|\n",p.t[i]);
  }
  printf("+-+\n");
  printf("Sommet a %d\n",p.longueur);
}

