#include <stdbool.h>
#include <stdio.h>
#include <stdlib.h>
#include "ARBRE.h"

ARBRE ARBRE_creer(float i, ARBRE ag, ARBRE ad)
{
  ARBRE a = (ARBRE)malloc(sizeof(struct Noeud));
  if (a != NULL)
  {
    a->val = i;
    a->g = ag;
    a->d = ad;
  }
  return a;
}

void ARBRE_lire(ARBRE a, float *pelt, bool *perr)
{
  if (a == NULL)
  {
    *perr = true;
  }
  else
  {
    *perr = false;
    *pelt = a->val;
  }
}

void ARBRE_ss_ag(ARBRE a, ARBRE *pssag, bool *perr)
{
  if (a == NULL)
  {
    *perr = true;
  }
  else
  {
    *perr = false;
    *pssag = a->g;
  }
}

void ARBRE_ss_ad(ARBRE a, ARBRE *pssad, bool *perr)
{
  if (a == NULL)
  {
    *perr = true;
  }
  else
  {
    *perr = false;
    *pssad = a->d;
  }
}

void ARBRE_aff_valeur(ARBRE a, float elt, bool *perr)
{
  if (a == NULL)
  {
    *perr = true;
  }
  else
  {
    *perr = false;
    a->val = elt;
  }
}
void ARBRE_aff_ss_ag(ARBRE a, ARBRE ag, bool *perr)
{
  if (a == NULL)
  {
    *perr = true;
  }
  else
  {
    *perr = false;
    a->g = ag;
  }
}
void ARBRE_aff_ss_ad(ARBRE a, ARBRE ad, bool *perr)
{
  if (a == NULL)
  {
    *perr = true;
  }
  else
  {
    *perr = false;
    a->d = ad;
  }
}

ARBRE ARBRE_rechercher(ARBRE a, float elt)
{
  ARBRE at;
  /*cas de l'arbre vide */
  if (a == NULL)
  {
    return NULL;
  }
  else
  {
    /* si on a trouve */
    if (a->val == elt)
    {
      return a;
    }
    else
    {
      /* sinon on cherche a gauche */
      at = ARBRE_rechercher(a->g, elt);
      if (at != NULL)
      {
        return at;
      }
      else
      {
        /* si pas trouve a gauche, on cherche a droite */
        return ARBRE_rechercher(a->d, elt);
      }
    }
  }
}

void ARBRE_afficher(ARBRE a)
{
  if (a == NULL)
  {
    printf("0");
  }
  else
  {
    printf("<");
    printf("%d", a->val);
    printf(",");
    ARBRE_afficher(a->g);
    printf(",");
    ARBRE_afficher(a->d);
    printf(">");
  }
}
