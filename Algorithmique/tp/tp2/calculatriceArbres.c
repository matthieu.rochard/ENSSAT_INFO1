/**
 * @file calculatriceArbres.c
 * @author your name (you@domain.com)
 * @brief 
 * @version 0.1
 * @date 2020-12-15
 * 
 * @copyright Copyright (c) 2020
 * 
 */
#include <stdio.h>
#include <stdlib.h>
#include "TA/ARBRE.c"

#define SUB 1.
#define ADD 0.
#define MUL 2.
#define DIV 3.

const char opp[] = "+-*/";

float evalNoeud(ARBRE a);
void calculatrice(ARBRE arb);
void affNoeud(ARBRE a);

int main()
{
    ARBRE arbre =
        ARBRE_creer(SUB,
                    ARBRE_creer(ADD,
                                ARBRE_creer(3., NULL, NULL),
                                ARBRE_creer(4., NULL, NULL)),
                    ARBRE_creer(DIV,
                                ARBRE_creer(MUL,
                                            ARBRE_creer(ADD,
                                                        ARBRE_creer(3., NULL, NULL),
                                                        ARBRE_creer(4., NULL, NULL)),
                                            ARBRE_creer(3., NULL, NULL)),
                                ARBRE_creer(2., NULL, NULL)));
    calculatrice(arbre);
}

void calculatrice(ARBRE arb)
{
    affNoeud(arb);
    printf("\nEval= %f", evalNoeud(arb));
}

void affNoeud(ARBRE a)
{
    printf("<");
    if (a->d == NULL && a->g == NULL)
    {
        printf("%f,NULL,NULL", a->val);
    }
    else
    {
        printf("%c,", opp[(int)a->val]);
        if (a->g == NULL)
        {
            printf("NULL,");
            affNoeud(a->d);
        }
        else if (a->d == NULL)
        {
            affNoeud(a->g);
            printf(",NULL");
        }
        else
        {
            affNoeud(a->g);
            printf(",");
            affNoeud(a->d);
        }
    }
    printf(">");
}

float evalNoeud(ARBRE a)
{
    if (a->d == NULL && a->g == NULL)
    {
        return a->val;
    }
    else
    {
        if (a->val == ADD)
        {
            return evalNoeud(a->g) + evalNoeud(a->d);
        }
        else if (a->val == SUB)
        {
            return evalNoeud(a->g) - evalNoeud(a->d);
        }
        else if (a->val == DIV)
        {
            return evalNoeud(a->g) / evalNoeud(a->d);
        }
        else if (a->val == MUL)
        {
            return evalNoeud(a->g) * evalNoeud(a->d);
        }
    }
}