const avl = require('./resultat_avl.json');
const abr = require('./resultat_abr.json');
const liste = require('./resultat_liste.json');
const hashT = require('./resultat_hashtable.json');
const stuff = ["10", "1000", "2000", "4000", "10000", "20000", "40000", "100000"]


var tmp;
console.log('{');
//ajoutComp
console.log("\"ajoutComp\": {");
for (i = 0; i < 8; i++) {
    console.log('"' + stuff[i] + '"' + ":{ \"avl\":" + avl[stuff[i]]['ajoutComp'] + "," +
        "\"abr\":" + abr[stuff[i]]['ajoutComp'] + "," +
        "\"liste\":" + liste[stuff[i]]['ajoutComp'] + "," +
        "\"hashTable\":" + hashT[stuff[i]]['ajoutComp'] + "" +
        "},")
}

console.log("}");//ajoutTmp
console.log("\"ajoutTmp\": {");
for (i = 0; i < 8; i++) {
    console.log('"' + stuff[i] + '"' + ":{ \"avl\":" + avl[stuff[i]]["ajoutTmp"] + "," +
        "\"abr\":" + abr[stuff[i]]["ajoutTmp"] + "," +
        "\"liste\":" + liste[stuff[i]]["ajoutTmp"] + "," +
        "\"hashTable\":" + hashT[stuff[i]]['ajoutTmp'] + "" +
        "},")
}

console.log("}");
//rechercheComp
console.log("\"rechercheComp\": {");
for (i = 0; i < 8; i++) {
    console.log('"' + stuff[i] + '"' + ":{ \"avl\":" + avl[stuff[i]]["rechercheComp"] + "," +
        "\"abr\":" + abr[stuff[i]]["rechercheComp"] + "," +
        "\"liste\":" + liste[stuff[i]]["rechercheComp"] + "," +
        "\"hashTable\":" + hashT[stuff[i]]['rechercheComp'] + "" +
        "},")
}

console.log("}");
//rechercheTmp
console.log("\"rechercheTmp\": {");
for (i = 0; i < 8; i++) {
    console.log('"' + stuff[i] + '"' + ":{ \"avl\":" + avl[stuff[i]]["rechercheTmp"] + "," +
        "\"abr\":" + abr[stuff[i]]["rechercheTmp"] + "," +
        "\"liste\":" + liste[stuff[i]]["rechercheTmp"] + "," +
        "\"hashTable\":" + hashT[stuff[i]]['rechercheTmp'] + "" +
        "},")
}
console.log("}");

console.log("}");