<<<<<<< HEAD
/* ==================================== Include    =================================== */
#include <stdio.h>
#include <stdlib.h>
#include <stdbool.h>

#include "liste.h"

/* ==================================== Functions and procedures =================================== */
T_liste creer_liste()
{
    return NULL;
}
// ajout en tete de liste de element avec modification de la liste
void ajout_entete_liste(T_liste *liste, t_film *element)
{
    T_maillon *newNoeud = (T_liste)malloc(sizeof(T_maillon));
    newNoeud->val = element;
    newNoeud->lien = *liste;
    *liste = newNoeud;
}
// ajout en tete selon une approche fonctionnelle sans modification de la liste
//T_liste ajouter_entete_fonc(T_liste *liste, int element)

void ajout_enqueu_liste(T_liste *liste, t_film *element)
{

    if (*liste == NULL)
    {
        T_maillon *newNoeud = (T_liste)malloc(sizeof(T_maillon));
        newNoeud->val = element;
        newNoeud->lien = *liste;
        *liste = newNoeud;
    }
    else
    {
        ajout_enqueu_liste(&(*liste)->lien, element);
    }
}
// affichages
// itératif
void afficher_liste(T_liste liste)
{
    T_liste showListe = liste;

    while (showListe != NULL)
    {
        printf(" %d->", showListe->val->num);
        showListe = showListe->lien;
    }
    printf("\n");
}
// récursif dans l ordre de la liste
void fafficher_dir_liste(T_liste liste)
{
    if (liste != NULL)
    {
        printf(" %d->", liste->val->num);
        fafficher_dir_liste(liste->lien);
    }
    else
    {
        printf("\n");
    }
}
// récursif dans l ordre inverse de la liste
void fafficher_inv_liste(T_liste liste)
{
    if (liste != NULL)
    {
        fafficher_inv_liste(liste->lien);
        printf("<-%d ", liste->val->num);
    }
}

//inversion
// inversion iterative d'une liste selon une approche MIS
void inv_mis_liste(T_liste *liste)
{
    T_liste tmpliste2;
    T_liste tmpliste1 = (*liste)->lien;
    T_liste cible = *liste;
    (*liste)->lien = NULL;
    while (tmpliste1 != NULL)
    {
        tmpliste2 = tmpliste1->lien;
        tmpliste1->lien = cible;
        cible = tmpliste1;
        tmpliste1 = tmpliste2;
    }
    *liste = cible;
}

// inversion iterative d'une liste selon une approche fonctionnelle
T_liste inv_fonc_liste(T_liste liste)
{
    T_liste newListe = creer_liste();
    T_liste tmpListe = liste;
    while (tmpListe != NULL)
    {
        ajout_entete_liste(&newListe, tmpListe->val);
        tmpListe = tmpListe->lien;
    }
    return newListe;
}

// comptage du nombre d'elements d'une liste
// selon une approche recursive enveloppee
int long_env(T_liste liste)
{
    if (liste->lien == NULL)
        return 1;
    else
        return 1 + long_env(liste->lien);
}
// selon une approche recursive terminale
int long_ter(T_liste liste, int aux)
{
    if (liste->lien == NULL)
        return aux + 1;
    else
        return long_ter(liste->lien, aux + 1);
}

void liberer_liste(T_liste liste)
{
    if (liste != NULL)
    {
        liberer_liste(liste->lien);
        free(liste);
    }
}

bool TLISTE_rechercher(T_liste l, int i)
{
    operation_elementaire();
    if (l == NULL)
        return false;
    else
    {
        operation_elementaire();
        if (l->val->num == i)
            return true;
        else
            return TLISTE_rechercher(l->lien, i);
    }
=======
/* ==================================== Include    =================================== */
#include <stdio.h>
#include <stdlib.h>
#include <stdbool.h>

#include "liste.h"

/* ==================================== Functions and procedures =================================== */
T_liste creer_liste()
{
    return NULL;
}
// ajout en tete de liste de element avec modification de la liste
void ajout_entete_liste(T_liste *liste, t_film *element)
{
    T_maillon *newNoeud = (T_liste)malloc(sizeof(T_maillon));
    newNoeud->val = element;
    newNoeud->lien = *liste;
    *liste = newNoeud;
}
// ajout en tete selon une approche fonctionnelle sans modification de la liste
//T_liste ajouter_entete_fonc(T_liste *liste, int element)

void ajout_enqueu_liste(T_liste *liste, t_film *element)
{

    if (*liste == NULL)
    {
        T_maillon *newNoeud = (T_liste)malloc(sizeof(T_maillon));
        newNoeud->val = element;
        newNoeud->lien = *liste;
        *liste = newNoeud;
    }
    else
    {
        ajout_enqueu_liste(&(*liste)->lien, element);
    }
}
// affichages
// itératif
void afficher_liste(T_liste liste)
{
    T_liste showListe = liste;

    while (showListe != NULL)
    {
        printf(" %d->", showListe->val->num);
        showListe = showListe->lien;
    }
    printf("\n");
}
// récursif dans l ordre de la liste
void fafficher_dir_liste(T_liste liste)
{
    if (liste != NULL)
    {
        printf(" %d->", liste->val->num);
        fafficher_dir_liste(liste->lien);
    }
    else
    {
        printf("\n");
    }
}
// récursif dans l ordre inverse de la liste
void fafficher_inv_liste(T_liste liste)
{
    if (liste != NULL)
    {
        fafficher_inv_liste(liste->lien);
        printf("<-%d ", liste->val->num);
    }
}

//inversion
// inversion iterative d'une liste selon une approche MIS
void inv_mis_liste(T_liste *liste)
{
    T_liste tmpliste2;
    T_liste tmpliste1 = (*liste)->lien;
    T_liste cible = *liste;
    (*liste)->lien = NULL;
    while (tmpliste1 != NULL)
    {
        tmpliste2 = tmpliste1->lien;
        tmpliste1->lien = cible;
        cible = tmpliste1;
        tmpliste1 = tmpliste2;
    }
    *liste = cible;
}

// inversion iterative d'une liste selon une approche fonctionnelle
T_liste inv_fonc_liste(T_liste liste)
{
    T_liste newListe = creer_liste();
    T_liste tmpListe = liste;
    while (tmpListe != NULL)
    {
        ajout_entete_liste(&newListe, tmpListe->val);
        tmpListe = tmpListe->lien;
    }
    return newListe;
}

// comptage du nombre d'elements d'une liste
// selon une approche recursive enveloppee
int long_env(T_liste liste)
{
    if (liste->lien == NULL)
        return 1;
    else
        return 1 + long_env(liste->lien);
}
// selon une approche recursive terminale
int long_ter(T_liste liste, int aux)
{
    if (liste->lien == NULL)
        return aux + 1;
    else
        return long_ter(liste->lien, aux + 1);
}

void liberer_liste(T_liste liste)
{
    if (liste != NULL)
    {
        liberer_liste(liste->lien);
        free(liste);
    }
}

bool TLISTE_rechercher(T_liste l, int i)
{
    operation_elementaire();
    if (l == NULL)
        return false;
    else
    {
        operation_elementaire();
        if (l->val->num == i)
            return true;
        else
            return TLISTE_rechercher(l->lien, i);
    }
>>>>>>> main
}