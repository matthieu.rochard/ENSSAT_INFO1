
#ifndef _HashTable_h
#define _HashTable_h
#include <stdbool.h>
#include "liste.h"
#include "film.h"
#include "complexite.h"

#define HASHTABLE_SIZE 10

typedef T_liste *T_hashTable;

void creerHashTable(T_hashTable *);
void insert(T_hashTable *, t_film *);
int hashFonction(int);
bool rechercheHashTable(T_hashTable, int);

void afficher_hashTable(T_hashTable t);
#endif