<<<<<<< HEAD
#include <stdio.h>
#include <stdlib.h>
#include <stdbool.h>
#include "avl.h"

void printBool(bool b);
void printlnBool(bool b);
T_AVL TAVL_creer()
{
    return NULL;
}
T_AVL TAVL_creerNoeud(t_film *i)
{
    T_AVL a = (T_AVL)malloc(sizeof(struct T_noeudAVL));
    a->d = NULL;
    a->g = NULL;
    a->val = i;
    a->eq = 0;
    return a;
}
void rotation_d(T_AVL *pa)
{
    T_AVL racine = (*pa)->g;
    (*pa)->g = racine->d;
    racine->d = (*pa);
    *pa = racine;
}

void rotation_g(T_AVL *pa)
{
    T_AVL racine = (*pa)->d;
    (*pa)->d = racine->g;
    racine->g = (*pa);
    *pa = racine;
}

void rotation_d_g(T_AVL *pa)
{
    rotation_d(&((*pa)->d));
    rotation_g(pa);
    // (*pa)->d->eq--;
}

void rotation_g_d(T_AVL *pa)
{
    rotation_g(&((*pa)->g));
    rotation_d(pa);
    //(*pa)->g->eq++;
}

void insertion(T_AVL *T, t_film *i, int *h)
{
    operation_elementaire();
    if (*T == NULL)
    { // cas de base
        (*T) = TAVL_creerNoeud(i);
        if (*T == NULL)
        {
            printf("Allocation memoire impossible\n");
        }
        else
        {
            (*h) = 1;
        }
    }
    else
    { // HR : on sait obtenir un AVL après insertion dans g, rayon de g peut être augmenté
        if (i->num < ((*T)->val)->num)
        { //ajoute à gauche
            insertion(&((*T)->g), i, h);
            //printf("Here %d,%d\n", i->num, (*T)->val->num);
            //lecture_avl(*T);
            //printf("\n");
            operation_elementaire();
            if (*h == 1)
            {
                switch (((*T)->eq))
                {
                case -1: //l'arbre à été re-équilibré
                    ((*T)->eq) = 0;
                    *h = 0;
                    break;
                case 0: //l'arbre à été déséquilibré
                    ((*T)->eq) = 1;
                    *h = 1;
                    break;
                case 1: //l'arbre à été encore plus déséquilibré
                    operation_elementaire();
                    if ((((*T)->g)->eq) == 1)
                    {
                        // rotation simple vers la droite
                        rotation_d(T);
                        (*T)->d->eq = 0;
                        (*T)->eq = 0;
                    }
                    else
                    {
                        // rotation double
                        rotation_g_d(T);
                        operation_elementaire();
                        if ((*T)->eq == -1)
                        {
                            (*T)->g->eq = 1;
                            (*T)->d->eq = 0;
                        }
                        else if ((*T)->eq == 1)
                        {
                            operation_elementaire();
                            (*T)->g->eq = 0;
                            (*T)->d->eq = -1;
                        }
                        else
                        {
                            operation_elementaire();
                            (*T)->d->eq = 0;
                            (*T)->g->eq = 0;
                        }
                        (*T)->eq = 0;
                    }
                    *h = 0;
                    break;
                }
            }
        }
        else
        { //ajout à droite
            insertion(&((*T)->d), i, h);
            //printf("There %d,%d\n", i->num, (*T)->val->num);
            //lecture_avl(*T);
            //printf("\n");
            if (*h == 1)
            {
                operation_elementaire();
                switch (((*T)->eq))
                {
                case 1: //l'arbre à été re-équilibré
                    ((*T)->eq) = 0;
                    *h = 0;
                    break;
                case 0: //l'arbre à été déséquilibré
                    ((*T)->eq) = -1;
                    *h = 1;
                    break;
                case -1: //l'arbre à été encore plus déséquilibré
                    operation_elementaire();
                    if ((((*T)->d)->eq) == -1)
                    {
                        // rotation simple vers la droite
                        rotation_g(T);
                        (*T)->g->eq = 0;
                        (*T)->eq = 0;
                    }
                    else
                    {
                        // rotation double
                        rotation_d_g(T);
                        operation_elementaire();
                        if ((*T)->eq == 1)
                        {
                            (*T)->d->eq = -1;
                            (*T)->g->eq = 0;
                        }
                        else if ((*T)->eq == -1)
                        {
                            operation_elementaire();
                            (*T)->d->eq = 0;
                            (*T)->g->eq = 1;
                        }
                        else
                        {
                            operation_elementaire();
                            (*T)->d->eq = 0;
                            (*T)->g->eq = 0;
                        }
                        (*T)->eq = 0;
                    }
                    *h = 0;
                    break;
                }
            }
        }
    }
}

void printBool(bool b)
{
    if (b)
    {
        printf(" True ");
    }
    else
    {
        printf(" False ");
    }
}

void printlnBool(bool b)
{
    if (b)
    {
        printf(" True\n");
    }
    else
    {
        printf(" False\n");
    }
}

bool TAVL_rechercher(T_AVL a, int i)
{
    operation_elementaire();
    if (a == NULL)
    {
        return false;
    }
    else
    {
        operation_elementaire();
        if ((a->val)->num == i)
        {
            return true;
        }
        else if (a->val->num > i)
        {
            operation_elementaire();
            return TAVL_rechercher(a->g, i);
        }
        else
        {
            operation_elementaire();
            return TAVL_rechercher(a->d, i);
        }
    }
}

void lecture_avl(T_AVL T)
{
    if (T != NULL)
    {
        printf("<");
        lecture_avl(T->g);
        printf(" , %d/%d, ", T->val->num, T->eq);
        lecture_avl(T->d);
        printf(">");
    }
}
=======
#include <stdio.h>
#include <stdlib.h>
#include <stdbool.h>
#include "avl.h"

void printBool(bool b);
void printlnBool(bool b);
T_AVL TAVL_creer()
{
    return NULL;
}
T_AVL TAVL_creerNoeud(t_film *i)
{
    T_AVL a = (T_AVL)malloc(sizeof(struct T_noeudAVL));
    a->d = NULL;
    a->g = NULL;
    a->val = i;
    a->eq = 0;
    return a;
}
void rotation_d(T_AVL *pa)
{
    T_AVL racine = (*pa)->g;
    (*pa)->g = racine->d;
    racine->d = (*pa);
    *pa = racine;
}

void rotation_g(T_AVL *pa)
{
    T_AVL racine = (*pa)->d;
    (*pa)->d = racine->g;
    racine->g = (*pa);
    *pa = racine;
}

void rotation_d_g(T_AVL *pa)
{
    rotation_d(&((*pa)->d));
    rotation_g(pa);
    // (*pa)->d->eq--;
}

void rotation_g_d(T_AVL *pa)
{
    rotation_g(&((*pa)->g));
    rotation_d(pa);
    //(*pa)->g->eq++;
}

void insertion(T_AVL *T, t_film *i, int *h)
{
    operation_elementaire();
    if (*T == NULL)
    { // cas de base
        (*T) = TAVL_creerNoeud(i);
        if (*T == NULL)
        {
            printf("Allocation memoire impossible\n");
        }
        else
        {
            (*h) = 1;
        }
    }
    else
    { // HR : on sait obtenir un AVL après insertion dans g, rayon de g peut être augmenté
        if (i->num < ((*T)->val)->num)
        { //ajoute à gauche
            insertion(&((*T)->g), i, h);
            //printf("Here %d,%d\n", i->num, (*T)->val->num);
            //lecture_avl(*T);
            //printf("\n");
            operation_elementaire();
            if (*h == 1)
            {
                switch (((*T)->eq))
                {
                case -1: //l'arbre à été re-équilibré
                    ((*T)->eq) = 0;
                    *h = 0;
                    break;
                case 0: //l'arbre à été déséquilibré
                    ((*T)->eq) = 1;
                    *h = 1;
                    break;
                case 1: //l'arbre à été encore plus déséquilibré
                    operation_elementaire();
                    if ((((*T)->g)->eq) == 1)
                    {
                        // rotation simple vers la droite
                        rotation_d(T);
                        (*T)->d->eq = 0;
                        (*T)->eq = 0;
                    }
                    else
                    {
                        // rotation double
                        rotation_g_d(T);
                        operation_elementaire();
                        if ((*T)->eq == -1)
                        {
                            (*T)->g->eq = 1;
                            (*T)->d->eq = 0;
                        }
                        else if ((*T)->eq == 1)
                        {
                            operation_elementaire();
                            (*T)->g->eq = 0;
                            (*T)->d->eq = -1;
                        }
                        else
                        {
                            operation_elementaire();
                            (*T)->d->eq = 0;
                            (*T)->g->eq = 0;
                        }
                        (*T)->eq = 0;
                    }
                    *h = 0;
                    break;
                }
            }
        }
        else
        { //ajout à droite
            insertion(&((*T)->d), i, h);
            //printf("There %d,%d\n", i->num, (*T)->val->num);
            //lecture_avl(*T);
            //printf("\n");
            if (*h == 1)
            {
                operation_elementaire();
                switch (((*T)->eq))
                {
                case 1: //l'arbre à été re-équilibré
                    ((*T)->eq) = 0;
                    *h = 0;
                    break;
                case 0: //l'arbre à été déséquilibré
                    ((*T)->eq) = -1;
                    *h = 1;
                    break;
                case -1: //l'arbre à été encore plus déséquilibré
                    operation_elementaire();
                    if ((((*T)->d)->eq) == -1)
                    {
                        // rotation simple vers la droite
                        rotation_g(T);
                        (*T)->g->eq = 0;
                        (*T)->eq = 0;
                    }
                    else
                    {
                        // rotation double
                        rotation_d_g(T);
                        operation_elementaire();
                        if ((*T)->eq == 1)
                        {
                            (*T)->d->eq = -1;
                            (*T)->g->eq = 0;
                        }
                        else if ((*T)->eq == -1)
                        {
                            operation_elementaire();
                            (*T)->d->eq = 0;
                            (*T)->g->eq = 1;
                        }
                        else
                        {
                            operation_elementaire();
                            (*T)->d->eq = 0;
                            (*T)->g->eq = 0;
                        }
                        (*T)->eq = 0;
                    }
                    *h = 0;
                    break;
                }
            }
        }
    }
}

void printBool(bool b)
{
    if (b)
    {
        printf(" True ");
    }
    else
    {
        printf(" False ");
    }
}

void printlnBool(bool b)
{
    if (b)
    {
        printf(" True\n");
    }
    else
    {
        printf(" False\n");
    }
}

bool TAVL_rechercher(T_AVL a, int i)
{
    operation_elementaire();
    if (a == NULL)
    {
        return false;
    }
    else
    {
        operation_elementaire();
        if ((a->val)->num == i)
        {
            return true;
        }
        else if (a->val->num > i)
        {
            operation_elementaire();
            return TAVL_rechercher(a->g, i);
        }
        else
        {
            operation_elementaire();
            return TAVL_rechercher(a->d, i);
        }
    }
}

void lecture_avl(T_AVL T)
{
    if (T != NULL)
    {
        printf("<");
        lecture_avl(T->g);
        printf(" , %d/%d, ", T->val->num, T->eq);
        lecture_avl(T->d);
        printf(">");
    }
}
>>>>>>> main
