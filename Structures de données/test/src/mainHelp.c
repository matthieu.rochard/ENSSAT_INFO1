/***
 * compilation :
 *		gcc -O3 -Wall -Wextra -Wconversion -ansi -Wpedantic -std=c11  *.c -o TestABR
 *
 * extraction des donnees : for l in 10 1000 2000 4000 10000 20000 40000 100000 200000 400000 1000000; do echo $l; head -n $l ~/tmp/title.basics.tsv | sort -R > title_$l.tsv; done
 *
 * generation des résutats :
 *		for l in 10 1000 2000 4000 10000 20000 40000 100000 200000 400000 1000000; do echo $l; ./TestABR title_$l.tsv | tee -a resultat_abr_$l.txt; done
 *
 */

#include <sys/types.h>
#include <sys/stat.h>
#include <fcntl.h>
#include <unistd.h>
#include <stdio.h>
#include <stdlib.h>
#include <math.h>
#include <time.h>
#include "liste.h"
#include "complexite.h"
#include "abr.h"
#include "avl.h"
#include "film.h"
#include "liste.h"
#include "hashTable.h"
/********************************************************************
 *************** main AVL *******************************************
 ********************************************************************/
int mainAVL(int argc, char *argv[])
// argc est le nombre d'argument et vaut au moins 1
// le premier argument étant le chemin vers le nom de l'exécutable
// argv[] est le tableau des arguments
{
	if (argc - 1 != 1)
	{
		fprintf(stderr, "Usage %s <fichier.tsv>\n", argv[0]);
		exit(EXIT_FAILURE);
	}

	FILE *fd = fopen(argv[1], "r");
	char ligne[4096];

	// Déclaration des structures

	T_AVL t;
	// cles = tableau des numéros des films
	// sauvegarde des clés dans ce tableau pour faire un tirage aléatoire d'une 50 aines de clés
	// puis effectuer une moyenne d'une nombre d'opérations fondamentales pour la recherche de ces 50 clés
	int *cles;
	t_film *film = (t_film *)malloc(sizeof(t_film));
	film->num = 0;
	film->titre = "Test Flim";
	film->annee = 2021;
	//initialisation d'un pointeur sur un film
	t_film *pfilm = NULL;
	// i prochaine case libre du tableau
	int x, y, h = 0, i = 0;
	srand(time(NULL));
	t = TAVL_creer();

	cles = (int *)malloc(100000 * sizeof(int)); // à modifier en fonction de la taille de la donnée

	/*********************************************************************/
	/************************* Phase d'insertion *************************/
	/*********************************************************************/

	// la partie suivante lit chaque ligne du fichier tsv pour extraire les informations utiles
	// à la création de films

	while (fgets(ligne, sizeof(ligne), fd) != NULL)
	{
		// creation d'un film à partir d'une ligne du fichier tsv et récupération d'un pointeur sur ce film.
		pfilm = creer_film(ligne);
		//printf("%d %s %d\n", pfilm->num, pfilm->titre, pfilm->annee);
		// insertion de l'adresse du film dans la structure
		if (pfilm != NULL)
		{
			//printf("////insert : %d\n", pfilm->num);
			insertion(&t, pfilm, &h);
			//lecture_avl(t);
			// récupération de la clé
			cles[i] = pfilm->num;
			// mise à jour de la prochaine case pouvant recevoir une clé
			i++;
		}
	}
	film->num = ++i;
	initialiser_comp_op_elementaire();
	demarrer_chrono();
	insertion(&t, film, &h);
	arreter_chrono();
	printf(",{\"ajoutComp\":%lu,", compteur_op_elementaire);
	i--;
	long int temps_cumul = temps_chrono();

	long int usec = temps_cumul % 1000;
	long int msec = (temps_cumul / 1000) % 1000;
	long int sec = temps_cumul / 1000000;
	// (s:ms:us)
	printf("\"ajoutTmp\":%ld,", sec * 1000000 + msec * 1000 + usec);

	/*********************************************************************/
	/*********************** Test de recherche **************************/
	/*********************************************************************/
	//89119

	demarrer_chrono();
	for (y = 0; y < 100; y++)
	{
		x = rand() % i;
		TAVL_rechercher(t, cles[x]);
	}
	arreter_chrono();
	temps_cumul = temps_chrono() / 100;

	usec = (temps_cumul % 1000);
	msec = ((temps_cumul / 1000) % 1000);
	sec = (temps_cumul / 1000000);
	//printf("Temps de recherche de 89119, trouve ?");
	printf("\"rechercheTmp\":%ld,", sec * 1000000 + msec * 1000 + usec);

	initialiser_comp_op_elementaire();
	for (y = 0; y < 100; y++)
	{
		x = rand() % i;
		TAVL_rechercher(t, cles[x]);
	}
	printf("\"rechercheComp\":%lu}", compteur_op_elementaire / 100);
	return EXIT_SUCCESS;
}

/********************************************************************
 *************** main ABR *******************************************
 ********************************************************************/
int mainABR(int argc, char *argv[])
// argc est le nombre d'argument et vaut au moins 1
// le premier argument étant le chemin vers le nom de l'exécutable
// argv[] est le tableau des arguments
{
	if (argc - 1 != 1)
	{
		fprintf(stderr, "Usage %s <fichier.tsv>\n", argv[0]);
		exit(EXIT_FAILURE);
	}

	FILE *fd = fopen(argv[1], "r");
	char ligne[4096];

	// Déclaration des structures

	T_Tree t;
	// cles = tableau des numéros des films
	// sauvegarde des clés dans ce tableau pour faire un tirage aléatoire d'une 50 aines de clés
	// puis effectuer une moyenne d'une nombre d'opérations fondamentales pour la recherche de ces 50 clés
	int *cles;
	t_film *film = (t_film *)malloc(sizeof(t_film));
	film->num = 0;
	film->titre = "Test Flim";
	film->annee = 2021;
	//initialisation d'un pointeur sur un film
	t_film *pfilm = NULL;
	// i prochaine case libre du tableau
	int x, y, i = 0;
	srand(time(NULL));
	TABR_creer(&t);

	cles = (int *)malloc(100000 * sizeof(int)); // à modifier en fonction de la taille de la donnée

	/*********************************************************************/
	/************************* Phase d'insertion *************************/
	/*********************************************************************/

	// la partie suivante lit chaque ligne du fichier tsv pour extraire les informations utiles
	// à la création de films

	while (fgets(ligne, sizeof(ligne), fd) != NULL)
	{
		// creation d'un film à partir d'une ligne du fichier tsv et récupération d'un pointeur sur ce film.
		pfilm = creer_film(ligne);
		//printf("%d %s %d\n", pfilm->num, pfilm->titre, pfilm->annee);
		// insertion de l'adresse du film dans la structure
		if (pfilm != NULL)
		{
			//printf("////insert : %d\n", pfilm->num);
			TABR_ajouter(&t, pfilm);
			//lecture_avl(t);
			// récupération de la clé
			cles[i] = pfilm->num;
			// mise à jour de la prochaine case pouvant recevoir une clé
			i++;
		}
	}
	film->num = ++i;
	initialiser_comp_op_elementaire();
	demarrer_chrono();
	TABR_ajouter(&t, film);
	arreter_chrono();
	printf(",{\"ajoutComp\":%lu,", compteur_op_elementaire);
	i--;
	long int temps_cumul = temps_chrono();

	long int usec = temps_cumul % 1000;
	long int msec = (temps_cumul / 1000) % 1000;
	long int sec = temps_cumul / 1000000;

	printf("\"ajoutTmp\":%ld,", sec * 1000000 + msec * 1000 + usec);

	/*********************************************************************/
	/*********************** Test de recherche **************************/
	/*********************************************************************/
	//89119

	demarrer_chrono();
	for (y = 0; y < 100; y++)
	{
		x = rand() % i;
		TABR_rechercher(t, cles[x]);
	}
	arreter_chrono();
	temps_cumul = temps_chrono() / 100;

	usec = (temps_cumul % 1000);
	msec = ((temps_cumul / 1000) % 1000);
	sec = (temps_cumul / 1000000);
	printf("\"rechercheTmp\":%ld,", sec * 1000000 + msec * 1000 + usec);

	initialiser_comp_op_elementaire();
	for (y = 0; y < 100; y++)
	{
		x = rand() % i;
		TABR_rechercher(t, cles[x]);
	}
	printf("\"rechercheComp\":%lu}", compteur_op_elementaire / 100);
	return EXIT_SUCCESS;
}

/********************************************************************
 *************** main Liste *******************************************
 ********************************************************************/
int mainL(int argc, char *argv[])
// argc est le nombre d'argument et vaut au moins 1
// le premier argument étant le chemin vers le nom de l'exécutable
// argv[] est le tableau des arguments
{
	if (argc - 1 != 1)
	{
		fprintf(stderr, "Usage %s <fichier.tsv>\n", argv[0]);
		exit(EXIT_FAILURE);
	}

	FILE *fd = fopen(argv[1], "r");
	char ligne[4096];

	// Déclaration des structures

	T_liste t;
	// cles = tableau des numéros des films
	// sauvegarde des clés dans ce tableau pour faire un tirage aléatoire d'une 50 aines de clés
	// puis effectuer une moyenne d'une nombre d'opérations fondamentales pour la recherche de ces 50 clés
	int *cles;
	t_film *film = (t_film *)malloc(sizeof(t_film));
	film->num = 0;
	film->titre = "Test Flim";
	film->annee = 2021;
	//initialisation d'un pointeur sur un film
	t_film *pfilm = NULL;
	// i prochaine case libre du tableau
	int x, y, i = 0;
	srand(time(NULL));
	t = creer_liste();

	cles = (int *)malloc(100000 * sizeof(int)); // à modifier en fonction de la taille de la donnée

	/*********************************************************************/
	/************************* Phase d'insertion *************************/
	/*********************************************************************/

	// la partie suivante lit chaque ligne du fichier tsv pour extraire les informations utiles
	// à la création de films

	while (fgets(ligne, sizeof(ligne), fd) != NULL)
	{
		// creation d'un film à partir d'une ligne du fichier tsv et récupération d'un pointeur sur ce film.
		pfilm = creer_film(ligne);
		//printf("%d %s %d\n", pfilm->num, pfilm->titre, pfilm->annee);
		// insertion de l'adresse du film dans la structure
		if (pfilm != NULL)
		{
			//printf("////insert : %d\n", pfilm->num);
			ajout_entete_liste(&t, pfilm);
			//lecture_avl(t);
			// récupération de la clé
			cles[i] = pfilm->num;
			// mise à jour de la prochaine case pouvant recevoir une clé
			i++;
		}
	}
	film->num = ++i;
	initialiser_comp_op_elementaire();
	demarrer_chrono();
	ajout_entete_liste(&t, film);
	arreter_chrono();
	printf(",{\"ajoutComp\":%lu,", compteur_op_elementaire);
	i--;
	long int temps_cumul = temps_chrono();

	long int usec = temps_cumul % 1000;
	long int msec = (temps_cumul / 1000) % 1000;
	long int sec = temps_cumul / 1000000;

	printf("\"ajoutTmp\":%ld,", sec * 1000000 + msec * 1000 + usec);

	/*********************************************************************/
	/*********************** Test de recherche **************************/
	/*********************************************************************/
	//89119

	demarrer_chrono();
	for (y = 0; y < 100; y++)
	{
		x = rand() % i;
		TLISTE_rechercher(t, cles[x]);
	}
	arreter_chrono();
	temps_cumul = temps_chrono() / 100;

	usec = (temps_cumul % 1000);
	msec = ((temps_cumul / 1000) % 1000);
	sec = (temps_cumul / 1000000);
	printf("\"rechercheTmp\":%ld,", sec * 1000000 + msec * 1000 + usec);

	initialiser_comp_op_elementaire();
	for (y = 0; y < 100; y++)
	{
		x = rand() % i;
		TLISTE_rechercher(t, cles[x]);
	}
	printf("\"rechercheComp\":%lu}", compteur_op_elementaire / 100);
	return EXIT_SUCCESS;
}

/********************************************************************
 *************** main HashTable *************************************
 ********************************************************************/
int main(int argc, char *argv[])
// argc est le nombre d'argument et vaut au moins 1
// le premier argument étant le chemin vers le nom de l'exécutable
// argv[] est le tableau des arguments
{
	if (argc - 1 != 1)
	{
		fprintf(stderr, "Usage %s <fichier.tsv>\n", argv[0]);
		exit(EXIT_FAILURE);
	}

	FILE *fd = fopen(argv[1], "r");
	char ligne[4096];

	// Déclaration des structures

	T_hashTable t;
	// cles = tableau des numéros des films
	// sauvegarde des clés dans ce tableau pour faire un tirage aléatoire d'une 50 aines de clés
	// puis effectuer une moyenne d'une nombre d'opérations fondamentales pour la recherche de ces 50 clés
	int *cles;
	t_film *film = (t_film *)malloc(sizeof(t_film));
	film->num = 0;
	film->titre = "Test Flim";
	film->annee = 2021;
	//initialisation d'un pointeur sur un film
	t_film *pfilm = NULL;
	// i prochaine case libre du tableau
	int x, y, h = 0, i = 0;
	srand(time(NULL));
	creerHashTable(&t);

	cles = (int *)malloc(100000 * sizeof(int)); // à modifier en fonction de la taille de la donnée

	/*********************************************************************/
	/************************* Phase d'insertion *************************/
	/*********************************************************************/

	// la partie suivante lit chaque ligne du fichier tsv pour extraire les informations utiles
	// à la création de films
	//
	while (fgets(ligne, sizeof(ligne), fd) != NULL)
	{
		// creation d'un film à partir d'une ligne du fichier tsv et récupération d'un pointeur sur ce film.
		pfilm = creer_film(ligne);
		//printf("%d %s %d\n", pfilm->num, pfilm->titre, pfilm->annee);
		// insertion de l'adresse du film dans la structure
		if (pfilm != NULL)
		{
			//printf("////insert : %d\n", pfilm->num);
			insert(&t, pfilm);
			//lecture_avl(t);
			// récupération de la clé
			cles[i] = pfilm->num;
			// mise à jour de la prochaine case pouvant recevoir une clé
			i++;
		}
	}
	film->num = ++i;
	initialiser_comp_op_elementaire();
	demarrer_chrono();
	insert(&t, film);
	arreter_chrono();
	printf(",{\"ajoutComp\":%lu,", compteur_op_elementaire);
	i--;
	long int temps_cumul = temps_chrono();

	long int usec = temps_cumul % 1000;
	long int msec = (temps_cumul / 1000) % 1000;
	long int sec = temps_cumul / 1000000;
	// (s:ms:us)
	printf("\"ajoutTmp\":%ld,", sec * 1000000 + msec * 1000 + usec);

	/*********************************************************************/
	/*********************** Test de recherche **************************/
	/*********************************************************************/
	//89119

	demarrer_chrono();
	for (y = 0; y < 100; y++)
	{
		x = rand() % i;
		rechercheHashTable(t, cles[x]);
	}
	arreter_chrono();
	temps_cumul = temps_chrono() / 100;

	usec = (temps_cumul % 1000);
	msec = ((temps_cumul / 1000) % 1000);
	sec = (temps_cumul / 1000000);
	//printf("Temps de recherche de 89119, trouve ?");
	printf("\"rechercheTmp\":%ld,", sec * 1000000 + msec * 1000 + usec);

	initialiser_comp_op_elementaire();
	for (y = 0; y < 100; y++)
	{
		x = rand() % i;
		rechercheHashTable(t, cles[x]);
	}
	printf("\"rechercheComp\":%lu}", compteur_op_elementaire / 100);
	return EXIT_SUCCESS;
}