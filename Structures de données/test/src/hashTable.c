#include <stdio.h>
#include <stdlib.h>
#include <stdbool.h>
#include "hashtable.h"
void creerHashTable(T_hashTable *t)
{
    (*t) = (T_hashTable)malloc(sizeof(T_liste) * 1000);
    for (int i = 0; i < 1000; i++)
    {
        (*t)[i] = creer_liste();
    }
}

void insert(T_hashTable *t, t_film *i)
{
    int index = hashFonction(i->num);
    ajout_entete_liste(&((*t)[index]), i);
}

int hashFonction(int i)
{
    return i % 1000;
}

bool rechercheHashTable(T_hashTable t, int i)
{
    int index = hashFonction(i);
    return TLISTE_rechercher(t[index], i);
}
/*
void afficher_hashTable(T_hashTable t)
{
    for (int i = 0; i < 9; i++)
    {
        printf("|%d|", i);
        afficher_liste(t[i]);
    }
}
*/