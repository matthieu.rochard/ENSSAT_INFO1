const avl = require('./resultat_avl.json');
const abr = require('./resultat_abr.json');
const liste = require('./resultat_liste.json');
const hashT = require('./resultat_hashtable.json');
const stuff = ["10", "1000", "2000", "4000", "10000", "20000", "40000", "100000"]

console.log("nombre de valeur;abr;avl;liste;hashTable")
for (i = 0; i < 8; i++) {
    console.log(stuff[i] + ';' +
        abr[stuff[i]]['ajoutComp'] + ";" +
        avl[stuff[i]]['ajoutComp'] + ";" +
        liste[stuff[i]]['ajoutComp'] + ";" +
        hashT[stuff[i]]['ajoutComp']);
}

console.log("nombre de valeur;abr;avl;liste;hashTable")
for (i = 0; i < 8; i++) {
    console.log(stuff[i] + ';' +
        abr[stuff[i]]['ajoutTmp'] + ";" +
        avl[stuff[i]]['ajoutTmp'] + ";" +
        liste[stuff[i]]['ajoutTmp'] + ";" +
        hashT[stuff[i]]['ajoutTmp']);
}

console.log("nombre de valeur;abr;avl;liste;hashTable")
for (i = 0; i < 8; i++) {
    console.log(stuff[i] + ';' +
        abr[stuff[i]]['rechercheComp'] + ";" +
        avl[stuff[i]]['rechercheComp'] + ";" +
        liste[stuff[i]]['rechercheComp'] + ";" +
        hashT[stuff[i]]['rechercheComp']);
}

console.log("nombre de valeur;abr;avl;liste;hashTable")
for (i = 0; i < 8; i++) {
    console.log(stuff[i] + ';' +
        abr[stuff[i]]['rechercheTmp'] + ";" +
        avl[stuff[i]]['rechercheTmp'] + ";" +
        liste[stuff[i]]['rechercheTmp'] + ";" +
        hashT[stuff[i]]['rechercheTmp']);
}