//const fs = require('fs');
const results = require('./' + process.argv[2]);
//reads the result file that must be read
//console.log(results[0]);
var semiFinal = {
    "ajoutComp": 0,
    "ajoutTmp": 0,
    "rechercheTmp": 0,
    "rechercheComp": 0
}
results.forEach(element => {
    semiFinal["ajoutComp"] += element["ajoutComp"];
    semiFinal["ajoutTmp"] += element["ajoutTmp"];
    semiFinal["rechercheTmp"] += element["rechercheTmp"];
    semiFinal["rechercheComp"] += element["rechercheComp"];
});
semiFinal["ajoutComp"] /= process.argv[3];
semiFinal["ajoutTmp"] /= process.argv[3];
semiFinal["rechercheTmp"] /= process.argv[3];
semiFinal["rechercheComp"] /= process.argv[3];
console.log("{");
console.log('"ajoutComp":' + semiFinal["ajoutComp"]);
console.log(',"ajoutTmp":' + semiFinal["ajoutTmp"]);
console.log(',"rechercheTmp":' + semiFinal["rechercheTmp"]);
console.log(',"rechercheComp":' + semiFinal["rechercheComp"]);
console.log("}");