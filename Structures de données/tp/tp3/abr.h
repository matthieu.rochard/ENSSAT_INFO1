#include <stdbool.h>

typedef struct T_noeud
{
    int val;
    struct T_noeud *g, *d;
} * TABR;

TABR TABR_creer();
TABR TABR_creerNoeud(int);
void TABR_ajouter(TABR *, int);
bool TABR_rechercher(TABR, int);
void TABR_ajouterEnRacine(TABR *, int);
void lecture_arbre(TABR);
void rotation_droite(TABR *);
void rotation_gauche(TABR *);
void lecture_arbre(TABR);