#include "abr.h"
#include <stdio.h>
#include <stdlib.h>

void printBool(bool b);
void printlnBool(bool b);

TABR TABR_creer()
{
    return NULL;
}

TABR TABR_creerNoeud(int i)
{
    TABR a = (TABR)malloc(sizeof(TABR));
    a->d = NULL;
    a->g = NULL;
    a->val = i;
    return a;
}

void TABR_ajouter(TABR *pa, int i)
{
    if (*pa == NULL)
    {
        (*pa) = TABR_creerNoeud(i);
    }
    else
    {
        if ((*pa)->val >= i)
        {
            TABR_ajouter(&(*pa)->g, i);
        }
        else
        {
            TABR_ajouter(&(*pa)->d, i);
        }
    }
}

bool TABR_rechercher(TABR a, int i)
{
    if (a == NULL)
    {
        return false;
    }
    else
    {
        if (a->val == i)
        {
            return true;
        }
        else if (a->val > i)
        {
            return TABR_rechercher(a->g, i);
        }
        else
        {
            return TABR_rechercher(a->d, i);
        }
    }
}

void rotation_droite(TABR *pa)
{
    TABR racine = (*pa)->g;
    (*pa)->g = racine->d;
    racine->d = (*pa);
    *pa = racine;
}

void rotation_gauche(TABR *pa)
{
    TABR racine = (*pa)->d;
    (*pa)->d = racine->g;
    racine->g = (*pa);
    *pa = racine;
}

void TABR_ajouterEnRacine(TABR *pa, int i)
{
    if (*pa == NULL)
    {
        (*pa) = TABR_creerNoeud(i);
    }
    else
    {
        printf("%d   ", (*pa)->val);
        if ((*pa)->val >= i)
        {
            TABR_ajouterEnRacine(&(*pa)->g, i);
            rotation_droite(pa);
        }
        else
        {
            TABR_ajouterEnRacine(&(*pa)->d, i);
            rotation_gauche(pa);
        }
    }
}

void lecture_arbre(TABR T)
{
    if (T != NULL)
    {
        printf("<");
        lecture_arbre(T->g);
        printf(" , %d, ", T->val);
        lecture_arbre(T->d);
        printf(">");
    }
}