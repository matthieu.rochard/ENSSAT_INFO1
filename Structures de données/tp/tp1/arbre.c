#include "abr.h"
#include <stdio.h>
#include <stdlib.h>
void printBool(bool b);
void printlnBool(bool b);

TABR TABR_creer()
{
    return NULL;
}

TABR TABR_creerNoeud(int i)
{
    TABR a = (TABR)malloc(sizeof(TABR));
    a->d = NULL;
    a->g = NULL;
    a->val = i;
    return a;
}

void TABR_ajouter(TABR *pa, int i)
{
    if (*pa == NULL)
    {
        (*pa) = TABR_creerNoeud(i);
    }
    else
    {
        if ((*pa)->val >= i)
        {
            TABR_ajouter(&(*pa)->g, i);
        }
        else
        {
            TABR_ajouter(&(*pa)->d, i);
        }
    }
}

bool TABR_rechercher(TABR a, int i)
{
    if (a == NULL)
    {
        return false;
    }
    else
    {
        if (a->val == i)
        {
            return true;
        }
        else if (a->val > i)
        {
            TABR_rechercher(a->g, i);
        }
        else
        {
            TABR_rechercher(a->d, i);
        }
    }
}

void rotation_droite(TABR *pa)
{
    TABR racine = (*pa)->g;
    (*pa)->g = racine->d;
    racine->d = (*pa);
    *pa = racine;
}

void rotation_gauche(TABR *pa)
{
    TABR racine = (*pa)->d;
    (*pa)->d = racine->g;
    racine->g = (*pa);
    *pa = racine;
}

void TABR_ajouterEnRacine(TABR *pa, int i)
{
    if (*pa == NULL)
    {
        (*pa) = TABR_creerNoeud(i);
    }
    else
    {
        printf("%d   ", (*pa)->val);
        if ((*pa)->val >= i)
        {
            TABR_ajouterEnRacine(&(*pa)->g, i);
            rotation_droite(pa);
        }
        else
        {
            TABR_ajouterEnRacine(&(*pa)->d, i);
            rotation_gauche(pa);
        }
    }
}

int main(int argc, char const *argv[])
{
    TABR abr = TABR_creer();
    TABR_ajouter(&abr, 12);
    TABR_ajouter(&abr, 3);
    TABR_ajouter(&abr, 2);

    /** printf("\t  %d\n", abr->val);
    printf("\t%d-|-", abr->g->val);
    printf("%d\n", abr->d->val);*/
    lecture_arbre(abr);
    printf("\n5 est dans :");
    printlnBool(TABR_rechercher(abr, 5));
    printf("3 est dans :");
    printlnBool(TABR_rechercher(abr, 3));
    printf("11 est dans :");
    printlnBool(TABR_rechercher(abr, 11));

    printf("%d\n", abr->g->val);
    TABR_ajouter(&abr, 5);
    lecture_arbre(abr);
    rotation_droite(&abr);
    
    printf("\n");
    lecture_arbre(abr);
    rotation_gauche(&abr);
    printf("\n");
    lecture_arbre(abr);
    printf("\n----------------------\n");
    TABR_ajouterEnRacine(&abr, 7);
    lecture_arbre(abr);
    printf("\n");
    return 0;
}

void printBool(bool b)
{
    if (b)
    {
        printf(" True ");
    }
    else
    {
        printf(" False ");
    }
}
void printlnBool(bool b)
{
    if (b)
    {
        printf(" True\n");
    }
    else
    {
        printf(" False\n");
    }
}

void lecture_arbre(TABR T)
{
    if (T != NULL)
    {
        printf("<");
        lecture_arbre(T->g);
        printf(" , %d, ", T->val);
        lecture_arbre(T->d);
        printf(">");
    }
}