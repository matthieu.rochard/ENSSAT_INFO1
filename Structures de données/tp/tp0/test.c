#include <stdio.h>
#include <stdlib.h>
#include "liste.h"

int main(void)
{
    T_liste liste = creer_liste();
    fafficher_dir_liste(liste);
    printf("Insert :\n");
    ajout_entete_liste(&liste,3);
    ajout_entete_liste(&liste,2);
    ajout_entete_liste(&liste,1);

    printf("\nAffichage :\n");
    fafficher_dir_liste(liste);
    printf("\nAffichage Inv:\n");
    fafficher_inv_liste(liste);

    printf("Compte:\n");
    printf("%d , %d",long_env(liste),long_ter(liste,0));

    printf("\nInverted :\n");
    inv_mis_liste(&liste);
    fafficher_dir_liste(liste);

    printf("Free\n");
    liberer_liste(liste);
    fafficher_dir_liste(liste);
}