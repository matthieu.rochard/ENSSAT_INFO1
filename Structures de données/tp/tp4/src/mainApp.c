/* ==================================== Include    =================================== */
#include <stdio.h>
#include <stdlib.h>
#include <stdbool.h>
#include "hashTable.h"

int main(int argc, char *argv[])
{
    int C[9] = {5,
                28,
                19,
                15,
                20,
                33,
                12,
                17,
                10};
    // int C[3] = {28, 19, 10};
    T_hashTable hashTable;
    creerHashTable(&hashTable);

    for (int i = 0; i < 9; i++)
    {
        insert(&hashTable, C[i]);
    }

    afficher_hashTable(hashTable);
}