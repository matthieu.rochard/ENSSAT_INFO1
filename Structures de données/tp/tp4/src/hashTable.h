
#ifndef _HashTable_h
#define _HashTable_h
#include <stdbool.h>
#include "liste.h"

#define HASHTABLE_SIZE 9

typedef T_liste *T_hashTable;

void creerHashTable(T_hashTable *);
void insert(T_hashTable *, int);
int hashFonction(int);
bool rechercheHashTable(T_hashTable, int);

void afficher_hashTable(T_hashTable t);
#endif