#include <stdio.h>    /* stderr, stdout, fprintf, perror */
#include <unistd.h>   /* fork */
#include <sys/wait.h> /* wait */
#include <stdlib.h>   /* exit */
#include <string.h>

int main(int argc, char **argv)
{
    printf("%s %d\n", argv[1], argc);
    char src[100] = "";
    printf("d--\n");
    for (int i = 2; i < argc; i++)
    {
        if (i != 2)
        {
            strcat(src, " ");
        }
        //printf("%d--", i);
        strcat(src, argv[i]);
    }
    printf("\n%s %s\n", argv[1], src);
    execlp(argv[1], argv[1], src, NULL);
}