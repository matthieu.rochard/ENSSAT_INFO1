#include <stdio.h>    /* stderr, stdout, fprintf, perror */
#include <unistd.h>   /* fork */
#include <sys/wait.h> /* wait */
#include <stdlib.h>   /* exit */
#include <string.h>

int main()
{
    char str[5], src[20];
    sprintf(src, "%s", "/bin/pstree");
    sprintf(str, "%d", getppid());

    //execlp("/bin/pstree", "/bin/pstree", str, "-p", NULL);

    char *const parmList[] = {"pstree", "-p", str, NULL};
    execv("/bin/pstree", parmList);
}