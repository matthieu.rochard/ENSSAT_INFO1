#include <stdio.h>    /* stderr, stdout, fprintf, perror */
#include <unistd.h>   /* fork */
#include <sys/wait.h> /* wait */
#include <stdlib.h>   /* exit */

int main(void)
{
    pid_t pid_fils = -1;
    int i = 1;
    do
    {
        pid_fils = fork();
        i++;
    } while (i < 4 && pid_fils == 0);
    sleep(10);
    wait(NULL);
    exit(EXIT_SUCCESS);
}