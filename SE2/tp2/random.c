#include <stdio.h>    /* stderr, stdout, fprintf, perror */
#include <unistd.h>   /* fork */
#include <sys/wait.h> /* wait */
#include <stdlib.h>   /* exit */

#define N 100

int main(void)
{
    pid_t pid_fils = -1;
    int i = 0, status = 0;
    do
    {
        pid_fils = fork();
        i++;
    } while (i < N && pid_fils != 0);

    if (pid_fils == 0)
    {
        sleep(1);
        exit(i);
    }
    else
    {
        wait(&status);

        while (i > 0)
        {
            wait(NULL);
            i--;
        }
        printf("random is: %d\n", WEXITSTATUS(status));
        exit(EXIT_SUCCESS);
    }
}