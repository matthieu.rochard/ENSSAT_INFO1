#include <stdio.h>    /* stderr, stdout, fprintf, perror */
#include <unistd.h>   /* fork */
#include <sys/wait.h> /* wait */
#include <stdlib.h>   /* exit */

#define N 100

int main()
{
    int n, x, i = 0;
    pid_t pid_fils = -1;
    printf("---Taille du tableau ?: ");
    scanf("%d", &n);
    int tab[n], half = n / 2;
    while (i < n)
    {
        printf("---Le %deme nombre ?: ", i + 1);
        scanf("%d", &(tab[i]));
        i++;
    }
    printf("---Valeur à rechercher ?: ");
    scanf("%d", &x);
    pid_fils = fork();
    if (pid_fils == 0)
    {
        for (i = 0; i < half; i++)
        {
            if (tab[i] == x)
            {
                printf("Valeur trouvé dans la première moitiée\n");
            }
        }
        exit(EXIT_SUCCESS);
    }

    pid_fils = fork();
    if (pid_fils == 0)
    {
        for (i = half; i < n; i++)
        {
            if (tab[i] == x)
            {
                printf("Valeur trouvé dans la deuxième moitiée\n");
            }
        }
        exit(EXIT_SUCCESS);
    }
    wait(NULL);
    wait(NULL);
    exit(EXIT_SUCCESS);
}