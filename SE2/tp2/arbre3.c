#include <stdio.h>    /* stderr, stdout, fprintf, perror */
#include <unistd.h>   /* fork */
#include <sys/wait.h> /* wait */
#include <stdlib.h>   /* exit */

int main(void)
{
    pid_t pid_fils = -1;
    int i = 0;
    pid_fils = fork();
    i++;
    if (pid_fils == 0)
    {
        pid_fils = fork();
        if (pid_fils == 0)
        {
            pid_fils = fork();
        }
    }
    else
    {
        do
        {
            pid_fils = fork();
            i++;
        } while (i < 3 && pid_fils != 0);
    }

    sleep(10);
    wait(NULL);
    exit(EXIT_SUCCESS);
}